package edu.illinois.cs.cogcomp.esrl.specificity;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
import edu.illinois.cs.cogcomp.nlp.utility.TokenizerTextAnnotationBuilder;

import java.util.ArrayList;
import java.util.List;

public class SpecificityDataReader extends DataReader {

    public SpecificityDataReader(String file, String corpusName) {
        super(file, corpusName, ESRLViewNames.SPECIFICITY);
    }

    @Override
    public List<TextAnnotation> readData() {
        TokenizerTextAnnotationBuilder textAnnotationBuilder = new TokenizerTextAnnotationBuilder(new IllinoisTokenizer());
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        try {
            for (String line : LineIO.read(file)) {
                String[] fields = line.split("\\t");
                String sentId = fields[0];
                // Remove sentence quotes
                String sentence = fields[3].substring(1, fields[3].length()-1);
                String label = fields[7];
                TextAnnotation ta = textAnnotationBuilder.createTextAnnotation(file, sentId, sentence);
                SpanLabelView view = new SpanLabelView(viewName, ta);
                view.addSpanLabel(0, ta.getView(ViewNames.TOKENS).count(), label, 1.0);
                ta.addView(viewName, view);
                textAnnotations.add(ta);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Couldn't read " + file);
        }
        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        List<Constituent> candidates = ta.getView(ViewNames.SENTENCE).getConstituents();
        return getFinalCandidates(ta.getView(viewName), candidates);
    }
}
