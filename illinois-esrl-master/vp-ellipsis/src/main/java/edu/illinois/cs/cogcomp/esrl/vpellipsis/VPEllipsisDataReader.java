package edu.illinois.cs.cogcomp.esrl.vpellipsis;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
import edu.illinois.cs.cogcomp.nlp.utility.TokenizerTextAnnotationBuilder;

import java.io.IOException;
import java.util.*;

/**
 * A data reader for the SemEval-2010 task 4 (VP Ellipsis) corpus.
 * <br/>
 * <b>NB:</b> The dataset is <b>NOT</b> tokenized!
 */
public abstract class VPEllipsisDataReader extends DataReader {
    public static final String TRIGGER_TYPE = "Type";
    public static final String ANTECEDENT_TYPE = "SyntacticType";

    /** Since the data is not tokenized, we need this */
    private static TokenizerTextAnnotationBuilder annotationBuilder;

    /**
     * Calls {@link #readData()} with the data from the WSJ directories specified in {@code folders}
     * and aligns them to the raw WSJ sentences.
     *
     * @param folders A comma separated list of WSJ directories to use (this will be passed to the {@code file}
     *                variable of the {@link DataReader}.
     */
    public VPEllipsisDataReader(String folders, String corpusName) {
        super(folders, corpusName, ESRLViewNames.VP_ELLIPSIS);
    }

    @Override
    public List<TextAnnotation> readData() {
        String wsjDir = rm.getString(ESRLConfigurator.WSJ_RAW_DIR);
        String vpDataDir = rm.getString(ESRLConfigurator.VP_ELLIPSIS_DATA_DIR);
        String[] wsjFolders = file.split(",");

        annotationBuilder = new TokenizerTextAnnotationBuilder(new IllinoisTokenizer());

        List<TextAnnotation> textAnnotations = new ArrayList<>();
        try {
            for (String dir : wsjFolders) {
                // First we need to store the WSJ lines with their line number for reference
                Map<String, List<String>> wsjLines = new HashMap<>();
                for (String wsjFile : IOUtils.lsFiles(wsjDir + "/" + dir)) {
                    wsjLines.put(IOUtils.getFileName(wsjFile), LineIO.read(wsjFile));
                }
                // Now we need to read the VPE file and get the references
                List<String> vpeLines = LineIO.read(vpDataDir + "/" + dir);
                for (String line : vpeLines) {
                    TextAnnotation ta = getTextAnnotation(wsjLines, line);
                    if (ta == null) continue;
                    textAnnotations.add(ta);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textAnnotations;
    }

    private TextAnnotation getTextAnnotation(Map<String, List<String>> wsjLines, String line) {
        //<FILENAME> <EL> <EB> <EE> <AL> <AB> <AE> <TR> <STA> <STP>  ??
        //wsj_0018    5    92   93   5    62   86   be   vped  soa  oth
        String[] fields = line.split("\\s+");
        String ellipsisFile = fields[0];
        int ellipsisLine = Integer.parseInt(fields[1]) - 1;
        int ellipsisCharStart = Integer.parseInt(fields[2]);
        int ellipsisCharEnd = Integer.parseInt(fields[3]);

        int antecedentLine= Integer.parseInt(fields[4]) - 1;
        int antecedentCharStart = Integer.parseInt(fields[5]);
        int antecedentCharEnd = Integer.parseInt(fields[6]);

        String triggerType = fields[7];
        String antecedentType = fields[8];
        String sourTargetPattern = fields[9];


        // If the ellipsis and the antecedent are in 2 different sentences put them in the same TA
        String wsjLine = wsjLines.get(ellipsisFile).get(ellipsisLine);

        if (ellipsisLine != antecedentLine) {
            return null;
//            String wsjLineAntecedent = wsjLines.get(ellipsisFile).get(antecedentLine);
//            wsjLine = wsjLineAntecedent + System.lineSeparator() + wsjLine;
//
//            assert (ellipsisLine > antecedentLine) : "Antecedent after ellipsis";
//
//            // Adjust the character offset to account for the added sentence
//            ellipsisCharStart += wsjLineAntecedent.length() + 1;
//            ellipsisCharEnd += wsjLineAntecedent.length() + 1;
        }
        TextAnnotation ta = annotationBuilder.createTextAnnotation("wsj", String.valueOf(ellipsisLine), wsjLine);

        int ellipsisStartToken = ta.getTokenIdFromCharacterOffset(ellipsisCharStart);
        int ellipsisEndToken = ta.getTokenIdFromCharacterOffset(ellipsisCharEnd);
        int antecedentStartToken = ta.getTokenIdFromCharacterOffset(antecedentCharStart);
        int antecedentEndToken = ta.getTokenIdFromCharacterOffset(antecedentCharEnd);

        // Add the pred-arg view
        PredicateArgumentView ellipsisView = new PredicateArgumentView(viewName, ta);
        Constituent trigger = new Constituent("Trigger", viewName, ta,
                ellipsisStartToken, ellipsisEndToken);
        trigger.addAttribute(TRIGGER_TYPE, triggerType);

        Constituent antecedent = new Constituent("Antecedent", viewName, ta,
                antecedentStartToken, antecedentEndToken);
        antecedent.addAttribute(ANTECEDENT_TYPE, antecedentType);

        ellipsisView.addPredicateArguments(trigger, Collections.singletonList(antecedent),
                new String[]{sourTargetPattern}, new double[]{1.0});
        ta.addView(viewName, ellipsisView);
        return ta;
    }
}
