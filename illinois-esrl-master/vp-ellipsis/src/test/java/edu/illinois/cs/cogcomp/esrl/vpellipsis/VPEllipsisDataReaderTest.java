package edu.illinois.cs.cogcomp.esrl.vpellipsis;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import org.junit.Test;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class VPEllipsisDataReaderTest {

    @Test
    public void testReader() throws Exception {
        VPEllipsisDataReader reader = new ArgumentReader("00,01", "dev");
        Constituent arg = (Constituent) reader.next();
        assertEquals("tied directly to Mr. Cray", arg.getTokenizedSurfaceForm());
        assertEquals("soa", arg.getIncomingRelations().get(0).getRelationName());

        reader.next(); reader.next();
        arg = (Constituent) reader.next();
        assertEquals("did", arg.getIncomingRelations().get(0).getSource().getTokenizedSurfaceForm());
        assertEquals("fell in September", arg.getTokenizedSurfaceForm());
        assertEquals("vp", arg.getAttribute(VPEllipsisDataReader.ANTECEDENT_TYPE));
    }

    @Test
    public void testReadData() throws Exception {
        Map<String, TextAnnotation> wsjSents = new HashMap<>();
        String dir = "00";
        String file = ESRLConfigurator.WSJ_RAW_DIR.value + "/" + dir + "/wsj_0018";

        // First we need to store the WSJ lines (as TextAnnotations) with their line number for reference
        List<String> lines = LineIO.read(file);

        assertEquals(53, lines.size());

        for (int lineNum = 1; lineNum < lines.size(); lineNum++) {
            String line = lines.get(lineNum);
            if (line.isEmpty()) continue;
            String key = IOUtils.getFileName(file) + " " + (lineNum + 1);
            List<String[]> tokenizedSentences = Collections.singletonList(line.split("\\s+"));
            wsjSents.put(key, BasicTextAnnotationBuilder.
                    createTextAnnotationFromTokens("wsj", key, tokenizedSentences));
        }

        assertEquals(34, wsjSents.size());

        // Now we need to read the VPE file and get the references
        String vpDataDir = getCorrectPath(ESRLConfigurator.VP_ELLIPSIS_DATA_DIR.value);
        lines = LineIO.read(vpDataDir + "/" + dir);
        assertEquals(32, lines.size());

        String line = lines.get(0);

        String[] fields = line.split("\\s+");

        assertEquals(21, fields.length);

        String ellipsisKey = fields[0] + " " + fields[1];
        int ellipsisCharStart = Integer.parseInt(fields[2]);
        int ellipsisCharEnd = Integer.parseInt(fields[3]) + 1;
        String antecedentKey = fields[0] + " " + fields[4];
        int antecedentCharStart = Integer.parseInt(fields[5]);
        int antecedentCharEnd = Integer.parseInt(fields[6]) + 1;
        String triggerType = fields[7];
        String antecedentType = fields[8];

        assertNotNull(wsjSents.get(ellipsisKey));
        assertNotNull(wsjSents.get(antecedentKey));

        TextAnnotation ta = wsjSents.get(ellipsisKey);
        int ellipsisStartToken = ta.getTokenIdFromCharacterOffset(ellipsisCharStart);
        int ellipsisEndToken = ta.getTokenIdFromCharacterOffset(ellipsisCharEnd);
        String[] ellipsisTokens = ta.getTokensInSpan(ellipsisStartToken, ellipsisEndToken);
        assertEquals("is", ellipsisTokens[0]);

        int antecedentStartToken = ta.getTokenIdFromCharacterOffset(antecedentCharStart);
        int antecedentEndToken = ta.getTokenIdFromCharacterOffset(antecedentCharEnd);
        String[] antecedentTokens = ta.getTokensInSpan(antecedentStartToken, antecedentEndToken);
        assertArrayEquals("tied directly to Mr. Cray,".split(" "), antecedentTokens);

        assertEquals("be", triggerType);
        assertEquals("vped", antecedentType);
    }

    private String getCorrectPath(String file) {
        if (file.contains("data") && !IOUtils.exists(file)) {
            int dataIndex = file.indexOf("data") - 1;
            int prevSlashIndex = file.lastIndexOf(File.separator, dataIndex - 1);
            file = file.substring(0, prevSlashIndex) + file.substring(dataIndex, file.length());
        }
        return file;
    }
}