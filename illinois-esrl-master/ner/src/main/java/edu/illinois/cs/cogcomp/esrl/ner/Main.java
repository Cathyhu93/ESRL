package edu.illinois.cs.cogcomp.esrl.ner;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;

import java.io.File;

public class Main {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.NER_MUC_DATA_DIR);
    private String viewName = ViewNames.NER_ONTONOTES;

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelName = modelsDir + File.separator + NERClassifier.CLASS_NAME;


    public void train() {
        NERClassifier classifier = new NERClassifier(modelName + ".lc", modelName + ".lex");
        NERDataReader trainDataReader = new NERDataReader(dataDir + "/Train", "train", viewName);
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader, 10000);
        trainer.train(50);
        classifier.save();
        trainDataReader.close();
    }

    public void test() {
        NERClassifier classifier = new NERClassifier(modelName + ".lc", modelName + ".lex");
        NERDataReader testDataReader = new NERDataReader(dataDir + "/Test", "test", viewName);
        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        TestDiscrete.testDiscrete(tester, classifier, new NERClassifier.Label(), testDataReader, true, 10000);
        testDataReader.close();
    }

    public static void main(String[] args) throws AnnotatorException {
        Main trainer = new Main();
        trainer.train();
        trainer.test();
    }
}
