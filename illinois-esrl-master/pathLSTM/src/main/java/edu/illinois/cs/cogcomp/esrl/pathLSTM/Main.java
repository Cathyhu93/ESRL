package edu.illinois.cs.cogcomp.esrl.pathLSTM;


import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.esrl.core.AbstractClassifier;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.nomsrl.NomSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.nomsrl.inference.ConstrainedNomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.learn.Lexicon;
import edu.illinois.cs.cogcomp.pipeline.server.ServerClientAnnotator;
import se.lth.cs.srl.pipeline.LBJavaArgumentClassifier;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;


public class Main{



    String[] pathLSTMArgs = {"uiuc",
            "/dev/null", "models/uiuc+vecs.model",
            "-pos", "N", "-nopi",
            "-reranker",  "/dev/null"};
    LBJavaArgumentClassifier nomSRL = new LBJavaArgumentClassifier(pathLSTMArgs);

    private void train() {


        NomSRLArgumentReader trainReader = new NomSRLArgumentReader("train", false);
        BatchTrainer trainer = new BatchTrainer(nomSRL, trainReader, 10000);
        trainer.train(50);//50
        nomSRL.save();
        trainReader.close();

    }
    private void test(){

        NomSRLArgumentReader testDataReader = new NomSRLArgumentReader("test", false);
        TestDiscrete tester = new TestDiscrete();
        tester.addNull("0");
        TestDiscrete.testDiscrete(tester, nomSRL, new NomSRLArgumentClassifier.Label(), testDataReader, true, 10000);
        testDataReader.close();


    }

    private void test_online() throws Exception
    {
        ServerClientAnnotator annotator = new ServerClientAnnotator();
        annotator.setUrl("http://bronte.cs.illinois.edu", "8009"); // set the url and port name of your server here
        annotator.setViews("PATH_LSTM_SRL"); // specify the views that you want
        TextAnnotation ta = annotator.annotate("This is the best sentence ever.");
        System.out.println(ta.getAvailableViews()); // here you should see that the required views are added

    }



    public static void main(String[] args) throws Exception {

       // Main main = new Main();
        //main.test_online();
        ServerClientAnnotator annotator = new ServerClientAnnotator();
        annotator.setUrl("http://bronte.cs.illinois.edu", "8009"); // set the url and port name of your server here
        annotator.setViews("PATH_LSTM_SRL"); // specify the views that you want
        TextAnnotation ta = annotator.annotate("This is the best sentence ever.");
        System.out.println(ta.getAvailableViews()); //
    }
}







