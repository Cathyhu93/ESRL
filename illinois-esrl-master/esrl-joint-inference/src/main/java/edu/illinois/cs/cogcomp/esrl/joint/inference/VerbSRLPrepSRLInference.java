package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.core.features.Contains;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.prepsrl.PrepSRLDataReader;
import edu.illinois.cs.cogcomp.esrl.prepsrl.PrepSRLClassifier;
import edu.illinois.cs.cogcomp.esrl.verbsrl.VerbSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.illinois.cs.cogcomp.esrl.core.features.Contains.containsPrep;
import static edu.illinois.cs.cogcomp.esrl.core.features.Contains.isAt;

/**
 * Joint inference definition for Verb and PrepSRL
 */
public class VerbSRLPrepSRLInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String dataDir = rm.getString(ESRLConfigurator.PREP_DATA_DIR);

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameVerbSRL = modelsDir + File.separator + VerbSRLArgumentClassifier.CLASS_NAME;
    private static String modelNamePrepSRL = modelsDir + File.separator + PrepSRLClassifier.CLASS_NAME;
    // TODO: 7/15/16 This is the unconstrained version of the classifier
    private static VerbSRLArgumentClassifier verbSRL = new VerbSRLArgumentClassifier(modelNameVerbSRL + ".lc", modelNameVerbSRL + ".lex");
    // TODO: 7/15/16 This is the unconstrained version of the classifier
    private static PrepSRLClassifier prepSRL = new PrepSRLClassifier(modelNamePrepSRL + ".lc", modelNamePrepSRL + ".lex");

    private static final VerbSRLArgumentReader verbSRLReader = new VerbSRLArgumentReader("train", true);
    private static final PrepSRLDataReader prepSRLReader = new PrepSRLDataReader(dataDir, "train");

    private static final Classifier verbSRLOracle = new VerbSRLArgumentClassifier.Label();
    private static final Classifier prepSRLOracle = new PrepSRLClassifier.Label();

    public VerbSRLPrepSRLInference(Constituent head) {
        super(head);
        List<Constituent> srlConstituents = VerbSRLArgumentReader.getAllArgumentInstances(head);

        Object[] context = {head};

        // First, check that a preposition exists
        FirstOrderConstraint at = new FirstOrderConstant(isAt.classify(head).toString().equals(Contains.YValue));
        FirstOrderConstraint prep = new FirstOrderConstant(containsPrep.classify(head).toString().equals(Contains.YValue));

        FirstOrderConstraint verbExistsTemp, prepTemporal, prepNumeric;
        verbExistsTemp = new ExistentialQuantifier("srlTemp", srlConstituents,
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, null), "AM-TMP", new LeftEAR(context)));
        prepTemporal = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(prepSRL, head), "Temporal");
        prepNumeric = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(prepSRL, head), "Numeric/Level");
        FirstOrderConstraint implicationTemp = new FirstOrderImplication(
                new FirstOrderConjunction(at, verbExistsTemp),
                new FirstOrderDisjunction(prepTemporal, prepNumeric));

        FirstOrderConstraint verbExistsLoc, prepExistsLoc, prepExistsDest;
        verbExistsLoc = new ExistentialQuantifier("srlLoc", srlConstituents,
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, null), "AM-LOC", new LeftEAR(context)));
        prepExistsLoc = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(prepSRL, head), "Location");
        prepExistsDest = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(prepSRL, head), "Destination");
        FirstOrderConstraint implicationLoc = new FirstOrderImplication(
                new FirstOrderConjunction(prep, verbExistsLoc),
                new FirstOrderDisjunction(prepExistsLoc, prepExistsDest));

        this.constraint =  new FirstOrderConjunction(implicationLoc, implicationTemp);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(verbSRL.toString(), 0.5);
        alphas.put(prepSRL.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(VerbSRLPrepSRLInference.class, alphas);
        jointTrainer.trainWeightsOnline(2, prepSRL, verbSRL, prepSRLReader, verbSRLReader, prepSRLOracle, verbSRLOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        tester.addNull("missed");
        verbSRLReader.setDataset("test");
        jointTrainer.test(tester, verbSRL, verbSRLReader, verbSRLOracle, false);

        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        prepSRLReader.setDataset("test");
        jointTrainer.test(tester, prepSRL, prepSRLReader, prepSRLOracle, false);

    }
}
