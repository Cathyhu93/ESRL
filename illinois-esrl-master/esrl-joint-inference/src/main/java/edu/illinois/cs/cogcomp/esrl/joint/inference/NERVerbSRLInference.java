package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.ner.NERClassifier;
import edu.illinois.cs.cogcomp.esrl.ner.NERDataReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.VerbSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Joint inference definition for NER and VerbSRL
 */
public class NERVerbSRLInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String dataDir = rm.getString(ESRLConfigurator.NER_ONTONOTES_DATA_DIR);
    private static String viewName = ViewNames.NER_ONTONOTES;

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameNER = modelsDir + File.separator + NERClassifier.CLASS_NAME;
    private static String modelNameSRL = modelsDir + File.separator + VerbSRLArgumentClassifier.CLASS_NAME;
    // TODO: 4/19/16 This is the unconstrained version of the classifier
    private static VerbSRLArgumentClassifier verbSRL = new VerbSRLArgumentClassifier(modelNameSRL + ".lc", modelNameSRL + ".lex");
    private static NERClassifier ner = new NERClassifier(modelNameNER + ".lc", modelNameNER + ".lex");

    private static final NERDataReader nerReader = new NERDataReader(dataDir + "/Train", "train", viewName);
    private static final VerbSRLArgumentReader verbSRLReader = new VerbSRLArgumentReader("train", true);

    private static final Classifier nerOracle = new NERClassifier.Label();
    private static final Classifier verbSRLOracle = new VerbSRLArgumentClassifier.Label();

    public NERVerbSRLInference(Constituent head) {
        super(head);
        List<Constituent> srlConstituents = VerbSRLArgumentReader.getAllArgumentInstances(head);

        Object[] context = {head};

        FirstOrderConstraint srlExistsTemp, nerExistsTime, nerExistsDate, nerPredictions;
        srlExistsTemp = new ExistentialQuantifier("srlTemp", srlConstituents,
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, null), "AM-TMP", new LeftEAR(context)));
        nerExistsTime = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(ner, head), "TIME");
        nerExistsDate = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(ner, head), "DATE");
        nerPredictions = new FirstOrderDisjunction(nerExistsTime, nerExistsDate);
        FirstOrderConstraint implicationTemp = new FirstOrderImplication(srlExistsTemp, nerPredictions);

        FirstOrderConstraint srlExistsLoc, nerExistsLoc;
        srlExistsLoc = new ExistentialQuantifier("srlLoc", srlConstituents,
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, null), "AM-LOC", new LeftEAR(context)));
        nerExistsLoc = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(ner, head), "LOC");
        FirstOrderConstraint implicationLoc = new FirstOrderImplication(srlExistsLoc, nerExistsLoc);

        FirstOrderConstraint srlExistsA0, srlExistsA1, srlExistsA2, srlExistsCore, nerExistsPerson;
        srlExistsA0 = new ExistentialQuantifier("srlA0", srlConstituents,
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, null), "A0", new LeftEAR(context)));
        srlExistsA1 = new ExistentialQuantifier("srlA1", srlConstituents,
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, null), "A1", new LeftEAR(context)));
        srlExistsA2 = new ExistentialQuantifier("srlA2", srlConstituents,
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, null), "A2", new LeftEAR(context)));
        srlExistsCore = new FirstOrderDisjunction(srlExistsA0, new FirstOrderDisjunction(srlExistsA1, srlExistsA2));
        nerExistsPerson = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(ner, head), "PERSON");
        FirstOrderConstraint implicationPerson = new FirstOrderImplication(nerExistsPerson, srlExistsCore);

        FirstOrderConstraint allConstraints = new FirstOrderConjunction(implicationPerson,
                new FirstOrderConjunction(implicationLoc, implicationTemp));
        this.constraint = new FirstOrderConjunction(new FirstOrderConstant(true), allConstraints);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(ner.toString(), 0.5);
        alphas.put(verbSRL.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(NERVerbSRLInference.class, alphas);
        jointTrainer.trainWeightsOnline(2, ner, verbSRL, nerReader, verbSRLReader, nerOracle, verbSRLOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        nerReader.setDataset("test");
        jointTrainer.test(tester, ner, nerReader, nerOracle, false);

        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        tester.addNull("missed");
        verbSRLReader.setDataset("test");
        jointTrainer.test(tester, verbSRL, verbSRLReader, verbSRLOracle, false);
    }
}
