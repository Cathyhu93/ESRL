package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.ner.NERClassifier;
import edu.illinois.cs.cogcomp.esrl.ner.NERDataReader;
import edu.illinois.cs.cogcomp.esrl.temporal.TemporalClassifier;
import edu.illinois.cs.cogcomp.esrl.temporal.TemporalDataReader;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Joint inference definition for NER and VerbSRL
 */
public class TemporalNERInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameNER = modelsDir + File.separator + NERClassifier.CLASS_NAME;
    private static String modelNameTemporal = modelsDir + File.separator + TemporalClassifier.CLASS_NAME;
    private static TemporalClassifier temporal = new TemporalClassifier(modelNameTemporal + ".lc", modelNameTemporal + ".lex");
    private static NERClassifier ner = new NERClassifier(modelNameNER + ".lc", modelNameNER + ".lex");

    private static final NERDataReader nerReader = new NERDataReader(
            rm.getString(ESRLConfigurator.NER_ONTONOTES_DATA_DIR) + "/Train", "train", ViewNames.NER_ONTONOTES);
    private static final TemporalDataReader temporalReader = new TemporalDataReader(
            rm.getString(ESRLConfigurator.TEMPORAL_DATA_DIR) + "/trainingData.txt", "train");

    private static final Classifier nerOracle = new NERClassifier.Label();
    private static final Classifier temporalOracle = new TemporalClassifier.Label();

    public TemporalNERInference(Constituent head) {
        super(head);
        FirstOrderConstraint nerDate, nerTime, nerDisjunction, tempDate, tempTime, tempDuration, tempDisjunction;
        nerDate = getBILabel(head, "DATE", ner);
        nerTime = getBILabel(head, "TIME", ner);
        nerDisjunction = new FirstOrderDisjunction(nerDate, nerTime);
        tempDate = getBILabel(head, "DATE", temporal);
        tempTime = getBILabel(head, "TIME", temporal);
        tempDuration = getBILabel(head, "DURATION", temporal);
        tempDisjunction = new FirstOrderDisjunction(tempDate, new FirstOrderDisjunction(tempTime, tempDuration));
        FirstOrderConstraint implication = new FirstOrderImplication(nerDisjunction, tempDisjunction);
        this.constraint = new FirstOrderConjunction(new FirstOrderConstant(true), implication);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(ner.toString(), 0.5);
        alphas.put(temporal.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(TemporalNERInference.class, alphas);

        jointTrainer.trainWeightsOnline(3, ner, temporal, nerReader, temporalReader, nerOracle, temporalOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        nerReader.setDataset("test");
        jointTrainer.test(tester, ner, nerReader, nerOracle, false);
        tester = new TestDiscrete();
        tester.addNull("O");
        temporalReader.setDataset("test");
        jointTrainer.test(tester, temporal, temporalReader, temporalOracle, false);
    }
}
