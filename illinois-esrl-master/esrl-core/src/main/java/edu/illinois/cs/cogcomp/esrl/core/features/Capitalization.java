package edu.illinois.cs.cogcomp.esrl.core.features;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.features.factory.WordFeatureExtractorFactory;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;

import java.util.Set;

public class Capitalization extends LBJavaFeatureExtractor {

    @Override
    public Set<Feature> getFeatures(Constituent instance) throws EdisonException {
        return WordFeatureExtractorFactory.capitalization.getFeatures(instance);
    }

    @Override
    public String getName() {
        return WordFeatureExtractorFactory.capitalization.getName();
    }
}
