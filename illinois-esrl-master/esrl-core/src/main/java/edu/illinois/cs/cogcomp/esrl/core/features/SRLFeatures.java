package edu.illinois.cs.cogcomp.esrl.core.features;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TreeView;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.features.factory.*;
import edu.illinois.cs.cogcomp.edison.features.helpers.PathFeatureHelper;
import edu.illinois.cs.cogcomp.edison.features.helpers.WordHelpers;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.edison.utilities.NomLexEntry;
import edu.illinois.cs.cogcomp.edison.utilities.NomLexReader;
import edu.illinois.cs.cogcomp.nlp.utilities.CollinsHeadFinder;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import java.util.List;
import java.util.Set;

public class SRLFeatures extends LBJavaFeatureExtractor {

    public static final String parseViewName = ViewNames.PARSE_GOLD;//PARSE_STANFORD
    private final FeatureExtractor fex;

    /** Needed for the LBJava generation. */
    @SuppressWarnings("unused")
    public SRLFeatures() {
        fex = null;
    }

    public SRLFeatures(FeatureExtractor fex) {
        this.fex = fex;
    }

    public static SRLFeatures path = new SRLFeatures(new ParsePath(parseViewName));

    public static SRLFeatures subcategorization = new SRLFeatures(new SubcategorizationFrame(parseViewName));

    public static SRLFeatures linearPosition = new SRLFeatures(new LinearPosition());

    public static SRLFeatures syntacticFrame = new SRLFeatures(new SyntacticFrame(parseViewName));

    public static SRLFeatures parseHeadWord = new SRLFeatures(new ParseHeadWordPOS(parseViewName));

    public static SRLFeatures parseSiblings = new SRLFeatures(new ParseSiblings(parseViewName));



    public static SRLFeatures phraseType = new SRLFeatures(new ParsePhraseType(parseViewName));

    public static SRLFeatures predLemma = new SRLFeatures(new FeatureExtractor() {
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {
            return WordFeatureExtractorFactory.lemma.getFeatures(c.getIncomingRelations().get(0).getSource());
        }

        @Override
        public String getName() {
            return "#pred-lmm";
        }
    });

    public static SRLFeatures nomlexType = new SRLFeatures(new FeatureExtractor() {

        NomLexClassFeature ncf = new NomLexClassFeature();
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {

            return ncf.getFeatures(c.getIncomingRelations().get(0).getSource());
        }

        @Override
        public String getName() {
            return "nomlextype";
        }
    });
    //public static SRLFeatures nomlexType = new SRLFeatures(new NomLexClassFeature());
    public static SRLFeatures predSubcat = new SRLFeatures(new FeatureExtractor(){

        SubcategorizationFrame sfr = new SubcategorizationFrame(parseViewName);
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException{

            return sfr.getFeatures(c.getIncomingRelations().get(0).getSource());
        }
        @Override
        public String getName(){
            return "#pred-subcat";
        }
    });

    public static SRLFeatures predVoice = new SRLFeatures(new FeatureExtractor() {
        FeatureExtractor fex = new VerbVoiceIndicator(parseViewName);
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {
            return fex.getFeatures(c.getIncomingRelations().get(0).getSource());
        }

        @Override
        public String getName() {
            return "#pred-voice";
        }
    });

    public static SRLFeatures predVerbClass = new SRLFeatures(new FeatureExtractor() {
        FeatureExtractor fex = new LevinVerbClassFeature();
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException{

            return fex.getFeatures(c.getIncomingRelations().get(0).getSource());
        }
        @Override
        public String getName(){
            return "pred-verbclass";
        }
    });


    public static SRLFeatures spanWords = new SRLFeatures(new FeatureExtractor() {
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {

            Set<Feature> features = new LinkedHashSet();
            int startSpan = c.getStartSpan();
            String startToken = c.getTextAnnotation().getToken(startSpan).toLowerCase().trim();
            features.add(DiscreteFeature.create("firstspanword:" + startToken));

            int endSpan = c.getEndSpan();
            String endToken = c.getTextAnnotation().getToken(endSpan-1).toLowerCase().trim();
            features.add(DiscreteFeature.create("endspanword:" + endToken));
            return features;
        }

        @Override
        public String getName() {
            return "span-firstlastWord";
        }
    });







    public static SRLFeatures ptypeAndPathLength = new SRLFeatures(FeatureUtilities.conjoin(new ParsePhraseTypeOnly(parseViewName), new ParsePathLength(parseViewName
    )));


    public static SRLFeatures predAndHeadWord = new SRLFeatures(new FeatureExtractor() {
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {
            Set<Feature>predFeatures = WordFeatureExtractorFactory.lemma.getFeatures(c.getIncomingRelations().get(0).getSource());

            TextAnnotation ta = c.getTextAnnotation();
            TreeView tree = (TreeView)ta.getView(parseViewName);

            Constituent phrase;
            try {
                phrase = tree.getParsePhrase(c);
            } catch (Exception var7) {
                throw new EdisonException(var7);
            }

            Set<Feature> headWordFeatures = new LinkedHashSet();
            int head = CollinsHeadFinder.getInstance().getHeadWordPosition(phrase);
            headWordFeatures.add(DiscreteFeature.create("hw:" + ta.getToken(head).toLowerCase().trim()));

            return FeatureUtilities.conjoin(predFeatures, headWordFeatures);

        }

        @Override
        public String getName() {
            return "pred&Hw";
        }
    });

    public static SRLFeatures predAndPosition= new SRLFeatures(new FeatureExtractor() {
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {
            Set<Feature>predFeatures = WordFeatureExtractorFactory.lemma.getFeatures(c.getIncomingRelations().get(0).getSource());

            DiscreteFeature CONTAINS = DiscreteFeature.create("C");
            DiscreteFeature AFTER = DiscreteFeature.create("A");
            DiscreteFeature BEFORE = DiscreteFeature.create("B");
            List<Relation> incomingRelation = c.getIncomingRelations();
            Set<Feature> positionFeatures = new LinkedHashSet();
            if (incomingRelation.size() > 0) {
                Constituent predicate = (incomingRelation.get(0)).getSource();
                if (predicate.getStartSpan() >= c.getEndSpan()) {
                    positionFeatures.add(BEFORE);
                } else if (c.getStartSpan() >= predicate.getEndSpan()) {
                    positionFeatures.add(AFTER);
                } else {
                    positionFeatures.add(CONTAINS);
                }
            }
            return FeatureUtilities.conjoin(predFeatures, positionFeatures);
        }

        @Override
        public String getName() {
            return "pred&Position";
        }
    });


    public static SRLFeatures predAndPath = new SRLFeatures(new FeatureExtractor() {
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {
            Set<Feature>predFeatures = WordFeatureExtractorFactory.lemma.getFeatures(c.getIncomingRelations().get(0).getSource());

            TextAnnotation ta = c.getTextAnnotation();
            TreeView parse = (TreeView)ta.getView(parseViewName);
            Set<Feature> pathFeatures = new LinkedHashSet();
            List<Relation> incomingRelations = c.getIncomingRelations();
            if (incomingRelations.size() > 0) {
                Constituent c1;
                Constituent c2;
                try {
                    c1 = parse.getParsePhrase(((Relation)incomingRelations.get(0)).getSource());
                    c2 = parse.getParsePhrase(c);
                } catch (Exception var14) {
                    throw new EdisonException(var14);
                }

                Pair<List<Constituent>, List<Constituent>> paths = PathFeatureHelper.getPathsToCommonAncestor(c1, c2, 400);
                List<Constituent> list = new ArrayList();

                for(int i = 0; i < (paths.getFirst()).size() - 1; ++i) {
                    list.add((paths.getFirst()).get(i));
                }

                Constituent top = (Constituent)((List)paths.getFirst()).get((paths.getFirst()).size() - 1);
                list.add(top);

                for(int i = (paths.getSecond()).size() - 2; i >= 0; --i) {
                    list.add((paths.getSecond()).get(i));
                }

                StringBuilder sb = new StringBuilder();

                for(int i = 0; i < (paths.getFirst()).size() - 1; ++i) {
                    Constituent cc = (Constituent)((List)paths.getFirst()).get(i);
                    sb.append(cc.getLabel());
                    sb.append("^");
                }

                String pathString = PathFeatureHelper.getPathString(paths, true, false);
                pathFeatures.add(DiscreteFeature.create(pathString));
            }
            return FeatureUtilities.conjoin(predFeatures, pathFeatures);
        }

        @Override
        public String getName() {
            return "pred&Path";
        }
    });




    public static SRLFeatures predicateAndPtype = new SRLFeatures(new FeatureExtractor() {
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {
            Set<Feature> predicateFeatures = new LinkedHashSet<>();
            predicateFeatures.add(DiscreteFeature.create("predicate:" + c.getIncomingRelations().get(0).getSource()));

            TextAnnotation ta = c.getTextAnnotation();

            TreeView tree = (TreeView) ta.getView(parseViewName);

            Constituent phrase;
            try {
                phrase = tree.getParsePhrase(c);
            } catch (Exception e) {
                throw new EdisonException(e);
            }

            Set<Feature> ptypeFeatures = new LinkedHashSet<>();

            if (phrase != null)
                ptypeFeatures.add(DiscreteFeature.create(phrase.getLabel()));
            return FeatureUtilities.conjoin(predicateFeatures, ptypeFeatures);

        }

        @Override
        public String getName() {
            return "predicate&Ptype";
        }
    });

    public static SRLFeatures predicateAndLastword = new SRLFeatures(new FeatureExtractor() {
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {
            Set<Feature> predicateFeatures = new LinkedHashSet<>();
            predicateFeatures.add(DiscreteFeature.create("predicate:" + c.getIncomingRelations().get(0).getSource()));

            Set<Feature> lastwordFeatures = new LinkedHashSet<>();
            int endSpan = c.getEndSpan();
            String endToken = c.getTextAnnotation().getToken(endSpan-1).toLowerCase().trim();
            lastwordFeatures.add(DiscreteFeature.create("endspanword:" + endToken));
            return FeatureUtilities.conjoin(predicateFeatures, lastwordFeatures);
        }

        @Override
        public String getName() {
            return "predicate&lastword";
        }
    });

    public static SRLFeatures nomtypeAndPosition = new SRLFeatures(new FeatureExtractor() {
        @Override
        public Set<Feature> getFeatures(Constituent c) throws EdisonException {

            DiscreteFeature DE_ADJECTIVAL = DiscreteFeature.create("nom-adj");
            DiscreteFeature DE_VERBAL = DiscreteFeature.create("nom-vb");

            Constituent predicate = c.getIncomingRelations().get(0).getSource();
            int tokenId = predicate.getEndSpan() - 1;
            TextAnnotation ta = predicate.getTextAnnotation();
            String predicateWord = ta.getToken(tokenId).toLowerCase().trim();
            String predicateLemma;
            if (predicate.hasAttribute(PredicateArgumentView.LemmaIdentifier))
                predicateLemma = predicate.getAttribute(PredicateArgumentView.LemmaIdentifier);
            else
                predicateLemma = WordHelpers.getLemma(ta, tokenId);

            NomLexReader nomLex = NomLexReader.getInstance();

            List<NomLexEntry> nomLexEntries = nomLex.getNomLexEntries(predicateWord, predicateLemma);

            Set<Feature> nomtypeFeatures = new LinkedHashSet<>();
            if (nomLexEntries.size() > 0) {
                for (NomLexEntry e : nomLexEntries) {
                    nomtypeFeatures.add(DiscreteFeature.create("nom-cls:" + e.nomClass));

                    if (NomLexEntry.VERBAL.contains(e.nomClass)) {
                        nomtypeFeatures.add(DE_VERBAL);
                        nomtypeFeatures.add(DiscreteFeature.create("nom-vb:" + e.verb));
                    } else if (NomLexEntry.ADJECTIVAL.contains(e.nomClass)) {
                        nomtypeFeatures.add(DE_ADJECTIVAL);
                        nomtypeFeatures.add(DiscreteFeature.create("nom-adj:" + e.adj));
                    }
                }
            }



            DiscreteFeature CONTAINS = DiscreteFeature.create("C");
            DiscreteFeature AFTER = DiscreteFeature.create("A");
            DiscreteFeature BEFORE = DiscreteFeature.create("B");
            List<Relation> incomingRelation = c.getIncomingRelations();
            Set<Feature> positionFeatures = new LinkedHashSet();
            if (incomingRelation.size() > 0) {
                predicate = (incomingRelation.get(0)).getSource();
                if (predicate.getStartSpan() >= c.getEndSpan()) {
                    positionFeatures.add(BEFORE);
                } else if (c.getStartSpan() >= predicate.getEndSpan()) {
                    positionFeatures.add(AFTER);
                } else {
                    positionFeatures.add(CONTAINS);
                }
            }

            return FeatureUtilities.conjoin(nomtypeFeatures, positionFeatures);
        }

        @Override
        public String getName() {
            return "nomtype&position";
        }
    });


    public static SRLFeatures chunkPathPattern = new SRLFeatures(new ChunkPathPattern(ViewNames.SHALLOW_PARSE));

    public static SRLFeatures containsNEG = new SRLFeatures(ChunkPropertyFeatureFactory.isNegated);

    public static SRLFeatures containsMOD = new SRLFeatures(ChunkPropertyFeatureFactory.hasModalVerb);

    @Override
    public Set<Feature> getFeatures(Constituent instance) throws EdisonException {
        if (fex == null) throw new RuntimeException("SRLFeatures should be accessed only via its static members");
        return fex.getFeatures(instance);

    }

    @Override
    public String getName() {
        if (fex == null) throw new RuntimeException("SRLFeatures should be accessed only via its static members");
        return fex.getName();
    }
}
