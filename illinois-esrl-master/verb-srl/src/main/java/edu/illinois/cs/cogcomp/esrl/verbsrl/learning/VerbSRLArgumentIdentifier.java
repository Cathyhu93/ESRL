package edu.illinois.cs.cogcomp.esrl.verbsrl.learning;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.core.AbstractClassifier;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features.*;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.DiscretePrimitiveStringFeature;
import edu.illinois.cs.cogcomp.lbjava.classify.Feature;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.lbjava.io.IOUtilities;
import edu.illinois.cs.cogcomp.lbjava.learn.SparseAveragedPerceptron;
import edu.illinois.cs.cogcomp.lbjava.learn.SparseNetworkLearner;

import java.io.File;

public class VerbSRLArgumentIdentifier extends AbstractClassifier {
    private static final String PACKAGE_NAME = "edu.illinois.cs.cogcomp.esrl.verbsrl.learning";
    public static final String CLASS_NAME = "VerbSRLArgumentIdentifier";

    private static VerbSRLArgumentIdentifier instance;

    public VerbSRLArgumentIdentifier(String modelPath, String lexiconPath) {
        super(PACKAGE_NAME + "." + CLASS_NAME, modelPath, lexiconPath);
        instance = this;
    }

    public static VerbSRLArgumentIdentifier getInstance() {
        return instance;
    }

    @Override
    protected void initialize() {
        containingPackage = PACKAGE_NAME;
        name = CLASS_NAME;
        setLabeler(new Label());
        setExtractor(new FeatureExtractor());
    }

    public static class FeatureExtractor extends AbstractClassifier.FeatureExtractor {
        public FeatureExtractor() {
            containingPackage = PACKAGE_NAME;
            name = CLASS_NAME + "$FeatureExtractor";
            featureSet = new Classifier[]{new wordFeatures(), new posFeatures(),
                    new shallowParseFeatures(), new identifierParseFeatures()};
        }
    }

    public static class Label extends AbstractClassifier.Label {
        public Label() {
            containingPackage = PACKAGE_NAME;
            name = CLASS_NAME + "$Label";
        }

        public String discreteValue(Object example) {
            return String.valueOf(!((Constituent) example).getLabel().equals("candidate"));
        }
    }
}

