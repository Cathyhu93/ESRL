package edu.illinois.cs.cogcomp.esrl.verbsrl;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.core.features.SRLFeatures;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentIdentifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.nlp.utilities.ParseTreeProperties;
import edu.illinois.cs.cogcomp.nlp.utilities.ParseUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class VerbArgumentDetector {
	private static final VerbSRLArgumentIdentifier identifier = VerbSRLArgumentIdentifier.getInstance();

	public List<Constituent> getArguments(Constituent predicate) {
		List<Constituent> output = new ArrayList<>();
		for (Constituent c : generateCandidates(predicate)) {
			// If this is not an argument according to the identifier
			if (identifier.discreteValue(c).equals("false")) continue;
			output.add(c);
		}
		return output;
	}

	public String getCandidateViewName() {
		return "XuePalmerHeuristicView";
	}

	public List<Constituent> generateCandidates(Constituent predicate) {
		Constituent predicateClone = predicate.cloneForNewView(getCandidateViewName());

		TextAnnotation ta = predicateClone.getTextAnnotation();
		int sentenceId = ta.getSentenceId(predicateClone);
		Tree<String> tree = ParseUtils.getParseTree(SRLFeatures.parseViewName, ta, sentenceId);

		if (SRLFeatures.parseViewName.equals(ViewNames.PARSE_GOLD)) {
			tree = ParseUtils.snipNullNodes(tree);
			tree = ParseUtils.stripFunctionTags(tree);
			tree = ParseUtils.stripIndexReferences(tree);
		}

		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils.getSpanLabeledTree(tree);

		int sentenceStart = ta.getSentence(sentenceId).getStartSpan();
		int predicatePosition = predicateClone.getStartSpan() - sentenceStart;

		Set<Constituent> out = new HashSet<>();

		List<Tree<Pair<String, IntPair>>> yield = spanLabeledTree.getYield();

		Tree<Pair<String, IntPair>> predicateTree = yield.get(predicatePosition);

		Tree<Pair<String, IntPair>> currentNode = predicateTree.getParent();

		boolean done = false;
		while (!done) {
			if (currentNode.isRoot())
				done = true;
			else {
				List<Constituent> candidates = new ArrayList<>();

				for (Tree<Pair<String, IntPair>> sibling : currentNode.getParent().getChildren()) {
					Pair<String, IntPair> siblingNode = sibling.getLabel();

					// do not take the predicate as the argument
					IntPair siblingSpan = siblingNode.getSecond();
					if (siblingSpan.equals(predicateClone.getSpan()))
						continue;

					// do not take any constituent including the predicate as an argument
					if ((predicatePosition >= siblingSpan.getFirst())
							&& (predicateClone.getEndSpan() <= siblingSpan.getSecond()))
						continue;

					String siblingLabel = siblingNode.getFirst();

					int start = siblingSpan.getFirst() + sentenceStart;
					int end = siblingSpan.getSecond() + sentenceStart;

					candidates.add(getNewConstituent(predicateClone, start, end));

					if (siblingLabel.startsWith("PP")) {
						for (Tree<Pair<String, IntPair>> child : sibling.getChildren()) {
							int candidateStart = child.getLabel().getSecond().getFirst() + sentenceStart;
							int candidateEnd = child.getLabel().getSecond().getSecond() + sentenceStart;

							candidates.add(getNewConstituent(predicateClone, candidateStart, candidateEnd));
						}
					}
				}
				out.addAll(candidates);

				currentNode = currentNode.getParent();
			}
		}

		// Punctuations maketh an argument not!
		List<Constituent> output = new ArrayList<>();
		for (Constituent c : out) {
			if (!ParseTreeProperties.isPunctuationToken(c.getSurfaceForm()))
				output.add(c);
		}

		return output;
	}

	protected Constituent getNewConstituent(String label, Constituent predicate, int start, int end) {
		Constituent newConstituent = new Constituent(label, 1.0, getCandidateViewName(),
				predicate.getTextAnnotation(), start, end);
		new Relation("ChildOf", predicate, newConstituent, 1.0);
		return newConstituent;
	}

	protected Constituent getNewConstituent(Constituent predicate, int start, int end) {
		return getNewConstituent(DataReader.CANDIDATE, predicate, start, end);
	}

	private void train() throws Exception {
		VerbSRLArgumentReader trainReader = new VerbSRLArgumentReader("train", false);
		BatchTrainer trainer = new BatchTrainer(identifier, trainReader, 10000);
		trainer.train(10);
		identifier.save();
	}

	private void test() throws Exception {
		VerbSRLArgumentReader testDataReader = new VerbSRLArgumentReader("test", false);
		TestDiscrete.testDiscrete(new TestDiscrete(), identifier, new VerbSRLArgumentIdentifier.Label(), testDataReader, true, 10000);
	}

	public static void main(String[] args) throws Exception {
		VerbArgumentDetector argumentDetector = new VerbArgumentDetector();
		argumentDetector.train();
		argumentDetector.test();
	}
}
