package edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features;

import edu.illinois.cs.cogcomp.esrl.core.features.Capitalization;
import edu.illinois.cs.cogcomp.esrl.core.features.SRLFeatures;
import edu.illinois.cs.cogcomp.esrl.core.features.WordBigrams;
import edu.illinois.cs.cogcomp.esrl.core.features.WordContextBigrams;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;

public class wordFeatures extends Classifier {
    private static final Capitalization Capitalization = new Capitalization();
    private static final WordBigrams WordBigrams = new WordBigrams();
    private static final WordContextBigrams WordContextBigrams = new WordContextBigrams();
    private static final SRLFeatures predLemma = SRLFeatures.predLemma;
    private static final SRLFeatures predVoice = SRLFeatures.predVoice;
    private static final SRLFeatures predVerbClass = SRLFeatures.predVerbClass;

    public wordFeatures() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features";
        name = "wordFeatures";
    }

    public FeatureVector classify(Object example) {
        FeatureVector result = new FeatureVector();
        result.addFeatures(Capitalization.classify(example));
        result.addFeatures(WordBigrams.classify(example));
        result.addFeatures(WordContextBigrams.classify(example));
        result.addFeatures(predLemma.classify(example));
        result.addFeatures(predVoice.classify(example));
        result.addFeatures(predVerbClass.classify(example));
        return result;
    }
}

