package edu.illinois.cs.cogcomp.esrl.verbsrl.inference;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.DiscretePrimitiveStringFeature;
import edu.illinois.cs.cogcomp.lbjava.classify.Feature;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.lbjava.infer.InferenceManager;

public class ConstrainedVerbSRLArgumentClassifier extends Classifier {
    private static final VerbSRLArgumentClassifier baseClassifier = VerbSRLArgumentClassifier.getInstance();

    public ConstrainedVerbSRLArgumentClassifier() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.verbsrl.inference";
        name = "ConstrainedVerbSRLArgumentClassifier";
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String getOutputType() {
        return "discrete";
    }


    public FeatureVector classify(Object example) {
        return new FeatureVector(featureValue(example));
    }

    public Feature featureValue(Object example) {
        String result = discreteValue(example);
        return new DiscretePrimitiveStringFeature(containingPackage, name, "", result, valueIndexOf(result),
                (short) allowableValues().length);
    }

    public String discreteValue(Object example) {
        Constituent head = SRLInference.findHead((Constituent) example);
        SRLInference inference = (SRLInference)
                InferenceManager.get("edu.illinois.cs.cogcomp.esrl.verbsrl.inference.SRLInference", head);

        if (inference == null) {
            inference = new SRLInference(head);
            InferenceManager.put(inference);
        }

        String result = null;

        try {
            result = inference.valueOf(baseClassifier, example);
        } catch (Exception e) {
            System.err.println("LBJava ERROR while evaluating classifier ConstrainedVerbSRLArgumentClassifier: " + e);
            e.printStackTrace();
            System.exit(1);
        }

        return result;
    }

    public FeatureVector[] classify(Object[] examples) {
        return super.classify(examples);
    }

    public int hashCode() {
        return "ConstrainedVerbSRLArgumentClassifier".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof ConstrainedVerbSRLArgumentClassifier;
    }
}

