package edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.verbsrl.VerbSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.util.List;

public class LegalRoles extends ParameterizedConstraint {
    private static final VerbSRLArgumentClassifier baseClassifier = VerbSRLArgumentClassifier.getInstance();

    public LegalRoles() {
        super("edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints.LegalRoles");
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String discreteValue(Object example) {
        Constituent predicate = (Constituent) example;

        boolean constraint = true;
        for (Constituent arg : VerbSRLArgumentReader.getArguments(predicate)) {
            constraint = false;
            for (String role : VerbSRLArgumentReader.getLegalRoles(predicate)) {
                constraint = baseClassifier.discreteValue(arg).equals(role) ||
                        baseClassifier.discreteValue(arg).equals("R-" + role) ||
                        baseClassifier.discreteValue(arg).equals("c-" + role);
            }
        }

        if (!constraint) return "false";

        return "true";
    }

    public int hashCode() {
        return "LegalRoles".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof LegalRoles;
    }

    public FirstOrderConstraint makeConstraint(Object example) {
        Constituent predicate = (Constituent) example;
        List<Constituent> arguments = VerbSRLArgumentReader.getArguments(predicate);

        Object[] context = new Object[1];
        context[0] = predicate;

        EqualityArgumentReplacer roleEAR = new EqualityArgumentReplacer(context) {
            public Object getLeftObject() {
                return quantificationVariables.get(0);
            }

            public String getRightValue() {
                return ((String) quantificationVariables.get(1));
            }
        };
        EqualityArgumentReplacer cRoleEAR = new EqualityArgumentReplacer(context) {
            public Object getLeftObject() {
                return quantificationVariables.get(0);
            }

            public String getRightValue() {
                return "C-" + quantificationVariables.get(1);
            }
        };
        EqualityArgumentReplacer rRoleEAR = new EqualityArgumentReplacer(context) {
            public Object getLeftObject() {
                return quantificationVariables.get(0);
            }

            public String getRightValue() {
                return "R-" + quantificationVariables.get(1);
            }
        };
        FirstOrderConstraint isRole = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), null, roleEAR);
        FirstOrderConstraint isCRole = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), null, cRoleEAR);
        FirstOrderConstraint isRRole = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), null, rRoleEAR);
        FirstOrderConstraint roleCR = new FirstOrderDisjunction(isCRole, isRRole);
        FirstOrderConstraint roleAny = new FirstOrderDisjunction(roleCR, isRole);

        FirstOrderConstraint existsRole = new ExistentialQuantifier("role", VerbSRLArgumentReader.getLegalRoles(predicate), roleAny);

        FirstOrderConstraint forAll = new UniversalQuantifier("arg", arguments, existsRole);

        return new FirstOrderConjunction(new FirstOrderConstant(true), forAll);
    }
}

