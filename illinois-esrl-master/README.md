# Extended Semantic Role Labeling

### Requirements 
* Java 1.7 or higher
* [Apache Maven](https://maven.apache.org/download.cgi)

### Datasets (for training individual models)
Copy the files under `/shared/corpora/notShared/esrl-data` in a directory called `data` under the root directory.

### Compiling the code
Compile the project from the root directory:
```
mvn clean compile
```

### Training the individual classifiers
Run the `Main.main()` method of each sub-project.

### Using the joint-inference models
See the files under `esrl-joint-inference/src/main/java/edu/illinois/cs/cogcomp/esrl/joint/inference`