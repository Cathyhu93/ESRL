package edu.illinois.cs.cogcomp.esrl.mwe;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Queries;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;

public class MWEDataReader extends DataReader {

    /**
     * The DataReader constructor for the Multi-word Expression task
     *
     * @param file The directory containing the train/test files
     */
    public MWEDataReader(String file, String corpusName) {
        super(file, corpusName, ESRLViewNames.MWEs);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TextAnnotation> readData() {
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        try {
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".converted");
                }
            };
            for (String subFile : IOUtils.lsFiles(file, filter)) {
                SAXBuilder builder = new SAXBuilder();
                Document doc = builder.build(subFile);
                List<Element> paragraphs = doc.getRootElement().getChild("context").getChildren();
                for (Element paragraph : paragraphs) {
                    String paragraphId = paragraph.getAttributeValue("pnum");
                    Map<IntPair, String> mweLabels = new HashMap<>();
                    List<String[]> tokenizedSentences = new ArrayList<>();
                    List<Element> sentences = paragraph.getChildren();
                    int sentenceOffset = 0;
                    for (int sentenceInd = 0; sentenceInd < sentences.size(); sentenceInd++) {
                        if (sentenceInd > 0)
                            // Add the length of the previous sentence to the global offset
                            sentenceOffset += tokenizedSentences.get(sentenceInd-1).length;

                        Element sentence = sentences.get(sentenceInd);
                        List<String> sentenceWords = new ArrayList<>();
                        List<Element> words = sentence.getChildren();
                        int wordOffset = 0;
                        for (int wordInd = 0; wordInd < words.size(); wordInd++) {
                            Element word = words.get(wordInd);
                            String wordString = word.getTextNormalize();
                            // If the word string contains _'s then it's a MWE
                            if (wordString.contains("_")) {
                                String[] splits = wordString.split("_");
                                Collections.addAll(sentenceWords, splits);
                                // Add the start and end index of the MWE
                                int startSpan = wordInd + wordOffset + sentenceOffset;
                                int endSpan = startSpan + splits.length;
                                // Add the length of the now split MWE to the sentence-level offset
                                wordOffset += splits.length - 1;
                                // If the MWE has an interesting label (person, group, etc) add it as a span label
                                String label = "MWE";
                                if (word.getAttributeValue("rdf") != null)
                                    label = word.getAttributeValue("rdf");
                                mweLabels.put(new IntPair(startSpan, endSpan), label);
                            }
                            else sentenceWords.add(wordString);
                        }
                        tokenizedSentences.add(sentenceWords.toArray(new String[sentenceWords.size()]));
                    }
                    TextAnnotation ta = BasicTextAnnotationBuilder
                            .createTextAnnotationFromTokens("SemCor", paragraphId, tokenizedSentences);
                    addMWEView(ta, mweLabels);
                    textAnnotations.add(ta);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Couldn't read " + file);
        }
        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        SpanLabelView chunkView = (SpanLabelView) ta.getView(ViewNames.SHALLOW_PARSE);
        List<Constituent> candidates = new ArrayList<>();

        for (Constituent chunk : chunkView.where(Queries.hasLabel("NP"))) {
            Constituent candidate = cleanUp(chunk);
            if (candidate.size() > 1) candidates.add(candidate);
        }

        return getFinalCandidates(ta.getView(viewName), candidates);
    }

    private Constituent cleanUp(Constituent chunk) {
        Constituent constituent = chunk.cloneForNewView(viewName);
        // Remove chunk-initial determiners, possessives or non-word characters (e.g. punctuation)
        if (constituent.toString().matches("(^[Tt]he|^[Aa]n*|^'s|^\\p{Punct}).*")) {
            constituent = new Constituent(constituent.getLabel(), viewName, constituent.getTextAnnotation(),
                    constituent.getStartSpan()+1, constituent.getEndSpan());
        }
        // Remove chunk-final determiners, possessives or non-word characters (e.g. punctuation)
        if (constituent.toString().matches(".*('s$|\\p{Punct}$)")) {
            constituent = new Constituent(constituent.getLabel(), viewName, constituent.getTextAnnotation(),
                    constituent.getStartSpan(), constituent.getEndSpan()-1);
        }
        return constituent;
    }

    private void addMWEView(TextAnnotation ta, Map<IntPair, String> labels) {
        SpanLabelView mweView = new SpanLabelView(viewName, ta);
        for (Map.Entry<IntPair, String> label : labels.entrySet()) {
            mweView.addSpanLabel(label.getKey().getFirst(), label.getKey().getSecond(), label.getValue(), 1.0);
        }
        ta.addView(viewName, mweView);
    }
}
