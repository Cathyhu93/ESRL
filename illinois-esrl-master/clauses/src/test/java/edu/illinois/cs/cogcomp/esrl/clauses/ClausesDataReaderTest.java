package edu.illinois.cs.cogcomp.esrl.clauses;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClausesDataReaderTest {
	private String dataDir;

	@Before
	public void setUp() throws Exception {
		ResourceManager rm = ESRLConfigurator.defaults();
		dataDir = rm.getString(ESRLConfigurator.CLAUSES_DATA_DIR);
	}

	@Test
	public void testReader() throws Exception {
		ClausesDataReader reader = new ClausesDataReader(dataDir + "/testa", "test");
		Constituent candidate = (Constituent) reader.next();
		assertEquals(3, candidate.size());

        // Skip a few known candidates
        for (int i = 0; i < 63; i++) reader.next();

        Constituent clause = ((Constituent) reader.next());
        assertEquals("S", clause.getLabel());
        assertEquals(9, clause.size());
	}
}