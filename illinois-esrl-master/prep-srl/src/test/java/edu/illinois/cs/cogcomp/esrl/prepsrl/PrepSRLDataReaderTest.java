package edu.illinois.cs.cogcomp.esrl.prepsrl;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrepSRLDataReaderTest {
    @Test
    public void readData() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        DataReader reader = new PrepSRLDataReader(rm.getString(ESRLConfigurator.PREP_DATA_DIR), "test");
        assertTrue(reader.next() != null);
        Constituent c = (Constituent) reader.next();
        assertEquals(DataReader.CANDIDATE, c.getLabel());
        reader.next();
        reader.next();
        assertEquals("ObjectOfVerb", ((Constituent) reader.next()).getLabel());
    }

}