package edu.illinois.cs.cogcomp.esrl.prepsrl.inference.constraints;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.lbjava.infer.FirstOrderConstraint;
import edu.illinois.cs.cogcomp.lbjava.infer.ParameterizedConstraint;

public class PrepSRLInferenceConstraints extends ParameterizedConstraint {
    private static final LegalRoles legalRoles = new LegalRoles();

    public PrepSRLInferenceConstraints() {
        super("edu.illinois.cs.cogcomp.esrl.prepsrl.inference.constraints.PrepSRLInferenceConstraints");
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String discreteValue(Object example) {
        Constituent phrase = (Constituent) example;

        boolean constraint = legalRoles.discreteValue(phrase).equals("true");
        if (!constraint) return "false";

        return "true";
    }

    public int hashCode() {
        return "PrepSRLInferenceConstraints".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof PrepSRLInferenceConstraints;
    }

    public FirstOrderConstraint makeConstraint(Object example) {
        Constituent phrase = (Constituent) example;
        return legalRoles.makeConstraint(phrase);
    }
}