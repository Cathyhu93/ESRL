package edu.illinois.cs.cogcomp.esrl.prepsrl.inference;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.prepsrl.PrepSRLClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.DiscretePrimitiveStringFeature;
import edu.illinois.cs.cogcomp.lbjava.classify.Feature;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.lbjava.infer.InferenceManager;

import java.io.File;

public class ConstrainedPrepSRLClassifier extends Classifier {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelName = modelsDir + File.separator + PrepSRLClassifier.CLASS_NAME;
    private static final PrepSRLClassifier prepSRLClassifier =
            new PrepSRLClassifier(modelName + ".lc", modelName + ".lex");

    public ConstrainedPrepSRLClassifier() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.prepsrl";
        name = "ConstrainedPrepSRLClassifier";
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String getOutputType() {
        return "discrete";
    }


    public FeatureVector classify(Object example) {
        return new FeatureVector(featureValue(example));
    }

    public Feature featureValue(Object example) {
        String result = discreteValue(example);
        return new DiscretePrimitiveStringFeature(containingPackage, name, "", result, valueIndexOf(result), (short) allowableValues().length);
    }

    public String discreteValue(Object example) {
        Constituent head = PrepSRLInference.findHead((Constituent) example);
        PrepSRLInference inference = (PrepSRLInference)
                InferenceManager.get("edu.illinois.cs.cogcomp.esrl.prepsrl.inference.PrepSRLInference", head);

        if (inference == null) {
            inference = new PrepSRLInference(head);
            InferenceManager.put(inference);
        }

        String result = null;

        try {
            result = inference.valueOf(prepSRLClassifier, example);
        } catch (Exception e) {
            System.err.println("LBJava ERROR: Fatal error while evaluating classifier ConstrainedPrepSRLClassifier: " + e);
            e.printStackTrace();
            System.exit(1);
        }

        return result;
    }

    public int hashCode() {
        return "ConstrainedPrepSRLClassifier".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof ConstrainedPrepSRLClassifier;
    }
}