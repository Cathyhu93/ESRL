package edu.illinois.cs.cogcomp.esrl.nomsrl;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.nomsrl.inference.ConstrainedNomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentIdentifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;

import java.io.File;

public class Main {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String identifierModelName = modelsDir + File.separator + NomSRLArgumentIdentifier.CLASS_NAME;
    private static String classifierModelName = modelsDir + File.separator + NomSRLArgumentClassifier.CLASS_NAME;

    private final NomSRLArgumentIdentifier identifier = new NomSRLArgumentIdentifier(identifierModelName + ".lc", identifierModelName + ".lex");
    private final NomSRLArgumentClassifier classifier = new NomSRLArgumentClassifier(classifierModelName + ".lc", classifierModelName + ".lex");

    private boolean useArgPredictor = !rm.getBoolean(ESRLConfigurator.SRL_USE_GOLD);


    private void train() {

        NomSRLArgumentReader trainReader = new NomSRLArgumentReader("train", false);
        BatchTrainer trainer = new BatchTrainer(identifier, trainReader, 10000);
        trainer.train(50); //50
        identifier.save();
        trainReader.close();

        trainReader = new NomSRLArgumentReader("train", useArgPredictor);
        trainer = new BatchTrainer(classifier, trainReader, 10000);
        trainer.train(50);//50
        classifier.save();
        trainReader.close();

    }

    private void test() throws Exception {
        // The constrained classifier contains a definition of NomSRLArgumentClassifier
        // (initialised with the same model path as above)
        ConstrainedNomSRLArgumentClassifier constClassifier = new ConstrainedNomSRLArgumentClassifier();
        NomSRLArgumentReader testDataReader = new NomSRLArgumentReader("test", useArgPredictor);
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        tester.addNull("missed");
        TestDiscrete.testDiscrete(tester, constClassifier, new NomSRLArgumentClassifier.Label(), testDataReader, true, 10000);
        testDataReader.close();


    }

    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.train();
        main.test();
    }
}
