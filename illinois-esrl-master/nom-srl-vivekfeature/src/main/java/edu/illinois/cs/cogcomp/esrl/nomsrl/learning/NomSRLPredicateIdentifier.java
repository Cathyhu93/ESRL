package edu.illinois.cs.cogcomp.esrl.nomsrl.learning;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.core.AbstractClassifier;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features.identifierParseFeatures;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features.posFeatures;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features.shallowParseFeatures;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features.wordFeatures;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;

public class NomSRLPredicateIdentifier extends AbstractClassifier {
    private static final String PACKAGE_NAME = "edu.illinois.cs.cogcomp.esrl.nomsrl.learning";
    public static final String CLASS_NAME = "NomSRLPredicateIdentifier";

    public NomSRLPredicateIdentifier(String modelPath, String lexiconPath) {
        super(PACKAGE_NAME + "." + CLASS_NAME, modelPath, lexiconPath);
    }

    @Override
    protected void initialize() {
        containingPackage = PACKAGE_NAME;
        name = CLASS_NAME;
        setLabeler(new Label());
        setExtractor(new FeatureExtractor());
    }

    public static class FeatureExtractor extends AbstractClassifier.FeatureExtractor {
        public FeatureExtractor() {
            containingPackage = PACKAGE_NAME;
            name = CLASS_NAME + "$FeatureExtractor";
            featureSet = new Classifier[]{new wordFeatures(), new posFeatures(),
                    new shallowParseFeatures(), new identifierParseFeatures()};
        }
    }

    public static class Label extends AbstractClassifier.Label {
        public Label() {
            containingPackage = PACKAGE_NAME;
            name = CLASS_NAME + "$Label";
        }

        public String discreteValue(Object example) {
            return String.valueOf(!((Constituent) example).getLabel().equals("candidate"));
        }
    }
}
