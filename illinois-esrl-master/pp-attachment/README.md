# Prepositional Phrase Attachment

## Dataset
A modified version of the data used for the model described in:

Ratnaparkhi, Adwait (1994). [A Maximum Entropy Model for Prepositional
Phrase Attachment](http://www.cis.upenn.edu/~adwait/papers/hlt94.ps).
Proceedings of the ARPA Human Language Technology Conference.

The original dataset is part of the NLTK distribution and can be found here:
[http://sourceforge.net/projects/nltk/files/OldFiles/ppattach.zip/download]
(http://sourceforge.net/projects/nltk/files/OldFiles/ppattach.zip/download)