package edu.illinois.cs.cogcomp.esrl.ppa;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Queries;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.nlp.utilities.ParseUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PPADataReader extends DataReader {

    public PPADataReader(String file, String corpusName) {
        super(file, corpusName, ESRLViewNames.PP_ATTACHMENT);
    }

    @Override
    public List<TextAnnotation> readData(){
        List<String> lines;
        try {
            lines = LineIO.read(file);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't read " + file);
        }
        // First line is always ##########
        if (!lines.get(0).startsWith("#")) {
            throw new IllegalArgumentException("First line needs to be ##########");
        }

        List<TextAnnotation> textAnnotations = new ArrayList<>();

        lines.remove(0);
		int sentId = 0;
        for (Iterator<String> iterator = lines.iterator(); iterator.hasNext(); ) {
            String line = iterator.next();
            // This is our current sentence
            String[] tokens = line.split("\\s+");
            List<String[]> tokenizedSentence = Collections.singletonList(tokens);
			String id = String.valueOf(sentId);
            TextAnnotation ta = BasicTextAnnotationBuilder.createTextAnnotationFromTokens("PPA", id, tokenizedSentence);
			sentId++;

            // Followed by a list of PPs starting with *
            List<String> phrases = new ArrayList<>();
            List<String> labels = new ArrayList<>();
            // Read all the PPs and their gold attachment
            while ((line = iterator.next()).startsWith("*")) {
                String phrase = line.substring(2);
                String label = phrase.substring(0, 1);
                phrase = phrase.substring(4);
                phrases.add(phrase);
                labels.add(label);
            }
            boolean emptyView = addGoldPPAView(ta, phrases, labels);
            // The view will be empty if it only had just postpositions-only spans
            if (!emptyView) textAnnotations.add(ta);
            // Now the reader has read the next ########## line
            // Which means that the next line is our next sentence
        }
        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        SpanLabelView goldClausesView = (SpanLabelView) ta.getView(viewName);
        //Starting with a PP chunk, get all the subsequent chunks until the end of the phrase
        SpanLabelView chunkView = (SpanLabelView) ta.getView(ViewNames.SHALLOW_PARSE);
        List<Constituent> candidates = new ArrayList<>();
        for (Constituent chunk : chunkView.getConstituents()) {
            if (chunk.getLabel().equals("PP"))
                candidates.addAll(getChunksAfter(chunkView, chunk));
        }
        return getFinalCandidates(goldClausesView, candidates);
    }

    private List<Constituent> getChunksAfter(SpanLabelView chunkView, Constituent ppChunk) {
        List<Constituent> candidates = new ArrayList<>();
        int start = ppChunk.getStartSpan();
        for (Constituent c : chunkView.where(Queries.after(ppChunk)).where(Queries.hasLabel("PP").negate().and(Queries.hasLabel("SBAR").negate()))) {
            candidates.add(new Constituent(CANDIDATE, viewName, ppChunk.getTextAnnotation(), start, c.getEndSpan()));
        }
        return candidates;
    }

    private boolean addGoldPPAView(TextAnnotation ta, List<String> pPhrases, List<String> labels) {
        // Need to allow overlapping spans
        String viewName = ESRLViewNames.PP_ATTACHMENT;
        SpanLabelView ppaView = new SpanLabelView(viewName, viewName+"-annotator", ta, 1.0, true);

        for (int i = 0; i < pPhrases.size(); i++) {
            String pPhrase = pPhrases.get(i);
            // Ignore phrases that contain only the preposition (usually when they are really postpositions)
            if (pPhrase.split("\\s+").length == 1) continue;

            // Find the phrase in the sentence (NB: assuming there is only one matching segment)
			IntPair tokenInd = getIndexFromCleanPhrase(ta, pPhrase);

            String label = labels.get(i);
            ppaView.addSpanLabel(tokenInd.getFirst(), tokenInd.getSecond(), label, 1.0);
        }
        ta.addView(ESRLViewNames.PP_ATTACHMENT, ppaView);
        return ppaView.getConstituents().isEmpty();
    }

    private IntPair getIndexFromCleanPhrase(TextAnnotation ta, String pPhrase) {
		int index = ta.getText().indexOf(pPhrase);
		if (index != -1) {
			int tokenStart = ta.getTokenIdFromCharacterOffset(index);
			int tokenEnd = tokenStart + pPhrase.split("\\s+").length;
			return new IntPair(tokenStart, tokenEnd);
		}

		pPhrase = ParseUtils.convertBracketsFromPTBFormat(pPhrase);
		index = ta.getText().indexOf(pPhrase);
		if (index != -1) {
			int tokenStart = ta.getTokenIdFromCharacterOffset(index);
			int tokenEnd = tokenStart + pPhrase.split("\\s+").length;
			return new IntPair(tokenStart, tokenEnd);
		}

		// There are some spurious 0s in the dataset
		pPhrase = pPhrase.replaceAll(" 0\\s*", " ");
		index = ta.getText().indexOf(pPhrase);
        if (index != -1) {
			int tokenStart = ta.getTokenIdFromCharacterOffset(index);
			int tokenEnd = tokenStart + pPhrase.split("\\s+").length;
			return new IntPair(tokenStart, tokenEnd);
		}

		pPhrase = pPhrase.replaceAll("``", "\"").replaceAll("''", "\"");
		index = ta.getText().indexOf(pPhrase);
		if (index != -1) {
			int tokenStart = ta.getTokenIdFromCharacterOffset(index);
			int tokenEnd = tokenStart + pPhrase.split("\\s+").length;
			return new IntPair(tokenStart, tokenEnd);
		}
		throw new AssertionError("Phrase (" + pPhrase + ") not found in text");
    }
}
