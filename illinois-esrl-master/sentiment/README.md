# Sentiment Analysis

## Dataset

### SemEval-2007 Task 14
Affective text. Data accessed from here:
[http://nlp.cs.swarthmore.edu/semeval/tasks/task14/data.shtml]
(http://nlp.cs.swarthmore.edu/semeval/tasks/task14/data.shtml)

Each line in the **emotion** annotations file follows the format:
           
    id  anger disgust fear joy sadness surprise 

where:

- the id is the headline identifier from the SGML formatted file
- each of the six emotions are indicated using a score of \[0, 100\]
(0 = the emotion is not present in the headline; 100 = the maximum 
amount of emotion is found in the headline)

Each line in the **valence** annotations file follows the format:
   
    id valence

where:

- the id is the headline identifier from the SGML formatted file
- the valence is indicated using a score in the interval \[-100, 100\]
where -100 means strongly negative, +10O means strongly positive, and
O means neutral.

see the [task's README file](../data/sentiment/affective-text/README) for more information.

### SemEval-2014 Task 4
Aspect Based Sentiment Analysis. Data accessed from here:
[http://alt.qcri.org/semeval2014/task4/index.php?id=data-and-tools]
(http://alt.qcri.org/semeval2014/task4/index.php?id=data-and-tools)