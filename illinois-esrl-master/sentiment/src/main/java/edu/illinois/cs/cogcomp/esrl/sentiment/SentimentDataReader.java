package edu.illinois.cs.cogcomp.esrl.sentiment;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.View;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
import edu.illinois.cs.cogcomp.nlp.utility.TokenizerTextAnnotationBuilder;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A data reader for both the SemEval-2007 (affective text) and SemEval-2014 (aspect-based) sentiment corpora
 * <br/>
 * <b>NB:</b> The datasets are <b>NOT</b> tokenized!
 */
public class SentimentDataReader extends DataReader {
    private static final Logger logger = LoggerFactory.getLogger(SentimentDataReader.class);

    /** Since the data is not tokenized, we need this */
    private static TokenizerTextAnnotationBuilder annotationBuilder;

    public SentimentDataReader(String file, String corpusName, String viewName) {
        super(file, corpusName, viewName);
    }

    @Override
    public List<TextAnnotation> readData() {
        List<TextAnnotation> textAnnotations;
        annotationBuilder = new TokenizerTextAnnotationBuilder(new IllinoisTokenizer());

        try {
            switch (viewName) {
                case ESRLViewNames.SENTIMENT_ASPECT:
                    textAnnotations = readAspectData(file);
                    break;
                case ESRLViewNames.SENTIMENT_AFFECT:
                    textAnnotations = readAffectData(file);
                    break;
                default:
                    throw new IllegalArgumentException("'viewName' should be either " + ESRLViewNames.SENTIMENT_ASPECT +
                            "or " + ESRLViewNames.SENTIMENT_AFFECT + ".");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Couldn't read " + file);
        }
        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        View goldView = ta.getView(viewName);
        View chunkView = ta.getView(ViewNames.SHALLOW_PARSE);
        View posView = ta.getView(ViewNames.POS);

        List<Constituent> candidates = new ArrayList<>();

        for (Constituent c : chunkView.getConstituents())
            if (c.getLabel().equals("NP") && c.size() > 1) candidates.add(c.cloneForNewView(viewName));
        for (Constituent c: posView.getConstituents())
            if (c.getLabel().startsWith("NN")) candidates.add(c.cloneForNewView(viewName));


        return getFinalCandidates(goldView, candidates);
    }

    @SuppressWarnings("unchecked")
    private List<TextAnnotation> readAspectData(String file) throws JDOMException, IOException {
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(file);
        List<Element> sentences = doc.getRootElement().getChildren();
        int skipped = 0;
        for (Element sentence : sentences) {
            String id = sentence.getAttributeValue("id");
            String text = sentence.getChild("text").getText();
            TextAnnotation ta = annotationBuilder.createTextAnnotation("aspect", id, text);
            Element termsList = sentence.getChild("aspectTerms");
            if (termsList == null) continue;
            List<Element> aspectTerms = termsList.getChildren();
            skipped += addAspectView(ta, aspectTerms);
            if (ta.getView(ESRLViewNames.SENTIMENT_ASPECT).getConstituents().isEmpty()) continue;
            textAnnotations.add(ta);
        }
        logger.info("Skipped {} sentences", skipped);
        return textAnnotations;
    }

    private int addAspectView(TextAnnotation ta, List<Element> aspectTerms) {
        int skipped = 0;
        SpanLabelView view = new SpanLabelView(ESRLViewNames.SENTIMENT_ASPECT, ta);
        for (Element aspectTerm : aspectTerms) {
            String charStart = aspectTerm.getAttributeValue("from");
            String charEnd = aspectTerm.getAttributeValue("to");
            int tokenStart, tokenEnd;
            try {
                tokenStart = ta.getTokenIdFromCharacterOffset(Integer.parseInt(charStart));
                tokenEnd = ta.getTokenIdFromCharacterOffset(Integer.parseInt(charEnd));
            } catch (IllegalArgumentException e) {
                skipped++;
                continue;
            }
            String polarityLabel = aspectTerm.getAttributeValue("polarity");
            view.addSpanLabel(tokenStart, tokenEnd, polarityLabel, 1.0);
        }
        ta.addView(ESRLViewNames.SENTIMENT_ASPECT, view);
        return skipped;
    }

    // TODO Finish this
    private List<TextAnnotation> readAffectData(String file) {
        logger.error("Not implemented yet");
        return null;
    }
}
