package edu.illinois.cs.cogcomp.esrl.quantities;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.DiscretePrimitiveStringFeature;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternFeatures extends Classifier {

    PatternFeatures() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.quantities";
        name = "PatternFeatures";
    }

    @Override
    public String getOutputType() {
        return "discrete%";
    }

    @Override
    public FeatureVector classify(Object example) {
        Constituent word = (Constituent) example;
        FeatureVector featureVector = new FeatureVector();

        String ordinal = "(?:" + "\\d+(?:st|nd|rd|th)" + "|first|second|third|fourth|fifth|sixth|seventh|eighth|ninth|tenth" + "|eleventh|twelfth|thirteenth|fourteenth|fifteenth|sixteenth" + "|seventeenth|eighteenth|nineteenth" + "|twentieth|thirtieth|fou?rtieth|fiftieth|sixtieth|seventieth" + "|eightieth|ninetieth" + "|hundredth|thousandth|millionth|billionth)";
        String fraction_denom = "(?:" + "half|halve|third|fourth|fifth|sixth|seventh|eighth|ninth|tenth" + "|eleventh|twelfth|thirteenth|fourteenth|fifteenth|sixteenth" + "|seventeenth|eighteenth|nineteenth" + "|twentieth|thirtieth|fou?rtieth|fiftieth|sixtieth|seventieth" + "|eightieth|ninetieth" + "|hundredth|thousandth|millionth|billionth)s";
        String writtenNumber = "twelve|seven|trillion|ten|seventeen|two|four|sixty|" + "zero|eighteen|thirteen|dozen|one|fourty|fifty|twenty" + "six|three|eleven|hundred|thousand|million|eighty" + "fourteen|five|nineteen|sixteen|fifteen|seventy|billion" + "thirty|ninety|nine|eight";
        String digits = "(\\d+)";
        String four_digits = "(\\d\\d\\d\\d)";
        String two_digits = "(\\d\\d)";
        String initial = "[A-Z]\\.";
        String abbrev = "([A-Z]?[a-z]+\\.)";
        String roman = "(M?M?M?(?:CM|CD|D?C?C?C?)(?:XC|XL|L?X?X?X?)(?:IX|IV|V?II?|III))";
        String numeric = "((?:\\d{1,3}(?:\\,\\d{3})*|\\d+)(?:\\.\\d+)?)";
        String doftw = "(?:Mon|Tues?|Wed(?:nes)?|Thurs?|Fri|Satui?r?|Sun)(?:day|\\.)";
        String month = "(?:jan(?:uary)?|febr?(?:uary)?|mar(?:ch)?|apr(?:il)?" + "|may|june?|july?|aug(?:ust)?|sept?(?:ember)?|oct(?:ober)?|nov(?:ember)?|" + "dec(?:ember)?)\\.?";
        String dayWords = "(?: today|tomorrow|yesterday|morning|afternoon|evening)";
        String possibleYear = "(?:\\d\\d\\d\\d(?:\\s*s)?|\\'?\\d\\d(?:\\s*?s)?)";
        String time = "(\\d\\d?)\\s*?(?:(\\:)?\\s*?(\\d\\d))?\\s*([ap]\\.m\\.?|[ap]m|[a" + "p])?\\s*(?:\\(?(GMT|EST|PST|CST)?\\)?)?(?:\\W|$)";

        addPatternFeature(word, featureVector, digits, "[digits]");
        addPatternFeature(word, featureVector, two_digits, "[two_digits]");
        addPatternFeature(word, featureVector, four_digits, "[four_digits]");
        addPatternFeature(word, featureVector, numeric, "[numeric]");
        addPatternFeature(word, featureVector, numeric, "[contains_numeric]");
        addPatternFeature(word, featureVector, writtenNumber, "[written_number]");
        addPatternFeature(word, featureVector, fraction_denom, "[fraction_denom]");
        addPatternFeature(word, featureVector, ordinal, "[ordinal]");
        addPatternFeature(word, featureVector, month, "[month]");
        addPatternFeature(word, featureVector, dayWords, "[day]");
        addPatternFeature(word, featureVector, doftw, "[day]");
        addPatternFeature(word, featureVector, possibleYear, "[year]");
        addPatternFeature(word, featureVector, time, "[time]");
        addPatternFeature(word, featureVector, roman, "[roman_numeral]");
        addPatternFeature(word, featureVector, initial, "[init_abbr]");
        addPatternFeature(word, featureVector, abbrev, "[init_abbr]");
        return featureVector;
    }

    private void addPatternFeature(Constituent word, FeatureVector featureVector, String regex, String id) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(word.getSurfaceForm());
        if (matcher.matches()) {
            featureVector.addFeature(new DiscretePrimitiveStringFeature(this.containingPackage, this.name, id,
                    "true", valueIndexOf("true"), (short) 0));
        }
    }
}
