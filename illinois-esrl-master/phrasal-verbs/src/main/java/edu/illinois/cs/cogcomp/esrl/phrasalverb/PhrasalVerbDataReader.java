package edu.illinois.cs.cogcomp.esrl.phrasalverb;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;

import java.io.FileNotFoundException;
import java.util.*;

/**
 * A {@link TextAnnotation}-based BIO data reader.
 */
public class PhrasalVerbDataReader extends DataReader {

    public PhrasalVerbDataReader(String file, String corpusName) {
        super(file, corpusName, ESRLViewNames.PHRASAL_VERB);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public List<TextAnnotation> readData() {
        String corpusId = IOUtils.getFileName(file);
        List<String> lines;
        try {
            lines = LineIO.read(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Couldn't read " + file);
        }
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        // token POS label
        List<String> tokens = new ArrayList<>();
        List<String> pos = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        int taId = 0;
        for (String line : lines) {
            if (line.isEmpty()) {
                List<String[]> tokenizedSentence = Collections.singletonList(tokens.toArray(new String[tokens.size()]));
                TextAnnotation ta = BasicTextAnnotationBuilder.createTextAnnotationFromTokens(
                        corpusId, String.valueOf(taId), tokenizedSentence);
                if (rm.getBoolean(ESRLConfigurator.USE_GOLD_POS))
                    addGoldPOSView(ta, pos);
                addGoldBIOView(ta, labels);
                textAnnotations.add(ta);
                tokens.clear();
                pos.clear();
                labels.clear();
                taId++;
            }
            else {
                String[] split = line.split("\\s+");
                tokens.add(split[0]);
                pos.add(split[1]);
                labels.add(split[2]);
            }
        }
        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        List<String> allowedVerbs = Arrays.asList("have", "take", "give", "do", "get", "make");
        View posView = ta.getView(ViewNames.POS);
        View lemmaView = ta.getView(ViewNames.LEMMA);
        List<Constituent> candidates = new ArrayList<>();

        for (Constituent prep : posView.where(Queries.hasLabel("IN").or(Queries.hasLabel("TO")))) {
            Iterator<Constituent> prevConstIter = lemmaView.where(Queries.adjacentToBefore(prep)).iterator();
            if (!prevConstIter.hasNext()) continue;
            Constituent c = prevConstIter.next();
            if (!allowedVerbs.contains(lemmaView.getConstituentsCovering(c).get(0).getLabel())) continue;
            candidates.add(new Constituent(CANDIDATE, viewName, ta, c.getStartSpan(), prep.getEndSpan()));
        }
        return getFinalCandidates(ta.getView(viewName), candidates);
    }
}
