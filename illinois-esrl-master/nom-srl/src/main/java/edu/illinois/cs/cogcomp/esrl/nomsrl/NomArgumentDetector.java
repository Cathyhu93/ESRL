package edu.illinois.cs.cogcomp.esrl.nomsrl;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.core.datastructures.trees.TreeTraversal;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.core.features.SRLFeatures;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentIdentifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.nlp.utilities.POSUtils;
import edu.illinois.cs.cogcomp.nlp.utilities.ParseTreeProperties;
import edu.illinois.cs.cogcomp.nlp.utilities.ParseUtils;
import edu.illinois.cs.cogcomp.pipeline.server.ServerClientAnnotator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NomArgumentDetector {
	private static ResourceManager rm = ESRLConfigurator.defaults();
	private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
	private static String modelName = modelsDir + File.separator + NomSRLArgumentIdentifier.CLASS_NAME;

	private static final NomSRLArgumentIdentifier identifier =
			new NomSRLArgumentIdentifier(modelName + "auto.lc", modelName + "auto.lex");

	public List<Constituent> getArguments(Constituent predicate) throws Exception {
		List<Constituent> output = new ArrayList<>();
		for (Constituent c : generateCandidates(predicate)) {
			// If this is not an argument according to the identifier
			if (identifier.discreteValue(c).equals("false")) continue;
			output.add(c);
		}
		return output;
	}

	public String getCandidateViewName()
	{
		return "XuePalmerHeuristicView";
	}


	public List<Constituent> generateCandidates(Constituent predicate) throws Exception {
		TextAnnotation ta = predicate.getTextAnnotation();

		int predicateSentenceId = ta.getSentenceId(predicate);

		int predicateSentenceStart = ta.getSentence(predicateSentenceId).getStartSpan();



		Tree<String> tree = ParseUtils.getParseTree(SRLFeatures.parseViewName, ta, predicateSentenceId);

		if (SRLFeatures.parseViewName.equals(ViewNames.PARSE_GOLD)) {
			tree = ParseUtils.snipNullNodes(tree);
			tree = ParseUtils.stripFunctionTags(tree);
			tree = ParseUtils.stripIndexReferences(tree);
		}

		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils.getSpanLabeledTree(tree);

		Constituent predicateClone = predicate.cloneForNewView(this.getCandidateViewName());

		int predicatePosition = predicate.getStartSpan() - predicateSentenceStart;

		Set<Constituent> out = new HashSet<>();


		// add all non terminals in the tree
		for (Tree<Pair<String, IntPair>> c : TreeTraversal.depthFirstTraversal(spanLabeledTree)) {
			if (!c.isRoot() && !c.isLeaf() && !c.getChild(0).isLeaf()) {
				int start = c.getLabel().getSecond().getFirst() + predicateSentenceStart;
				int end = c.getLabel().getSecond().getSecond() + predicateSentenceStart;

				out.add(getNewConstituent(predicateClone, start, end));
			}
		}

		Tree<Pair<String, IntPair>> predicateNode = spanLabeledTree.getYield().get(predicatePosition);
		// add all siblings of the predicate
		for (Tree<Pair<String, IntPair>> sibling : predicateNode.getParent().getParent().getChildren()) {
			Pair<String, IntPair> siblingNode = sibling.getLabel();
			IntPair siblingSpan = siblingNode.getSecond();
			int siblingSpanFirst = siblingSpan.getFirst();
			int siblingSpanSecond = siblingSpan.getSecond();

			// unlike in VerbSRL (XuePalmerCandidateGenerator) we do allow the predicate
			// to also be an argument, so there is no need to exclude it here

			int start = siblingSpanFirst + predicateSentenceStart;
			int end = siblingSpanSecond + predicateSentenceStart;

			out.add(getNewConstituent(predicateClone, start, end));
		}

		// verb nodes that dominate the predicate
		Tree<Pair<String, IntPair>> node = predicateNode.getParent();

		while (!node.isRoot() && !ParseTreeProperties.isNonTerminalVerb(node.getLabel().getFirst()))
			node = node.getParent();

		for (Tree<Pair<String, IntPair>> verbCandidate : node.getYield()) {
			if (POSUtils.isPOSVerb(verbCandidate.getParent().getLabel().getFirst())) {
				int start = verbCandidate.getLabel().getSecond().getFirst() + predicateSentenceStart;
				int end = start + 1;

				out.add(getNewConstituent(predicateClone, start, end));
			}
		}

		// pronouns in NPs within the same clause that dominate this predicate
		node = predicateNode.getParent();
		while (!node.isRoot()) {
			String label = node.getLabel().getFirst();
			if (label.startsWith("S"))
				break;

			if (ParseTreeProperties.isNonTerminalNoun(label)) {
				for (Tree<Pair<String, IntPair>> nominalCandidate : node.getYield()) {
					if (POSUtils.isPOSPossessivePronoun(nominalCandidate.getParent().getLabel().getFirst())) {
						int start = nominalCandidate.getLabel().getSecond().getFirst() + predicateSentenceStart;
						int end = start + 1;

						out.add(getNewConstituent(predicateClone, start, end));
					}
				}
			}
			node = node.getParent();

		}

		// if predicate is dominated by a PP, then the head of that PP
		node = predicateNode.getParent();
		boolean ppParentFound = false;
		while (!node.isRoot()) {
			String label = node.getLabel().getFirst();
			if (ParseTreeProperties.isNonTerminalPP(label)) {
				ppParentFound = true;
				break;
			} else if (label.startsWith("S"))
				break;

			node = node.getParent();
		}

		if (ppParentFound) {
			int start = node.getLabel().getSecond().getFirst() + predicateSentenceStart;
			int end = start + 1;
			out.add(getNewConstituent(predicateClone, start, end));
		}
		List<Constituent> output = new ArrayList<>();
		for (Constituent c : out) {
			if (!ParseTreeProperties.isPunctuationToken(c.getSurfaceForm()))
				output.add(c);
		}
		return new ArrayList<>(output);
	}
/*	public List<Constituent> generateCandidates(Constituent predicate) {
		Constituent predicateClone = predicate.cloneForNewView(getCandidateViewName());

		TextAnnotation ta = predicateClone.getTextAnnotation();
		int sentenceId = ta.getSentenceId(predicateClone);
		Tree<String> tree = ParseUtils.getParseTree(SRLFeatures.parseViewName, ta, sentenceId);

		if (SRLFeatures.parseViewName.equals(ViewNames.PARSE_GOLD)) {
			tree = ParseUtils.snipNullNodes(tree);
			tree = ParseUtils.stripFunctionTags(tree);
			tree = ParseUtils.stripIndexReferences(tree);
		}

		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils.getSpanLabeledTree(tree);

		int sentenceStart = ta.getSentence(sentenceId).getStartSpan();
		int predicatePosition = predicateClone.getStartSpan() - sentenceStart;

		Set<Constituent> out = new HashSet<>();

		List<Tree<Pair<String, IntPair>>> yield = spanLabeledTree.getYield();

		Tree<Pair<String, IntPair>> predicateTree = yield.get(predicatePosition);

		Tree<Pair<String, IntPair>> currentNode = predicateTree.getParent();

		boolean done = false;
		while (!done) {
			if (currentNode.isRoot())
				done = true;
			else {
				List<Constituent> candidates = new ArrayList<>();

				for (Tree<Pair<String, IntPair>> sibling : currentNode.getParent().getChildren()) {
					Pair<String, IntPair> siblingNode = sibling.getLabel();

					// do not take the predicate as the argument
					IntPair siblingSpan = siblingNode.getSecond();
					if (siblingSpan.equals(predicateClone.getSpan()))
						continue;

					// do not take any constituent including the predicate as an argument
					if ((predicatePosition >= siblingSpan.getFirst())
							&& (predicateClone.getEndSpan() <= siblingSpan.getSecond()))
						continue;

					String siblingLabel = siblingNode.getFirst();

					int start = siblingSpan.getFirst() + sentenceStart;
					int end = siblingSpan.getSecond() + sentenceStart;

					candidates.add(getNewConstituent(predicateClone, start, end));

					if (siblingLabel.startsWith("PP")) {
						for (Tree<Pair<String, IntPair>> child : sibling.getChildren()) {
							int candidateStart = child.getLabel().getSecond().getFirst() + sentenceStart;
							int candidateEnd = child.getLabel().getSecond().getSecond() + sentenceStart;

							candidates.add(getNewConstituent(predicateClone, candidateStart, candidateEnd));
						}
					}
				}
				out.addAll(candidates);

				currentNode = currentNode.getParent();
			}
		}

		// Punctuations maketh an argument not!
		List<Constituent> output = new ArrayList<>();
		for (Constituent c : out) {
			if (!ParseTreeProperties.isPunctuationToken(c.getSurfaceForm()))
				output.add(c);
		}

		return output;
	}*/

	protected Constituent getNewConstituent(String label, Constituent predicate, int start, int end) {
		Constituent newConstituent = new Constituent(label, 1.0, getCandidateViewName(),
				predicate.getTextAnnotation(), start, end);
		new Relation("ChildOf", predicate, newConstituent, 1.0);
		return newConstituent;
	}

	protected Constituent getNewConstituent(Constituent predicate, int start, int end) {
		return getNewConstituent(DataReader.CANDIDATE, predicate, start, end);
	}

	private void train() throws Exception {
		NomSRLArgumentReader trainReader = new NomSRLArgumentReader("train", false);
		BatchTrainer trainer = new BatchTrainer(identifier, trainReader, 10000);
		trainer.train(10);
		identifier.save();
	}

	private void test() throws Exception {
		NomSRLArgumentReader testDataReader = new NomSRLArgumentReader("test", false);
		TestDiscrete.testDiscrete(new TestDiscrete(), identifier, new NomSRLArgumentClassifier.Label(), testDataReader, true, 10000);
	}

	public static void main(String[] args) throws Exception {
		NomArgumentDetector argumentDetector = new NomArgumentDetector();
		argumentDetector.train();
		argumentDetector.test();
	}
}
