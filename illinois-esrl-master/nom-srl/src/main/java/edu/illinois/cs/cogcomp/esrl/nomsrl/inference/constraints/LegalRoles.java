package edu.illinois.cs.cogcomp.esrl.nomsrl.inference.constraints;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.nomsrl.NomSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.List;

public class LegalRoles extends ParameterizedConstraint {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelName = modelsDir + File.separator + NomSRLArgumentClassifier.CLASS_NAME;

    private static final NomSRLArgumentClassifier baseClassifier =
            new NomSRLArgumentClassifier(modelName + ".lc", modelName + ".lex");

    public LegalRoles() {
        super("edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints.LegalRoles");
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String discreteValue(Object example) {
        Constituent predicate = (Constituent) example;

        boolean constraint = true;
        try {
            for (Constituent arg : NomSRLArgumentReader.getArguments(predicate)) {
                constraint = false;
                for (String role : NomSRLArgumentReader.getLegalRoles(predicate)) {
                    constraint = baseClassifier.discreteValue(arg).equals(role) ||
                            baseClassifier.discreteValue(arg).equals("R-" + role) ||
                            baseClassifier.discreteValue(arg).equals("c-" + role);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!constraint) return "false";

        return "true";
    }

    public int hashCode() {
        return "LegalRoles".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof LegalRoles;
    }

    public FirstOrderConstraint makeConstraint(Object example) {
        Constituent predicate = (Constituent) example;
        List<Constituent> arguments = null;
        try {
            arguments = NomSRLArgumentReader.getArguments(predicate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Object[] context = new Object[1];
        context[0] = predicate;

        EqualityArgumentReplacer roleEAR = new EqualityArgumentReplacer(context) {
            public Object getLeftObject() {
                return quantificationVariables.get(0);
            }

            public String getRightValue() {
                return ((String) quantificationVariables.get(1));
            }
        };
        EqualityArgumentReplacer cRoleEAR = new EqualityArgumentReplacer(context) {
            public Object getLeftObject() {
                return quantificationVariables.get(0);
            }

            public String getRightValue() {
                return "C-" + quantificationVariables.get(1);
            }
        };
        EqualityArgumentReplacer rRoleEAR = new EqualityArgumentReplacer(context) {
            public Object getLeftObject() {
                return quantificationVariables.get(0);
            }

            public String getRightValue() {
                return "R-" + quantificationVariables.get(1);
            }
        };
        FirstOrderConstraint isRole = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), null, roleEAR);
        FirstOrderConstraint isCRole = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), null, cRoleEAR);
        FirstOrderConstraint isRRole = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), null, rRoleEAR);
        FirstOrderConstraint roleCR = new FirstOrderDisjunction(isCRole, isRRole);
        FirstOrderConstraint roleAny = new FirstOrderDisjunction(roleCR, isRole);

        FirstOrderConstraint existsRole = new ExistentialQuantifier("role", NomSRLArgumentReader.getLegalRoles(predicate), roleAny);

        FirstOrderConstraint forAll = new UniversalQuantifier("arg", arguments, existsRole);

        return new FirstOrderConjunction(new FirstOrderConstant(true), forAll);
    }
}

