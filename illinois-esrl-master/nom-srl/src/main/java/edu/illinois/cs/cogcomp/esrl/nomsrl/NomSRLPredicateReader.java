package edu.illinois.cs.cogcomp.esrl.nomsrl;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.nlp.corpusreaders.NombankReader;

import java.util.ArrayList;
import java.util.List;

/**
 * A wrapper for {@link NombankReader}.
 */
public class NomSRLPredicateReader extends DataReader {
    private static final ResourceManager rm = ESRLConfigurator.defaults();
    private static final String nombankDir = rm.getString(ESRLConfigurator.NOMBANK_DIR);
    private static final String treebankDir = rm.getString(ESRLConfigurator.TREEBANK_DIR);

    private static final NomPredicateDetector predicateDetector = new NomPredicateDetector();

    private static boolean useGold = rm.getBoolean(ESRLConfigurator.SRL_USE_GOLD);
    private final boolean usePredictor;

    public NomSRLPredicateReader(String corpusName, boolean usePredictor) throws Exception {
        super(corpusName, corpusName, ViewNames.SRL_NOM);
        this.usePredictor = usePredictor;
    }

    @Override
    public List<TextAnnotation> readData() {
        String[] sections;
        // Here, `file` is the corpus name
        switch (file) {
            case "train":
                sections = new String[]{"02", "03", "04", "05", "06", "07", "08", "09",
                        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21"};
                break;
            case "dev":
                sections = new String[]{"24"};
                break;
            case "test":
                sections = new String[]{"23"};
                break;
            default:
                throw new RuntimeException("Wrong dataset name. Choose one of train, dev, test");
        }
        NombankReader reader;
        try {
            reader = new NombankReader(treebankDir, nombankDir, sections, viewName, false);
        } catch (Exception e) {
            throw new RuntimeException("Reader exception: " + e);
        }
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        while (reader.hasNext()) textAnnotations.add(reader.next());
        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        PredicateArgumentView predArgView = (PredicateArgumentView) ta.getView(viewName);
        List<Constituent> predicates = predArgView.getPredicates();
        if (useGold) return predicates;
        if (usePredictor)
            predicates = predicateDetector.getPredicates(ta);
        else predicates = predicateDetector.generateCandidates(ta);
        return getFinalCandidates(predArgView, predicates);
    }
}
