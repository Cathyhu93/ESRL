package edu.illinois.cs.cogcomp.esrl.nomsrl;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.AbstractClassifier;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.nomsrl.inference.ConstrainedNomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentIdentifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.learn.Lexicon;

import java.io.File;
import java.io.*;

public class Main {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String identifierModelName = modelsDir + File.separator + NomSRLArgumentIdentifier.CLASS_NAME;
    private static String classifierModelName = modelsDir + File.separator + NomSRLArgumentClassifier.CLASS_NAME;

    private final NomSRLArgumentIdentifier identifier = new NomSRLArgumentIdentifier(identifierModelName + "auto.lc", identifierModelName + "auto.lex");
    private final NomSRLArgumentClassifier classifier = new NomSRLArgumentClassifier(classifierModelName + "auto.lc", classifierModelName + "auto.lex");

    private boolean useArgPredictor = !rm.getBoolean(ESRLConfigurator.SRL_USE_GOLD);//SRL_USE_GOLD = Configurator.FALSE

    private void error() throws Exception {

        ConstrainedNomSRLArgumentClassifier constClassifier = new ConstrainedNomSRLArgumentClassifier();
        NomSRLArgumentReader testDataReader = new NomSRLArgumentReader(false,"test", useArgPredictor);


        String errorsDir = rm.getString(ESRLConfigurator.ERRORS_DIR);
        String errorPath = errorsDir + File.separator + "NomSRL_errors.txt";
        File errorFile = new File(errorPath);
        errorFile.createNewFile();
        BufferedWriter out = new BufferedWriter(new FileWriter(errorFile));
        AbstractClassifier.Label oracle = new NomSRLArgumentClassifier.Label();

        Lexicon labelLexicon = null;
        boolean preExtraction = false;
        Object example;
        String prediction;
        String gold;
        example = testDataReader.next();
        if (example instanceof Object[] && ((Object[]) ((Object[]) example))[0] instanceof int[]) {
            preExtraction = true;
            labelLexicon = ((Learner) ((Classifier) constClassifier)).getLabelLexicon();
            gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
        } else {
            gold = oracle.discreteValue(example);
        }
        if (gold.contains("missed")) {
            prediction = "missed";
            gold = gold.substring(6);
        } else {
            prediction = constClassifier.discreteValue(example);
        }

        if (!prediction.equals(gold)) {

            String sentence = ((Constituent) example).getTextAnnotation().getSentence(0).toString();
            int argument_startCharOffset = ((Constituent) example).getStartCharOffset();
            int argument_endCharOffset = ((Constituent) example).getEndCharOffset();
            String arg_word = sentence.substring(argument_startCharOffset, argument_endCharOffset);

            Constituent predicate = ((Constituent) example).getIncomingRelations().get(0).getSource();
            int predicate_startCharOffset = predicate.getStartCharOffset();
            int predicate_endCharOffset = predicate.getEndCharOffset();

            String pred_word = sentence.substring(predicate_startCharOffset, predicate_endCharOffset);
            out.write("sentence:" + sentence + "\r\n");
            out.write("predicate:" + pred_word + "\r\n");
            out.write("argument:" + arg_word + "\r\n");
            out.write("gold:" + gold + "\r\n");
            out.write("prediction:" + prediction + "\r\n");
            out.write("\r\n");
            out.write(example.toString());
        }


        while ((example = testDataReader.next()) != null) {

            if (preExtraction) {
                gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
            } else {
                gold = oracle.discreteValue(example);
            }
            if (gold.contains("missed")) {
                prediction = "missed";
                gold = gold.substring(6);
            } else {
                prediction = constClassifier.discreteValue(example);
            }


            if (!prediction.equals(gold)) {

                String sentence = ((Constituent) example).getTextAnnotation().toString();

                Constituent predicate = ((Constituent) example).getIncomingRelations().get(0).getSource();

                out.write("sentence:" + sentence + "\r\n");
                out.write("predicate:" + predicate.toString() + "\r\n");
                out.write("argument:" + example.toString() + "\r\n");
                out.write("gold:" + gold + "\r\n");
                out.write("prediction:" + prediction + "\r\n");
                out.write("\r\n");
            }
        }


        out.close();
        testDataReader.close();
    }

    private void train() throws Exception {

        NomSRLArgumentReader trainIdentifyReader = new NomSRLArgumentReader("test", false);
        BatchTrainer identifyTrainer = new BatchTrainer(identifier, trainIdentifyReader, 10000);
        identifyTrainer.train(10);
        identifier.save();
        trainIdentifyReader.close();


        /*NomSRLArgumentReader trainClassifyReader = new NomSRLArgumentReader("test", useArgPredictor);
        BatchTrainer classifyTrainer = new BatchTrainer(classifier, trainClassifyReader, 10000);
        classifyTrainer.train(10);
        classifier.save();
        trainClassifyReader.close();*/

    }


    private void test() throws Exception {

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("missed");
        tester.addNull("candidate");
        ConstrainedNomSRLArgumentClassifier constClassifier = new ConstrainedNomSRLArgumentClassifier();
        NomSRLArgumentReader testDataReader = new NomSRLArgumentReader(false, "test", useArgPredictor);

        String resultsDir = rm.getString(ESRLConfigurator.RESULTS_DIR);
        String resultsPath = resultsDir + File.separator + "NomSRL_results.txt";
        File resultsFile = new File(resultsPath);
        resultsFile.createNewFile();
        PrintStream out = new PrintStream(resultsFile);

        AbstractClassifier.Label oracle = new NomSRLArgumentClassifier.Label();

        Lexicon labelLexicon = null;
        boolean preExtraction = false;
        Object example;
        String prediction;
        String gold;
        example = testDataReader.next();

        if (example instanceof Object[] && ((Object[]) ((Object[]) example))[0] instanceof int[]) {
            preExtraction = true;
            labelLexicon = ((Learner) ((Classifier) constClassifier)).getLabelLexicon();
            gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
        } else {
            gold = oracle.discreteValue(example);
        }
        //missed => not identified
        if (gold.contains("missed")) {
            prediction = "missed";
            gold = gold.substring(6);
        } else {
            prediction = constClassifier.discreteValue(example);
        }
        tester.reportPrediction(prediction, gold);


        while ((example = testDataReader.next()) != null) {
            if (preExtraction) {
                gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
            } else {
                gold = oracle.discreteValue(example);
            }

            if (gold.contains("missed")) {
                prediction = "missed";
                gold = gold.substring(6);
            } else {
                prediction = constClassifier.discreteValue(example);
                assert prediction != null : "Classifier returned null prediction for example " + example;
            }
            tester.reportPrediction(prediction, gold);
        }

        tester.printPerformance(out);

        out.close();
        testDataReader.close();
    }


    public void evaluateIdentification() throws Exception{
        ConstrainedNomSRLArgumentClassifier constClassifier = new ConstrainedNomSRLArgumentClassifier();
        AbstractClassifier.Label oracle = new NomSRLArgumentClassifier.Label();
        String resultsDir = rm.getString(ESRLConfigurator.RESULTS_DIR);
        String resultsPath = resultsDir + File.separator + "NomSRL_results_identify.txt";
        File resultsFile = new File(resultsPath);
        PrintStream out = new PrintStream(resultsFile);

        TestDiscrete tester = new TestDiscrete();
        NomSRLArgumentReader testDataReader = new NomSRLArgumentReader(false,"test", useArgPredictor);

        boolean preExtraction = false;
        Object example;
        Lexicon labelLexicon = null;
        String prediction;
        String gold;
        example = testDataReader.next();
        if (example instanceof Object[] && ((Object[]) ((Object[]) example))[0] instanceof int[]) {
            preExtraction = true;
            labelLexicon = ((Learner) ((Classifier) constClassifier)).getLabelLexicon();
            gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
        } else {
            gold = oracle.discreteValue(example);
        }
        if (gold.contains("missed")) {
            prediction = "missed";
            gold = "candidate";
        }
        else if(gold == "candidate"){
           gold = "missed";
           prediction = "candidate";
        }
        else {
            gold = "candidate";
            prediction = gold;
        }

        tester.reportPrediction(prediction, gold);


        while ((example = testDataReader.next()) != null) {
            if (preExtraction) {
                gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
            } else {
                gold = oracle.discreteValue(example);
            }

            if (gold.contains("missed")) {
                prediction = "missed";
                gold = "candidate";
            }
            else if(gold == "candidate"){
                gold = "missed";
                prediction = "candidate";
            }
            else {
                gold = "candidate";
                prediction = gold;
            }
            tester.reportPrediction(prediction, gold);
        }

        tester.printPerformance(out);

        out.close();
        testDataReader.close();

    }


    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.train();
        //main.evaluateIdentification();
        //main.test();
        //main.error();

    }
}
