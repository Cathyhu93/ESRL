package edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features;

import edu.illinois.cs.cogcomp.esrl.core.features.SRLFeatures;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;

public class parseFeatures extends Classifier {
    private static final SRLFeatures linearPosition = SRLFeatures.linearPosition;
    private static final SRLFeatures path = SRLFeatures.path;
    private static final SRLFeatures syntacticFrame = SRLFeatures.syntacticFrame;
    private static final SRLFeatures phraseType = SRLFeatures.phraseType;
    private static final SRLFeatures parseHeadWord = SRLFeatures.parseHeadWord;
    private static final SRLFeatures subcategorization = SRLFeatures.subcategorization;

    public parseFeatures() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features";
        name = "parseFeatures";
    }

    public FeatureVector classify(Object example) {
        FeatureVector result = new FeatureVector();
        result.addFeatures(linearPosition.classify(example));
        result.addFeatures(path.classify(example));
        result.addFeatures(syntacticFrame.classify(example));
        result.addFeatures(phraseType.classify(example));
        result.addFeatures(parseHeadWord.classify(example));
        result.addFeatures(subcategorization.classify(example));
        return result;
    }
}

