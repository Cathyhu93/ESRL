package edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features;



import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.esrl.core.features.SRLFeatures;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;

public class NomArgumentIdentifierConjunction extends Classifier{


    public  NomArgumentIdentifierConjunction ()
    {
        containingPackage ="edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features";
        name = "nomArgumentIdentifierConjuction";
    }
    private static final SRLFeatures ptypeAndPathLength  = SRLFeatures.ptypeAndPathLength;
    private static final SRLFeatures predAndHeadWord = SRLFeatures.predAndHeadWord;
    private static final SRLFeatures predAndPath = SRLFeatures.predAndPath;
    private static final SRLFeatures predAndPosition = SRLFeatures.predAndPosition;

    public FeatureVector classify(Object example){
        FeatureVector result = new FeatureVector();

        result.addFeatures(ptypeAndPathLength.classify(example));
        result.addFeatures(predAndHeadWord.classify(example));
        result.addFeatures(predAndPath.classify(example));
        result.addFeatures(predAndPosition.classify(example));
        return result;

    }
}



