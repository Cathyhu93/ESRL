#!/bin/bash


LD_LIBRARY_PATH=/shared/grandpa/opt/xpressmp_client_64/lib:/shared/grandpa/opt/share/gurobi405/linux64/lib
XPRESSMP_LIB=/shared/grandpa/opt/xpressmp_client_64/lib/xprb.jar:/shared/grandpa/opt/xpressmp_client_64/lib/xprm.jar:/shared/grandpa/opt/xpressmp_client_64/lib/xprs.jar

mvn -q -o compile
JAVA=java

LIBDIR=target/dependency

CP=models:config:target/classes:$XPRESSMP_LIB
for file in `ls $LIBDIR`; do
    CP=$CP:$LIBDIR/$file
done

MEMORY="-Xmx10g"

OPTIONS="-ea -XX:+UseParallelGC -cp $CP $MEMORY"

time nice $JAVA $MEMORY $OPTIONS  edu.illinois.cs.cogcomp.srl.main.SRLApplication $*
