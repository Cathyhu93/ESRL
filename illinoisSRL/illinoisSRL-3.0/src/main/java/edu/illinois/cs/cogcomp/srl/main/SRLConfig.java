package edu.illinois.cs.cogcomp.srl.main;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.WordNetManager;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentClassifier;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentIdentifier;

public class SRLConfig {

	public static final String SRL_CONFIG_PROPERTIES_FILE = "config/srl-config.properties";
	private static String currentConfigFile = null;
	private static SRLConfig config = null;

	public static SRLConfig getInstance(String configFile) {
		if (config == null || configFile.equals(currentConfigFile)) {
			config = new SRLConfig(configFile);
			currentConfigFile = configFile;
		}

		return config;
	}

	public static SRLConfig getInstance() {

		if (config == null) {
			currentConfigFile = SRL_CONFIG_PROPERTIES_FILE;
			config = new SRLConfig(currentConfigFile);
		}

		return config;
	}

	private String curatorHost;
	private int curatorPort;

	private String defaultParser;
	private int beamSize;

	private boolean beamSearch;

	private String wordNetFile;
	private String modelsDirectory;

	private int numThreads;

	private SRLConfig(String configFile) {
		PropertiesConfiguration config = null;

		try {
			config = new PropertiesConfiguration(configFile);

			curatorHost = config.getString("CuratorHost", "");
			curatorPort = config.getInt("CuratorPort", -1);

			defaultParser = config.getString("DefaultParser");
			if (defaultParser.equals("Charniak")) {
				defaultParser = ViewNames.PARSE_CHARNIAK;

			} else if (defaultParser.equals("Stanford")) {
				defaultParser = ViewNames.PARSE_STANFORD;
			} else {
				throw new ConfigurationException("Invalid default parser."
						+ " Expecting one of Charniak/Stanford.");
			}

			if (config.containsKey("BeamSize"))
				beamSize = config.getInt("BeamSize");
			else
				beamSize = 50;

			beamSearch = config.containsKey("Inference")
					&& config.getString("Inference").toLowerCase()
							.equals("beamsearch");

			if (config.containsKey("LoadWordNetConfigFromClassPath")
					&& config.getBoolean("LoadWordNetConfigFromClassPath")) {
				WordNetManager.loadConfigAsClasspathResource(true);
			}

			if (config.containsKey("WordNetConfig"))
				this.wordNetFile = config.getString("WordNetConfig");
			else
				this.wordNetFile = "jwnl_properties.xml";

			if (config.containsKey("ModelsDirectory"))
				this.modelsDirectory = config.getString("ModelsDirectory");
			else
				this.modelsDirectory = "models";

			if (config.containsKey("NumThreads"))
				this.numThreads = config.getInt("NumThreads");
			else
				this.numThreads = 1;

		} catch (ConfigurationException e) {
			System.err.println("Error in config file. " + e.getMessage());
			System.exit(-1);
		}

	}

	public String getCuratorHost() {
		return curatorHost;
	}

	public int getCuratorPort() {
		return curatorPort;
	}

	public String getDefaultParser() {
		return this.defaultParser;
	}

	public void setDefaultParser(String defaultParserViewName) {
		this.defaultParser = defaultParserViewName;
	}

	public boolean doBeamSearch() {
		return this.beamSearch;
	}

	public int getBeamSize() {
		return this.beamSize;
	}

	public String getWordNetFile() {
		return this.wordNetFile;
	}

	public String getModelsDirectory() {
		return this.modelsDirectory;
	}

	public String getLexiconFile() {
		return VerbArgumentClassifier.class.getSimpleName() + ".lex";
	}

	public String getIdentifierModelFile() {
		return VerbArgumentIdentifier.class.getSimpleName() + ".lc";
	}

	public String getClassifierModelFile() {
		return VerbArgumentClassifier.class.getSimpleName() + ".lc";
	}

	public int getNumThreads() {
		return this.numThreads;
	}

}
