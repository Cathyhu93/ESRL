// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000055E8D4A02C030158FA2F699C6CB08B2575582226DB0C8DE4BD089904A35ADA2DBBB94145C5E7F87F7E4A5E8C2533EC1C757D2B8F0F199C75C41BEE1D33538B1FCA70190A4AE2806061DD26F2C928C0AE47C4A0821746D13A0867D1BEF7AC8DD3658488E9E1EE9CDCD7180F48A24BAE96636189C96FB9654979FE513958FDF139E66ADC6D57971A843E49D4633D6EA45D6CA5B85FD0E2F454F9DD000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;
import java.util.*;


public class NomSRLInferenceBeamSearch extends ILPInference
{
  public static TextAnnotation findHead(Constituent a)
  {
    return a.getTextAnnotation();
  }


  public NomSRLInferenceBeamSearch() { }
  public NomSRLInferenceBeamSearch(TextAnnotation head)
  {
    super(head, new BeamSearch(SRLConfig.getInstance().getBeamSize()));
    constraint = new NomSRLInferenceBeamSearch$subjectto().makeConstraint(head);
  }

  public String getHeadType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }
  public String[] getHeadFinderTypes()
  {
    return new String[]{ "edu.illinois.cs.cogcomp.edison.sentences.Constituent" };
  }

  public Normalizer getNormalizer(Learner c)
  {
    return new Softmax();
  }
}

