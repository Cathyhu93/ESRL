// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000D5EC13A02C0401581EBAC41611B2F006390AD8013688616163BE236859576699995114CBBB604056BDFFB75C3FC4A62E2091CE786734714142F87C428AABDE76A9FC754F66C5D10F16D011B93B0C40A8463D27E0F48AB3EDDD152AB4A5DFAF2E4057B83AB8D8C89F6F9C5267CBEC9260FEC0B4A513950AC3582960CFE7CF3852CBEDC22DE4F99C000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class SRLInferenceXpressMP$subjectto extends ParameterizedConstraint
{
  private static final LegalArguments __LegalArguments = new LegalArguments();
  private static final References __References = new References();
  private static final Continuences __Continuences = new Continuences();
  private static final NoDuplicates __NoDuplicates = new NoDuplicates();
  private static final NoOverlaps __NoOverlaps = new NoOverlaps();

  public SRLInferenceXpressMP$subjectto() { super("edu.illinois.cs.cogcomp.srl.learners.SRLInferenceXpressMP$subjectto"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'SRLInferenceXpressMP$subjectto(TextAnnotation)' defined on line 264 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    {
      boolean LBJ2$constraint$result$0;
      {
        boolean LBJ2$constraint$result$1;
        {
          boolean LBJ2$constraint$result$2;
          {
            boolean LBJ2$constraint$result$3;
            {
              boolean LBJ2$constraint$result$4;
              LBJ2$constraint$result$4 = __NoOverlaps.discreteValue(sentence).equals("true");
              if (LBJ2$constraint$result$4)
                LBJ2$constraint$result$3 = __LegalArguments.discreteValue(sentence).equals("true");
              else LBJ2$constraint$result$3 = false;
            }
            if (LBJ2$constraint$result$3)
              LBJ2$constraint$result$2 = __NoDuplicates.discreteValue(sentence).equals("true");
            else LBJ2$constraint$result$2 = false;
          }
          if (LBJ2$constraint$result$2)
            LBJ2$constraint$result$1 = __Continuences.discreteValue(sentence).equals("true");
          else LBJ2$constraint$result$1 = false;
        }
        if (LBJ2$constraint$result$1)
          LBJ2$constraint$result$0 = __References.discreteValue(sentence).equals("true");
        else LBJ2$constraint$result$0 = false;
      }
      if (!LBJ2$constraint$result$0) return "false";
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'SRLInferenceXpressMP$subjectto(TextAnnotation)' defined on line 264 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "SRLInferenceXpressMP$subjectto".hashCode(); }
  public boolean equals(Object o) { return o instanceof SRLInferenceXpressMP$subjectto; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'SRLInferenceXpressMP$subjectto(TextAnnotation)' defined on line 264 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    {
      FirstOrderConstraint LBJ2$constraint$result$0 = null;
      {
        FirstOrderConstraint LBJ2$constraint$result$1 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$2 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$3 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$4 = null;
              LBJ2$constraint$result$4 = __NoOverlaps.makeConstraint(sentence);
              FirstOrderConstraint LBJ2$constraint$result$5 = null;
              LBJ2$constraint$result$5 = __LegalArguments.makeConstraint(sentence);
              LBJ2$constraint$result$3 = new FirstOrderConjunction(LBJ2$constraint$result$4, LBJ2$constraint$result$5);
            }
            FirstOrderConstraint LBJ2$constraint$result$6 = null;
            LBJ2$constraint$result$6 = __NoDuplicates.makeConstraint(sentence);
            LBJ2$constraint$result$2 = new FirstOrderConjunction(LBJ2$constraint$result$3, LBJ2$constraint$result$6);
          }
          FirstOrderConstraint LBJ2$constraint$result$7 = null;
          LBJ2$constraint$result$7 = __Continuences.makeConstraint(sentence);
          LBJ2$constraint$result$1 = new FirstOrderConjunction(LBJ2$constraint$result$2, LBJ2$constraint$result$7);
        }
        FirstOrderConstraint LBJ2$constraint$result$8 = null;
        LBJ2$constraint$result$8 = __References.makeConstraint(sentence);
        LBJ2$constraint$result$0 = new FirstOrderConjunction(LBJ2$constraint$result$1, LBJ2$constraint$result$8);
      }
      __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
    }

    return __result;
  }
}

