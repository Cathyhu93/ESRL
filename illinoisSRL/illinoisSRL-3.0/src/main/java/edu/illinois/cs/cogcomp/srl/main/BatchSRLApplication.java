package edu.illinois.cs.cogcomp.srl.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.edison.data.ColumnFormatWriter;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorClient;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.utilities.SRLUtils;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;

public class BatchSRLApplication implements Runnable {

	private static final Logger log = LoggerFactory
			.getLogger(BatchSRLApplication.class);

	private final SRLConfig config;
	private final String inputFile;
	private final boolean whitespace;
	private final String outputFile;

	private final SRLSystem srlSystem;

	public BatchSRLApplication(SRLSystem srlSystem, SRLConfig config,
			String inputFile, String outputFile, boolean whitespace) {
		this.srlSystem = srlSystem;
		this.config = config;
		this.inputFile = inputFile;
		this.outputFile = outputFile;
		this.whitespace = whitespace;

	}

	@Override
	public void run() {
		try {

			log.info("Creating curator client");

			CuratorClient curator = new CuratorClient(config.getCuratorHost(),
					config.getCuratorPort());

			boolean charniak = config.getDefaultParser().equals(
					ViewNames.PARSE_CHARNIAK);

			log.info("Default parser = {}", charniak ? "Charniak" : "Stanford");

			ColumnFormatWriter writer = new ColumnFormatWriter(
					config.getDefaultParser(), ViewNames.SRL);

			PrintWriter out = new PrintWriter(new File(outputFile));

			for (String line : LineIO.read(inputFile)) {

				TextAnnotation ta = getTextAnnotationFromCurator(curator,
						charniak, false, line);
				PredicateArgumentView predicateArgumentView = srlSystem.getSRL(
						ta, config.doBeamSearch());

				ta.addView(ViewNames.SRL, predicateArgumentView);

				writer.transform(ta, out);

			}

			out.close();

		} catch (FileNotFoundException e) {
			System.err.println("Error reading " + inputFile);
			System.out.println(e.getLocalizedMessage());
		} catch (ServiceUnavailableException e) {
			System.err.println("Curator Error: " + e.getReason());
		} catch (AnnotationFailedException e) {
			System.err.println("Curator error: " + e.getReason());
		} catch (Exception e) {
			System.err.println("Error: " + e.getLocalizedMessage());
		}

	}

	private TextAnnotation getTextAnnotationFromCurator(CuratorClient curator,
			boolean charniak, boolean forceUpdate, String line)
			throws ServiceUnavailableException, AnnotationFailedException,
			TException {

		TextAnnotation ta;
		if (whitespace)
			ta = new TextAnnotation("", "", Arrays.asList(line));
		else
			ta = curator.getTextAnnotation("", "", line, forceUpdate);

		if (charniak)
			curator.addCharniakParse(ta, forceUpdate);
		else
			curator.addStanfordParse(ta, forceUpdate);

		curator.addPOSView(ta, forceUpdate);
		curator.addChunkView(ta, forceUpdate);
		curator.addNamedEntityView(ta, forceUpdate);

		ta.addView(ViewNames.PSEUDO_PARSE, SRLUtils.createPsuedoParseView(ta));

		return ta;
	}
}
