package edu.illinois.cs.cogcomp.srl.main;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import LBJ2.classify.Classifier;
import LBJ2.learn.Learner;

public abstract class SRLSystem {

	private static final Logger log = LoggerFactory.getLogger(SRLSystem.class);

	private ExecutorService executor;

	protected SRLSystem() {

		log.info("Initializing SRL classifiers");
		initializeClassifiers();

		log.info("Finished initializing all classifiers");

		int numThreads = SRLConfig.getInstance().getNumThreads();
		executor = Executors.newFixedThreadPool(numThreads);

		runDummySentence();
	}

	double getClassifierNullScore(Constituent candidate) {
		return this.getArgumentClassifier().scores(candidate).get("null");
	}

	abstract List<Constituent> getFilteredCandidatesForPredicate(
			Constituent goldPredicate);

	protected abstract List<Constituent> getPredicates(TextAnnotation ta);

	abstract String getPredicateType();

	public PredicateArgumentView getSRL(TextAnnotation ta, boolean beamSearch) {
		return getSRL(ta, getPredicates(ta), beamSearch);
	}

	public PredicateArgumentView getSRL(TextAnnotation ta,
			List<Constituent> predicates, boolean beamSearch) {
		SemanticRoleLabeler labeler = new SemanticRoleLabeler(this, ta,
				predicates, beamSearch);

		try {
			return labeler.call();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	abstract Classifier getSRLBeamSearch();

	abstract Classifier getSRLXpressMP();

	public abstract Learner getIdentifier();

	public abstract Learner getArgumentClassifier();

	public abstract String getViewName();

	protected abstract void initializeClassifiers();

	protected abstract TextAnnotation initializeDummySentence();

	public abstract boolean isCoreArgument(String label);

	protected void runDummySentence() {

		TextAnnotation ta = initializeDummySentence();

		log.info("Loading models into memory by running SRL on a dummy sentence...");
		log.info("Dummy sentence: {} ", ta.getText());

		PredicateArgumentView srl = this.getSRL(ta, true);
		log.info("Obtained SRL for sentence: {}", srl);

		assert srl != null;

		log.info("Finished loading models. Phew!");

	}

	abstract boolean ignorePredicatesWithoutArguments();

	public abstract String getSRLSystemName();

	public abstract String getSRLSystemVersion();

	public abstract String getSRLSystemIdentifier();
}
