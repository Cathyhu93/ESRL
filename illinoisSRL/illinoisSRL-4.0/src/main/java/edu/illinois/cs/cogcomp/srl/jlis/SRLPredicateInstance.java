package edu.illinois.cs.cogcomp.srl.jlis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.utilities.Parallel;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.indsup.inference.IInstance;
import edu.illinois.cs.cogcomp.srl.core.Models;
import edu.illinois.cs.cogcomp.srl.core.ArgumentCandidateGenerator;
import edu.illinois.cs.cogcomp.srl.core.ArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.core.ModelInfo;
import edu.illinois.cs.cogcomp.srl.core.SRLManager;
import edu.illinois.cs.cogcomp.srl.jlis.SRLMulticlassInstance;

public class SRLPredicateInstance implements IInstance {

	private static final int FEATURE_EXTRACTION_N_THREADS = 10;

	private final static Logger log = LoggerFactory
			.getLogger(SRLPredicateInstance.class);

	private final List<SRLMulticlassInstance> candidates;
	private final SRLMulticlassInstance senseInstance;
	private final SRLManager manager;

	public SRLPredicateInstance(List<SRLMulticlassInstance> candidates,
			SRLMulticlassInstance sense, SRLManager manager) {
		this.candidates = candidates;
		this.senseInstance = sense;
		this.manager = manager;
	}

	public SRLPredicateInstance(Constituent predicate, SRLManager manager) {

		this.manager = manager;
		senseInstance = new SRLMulticlassInstance(predicate, predicate, manager);

		List<SRLMulticlassInstance> list = new ArrayList<SRLMulticlassInstance>();

		ArgumentCandidateGenerator candidateGenerator = manager
				.getArgumentCandidateGenerator();

		List<Constituent> cands = candidateGenerator
				.generateCandidates(predicate);

		for (Constituent c : cands) {
			list.add(new SRLMulticlassInstance(c, predicate, manager));
		}
		this.candidates = Collections.unmodifiableList(list);
	}

	public SRLPredicateInstance(Constituent predicate, SRLManager manager,
			ArgumentIdentifier identifier) {

		this.manager = manager;
		senseInstance = new SRLMulticlassInstance(predicate, predicate, manager);

		ArgumentCandidateGenerator candidateGenerator = manager
				.getArgumentCandidateGenerator();

		List<SRLMulticlassInstance> allCandidates = new ArrayList<SRLMulticlassInstance>();

		for (Constituent c : candidateGenerator.generateCandidates(predicate)) {
			allCandidates.add(new SRLMulticlassInstance(c, predicate, manager));
		}

		cacheIdentifierFeatures(allCandidates);

		List<SRLMulticlassInstance> list = new ArrayList<SRLMulticlassInstance>();

		for (SRLMulticlassInstance c : allCandidates) {
			if (identifier.getIdentifierScaledDecision(c))
				list.add(c);
		}

		this.candidates = Collections.unmodifiableList(list);
	}

	public List<SRLMulticlassInstance> getCandidateInstances() {
		return candidates;
	}

	public SRLMulticlassInstance getSenseInstance() {
		return senseInstance;
	}

	@Override
	public double size() {
		return this.candidates.size() + 1;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		for (SRLMulticlassInstance c : candidates) {
			sb.append("Candidate: " + c.toString() + "\n");
		}

		sb.append("Sense: " + senseInstance.toString() + "\n");

		return sb.toString();
	}

	private synchronized void cacheIdentifierFeatures(
			List<SRLMulticlassInstance> xs) {

		for (SRLMulticlassInstance x : xs) {
			ModelInfo modelInfo = manager.getModelInfo(Models.Identifier);

			try {
				Set<Feature> feats = modelInfo.fex.getFeatures(x
						.getConstituent());

				x.cacheFeatureVector(Models.Identifier, feats);
			} catch (Exception e) {
				log.error("Unable to extract features for {}", x, e);

				throw new RuntimeException(e);
			}
		}
		//
		// Parallel.Method<SRLMulticlassInstance> function = new
		// Parallel.Method<SRLMulticlassInstance>() {
		//
		// @Override
		// public void run(SRLMulticlassInstance x) {
		//
		// ModelInfo modelInfo = manager.getModelInfo(Models.Identifier);
		//
		// try {
		// Set<Feature> feats = modelInfo.fex.getFeatures(x
		// .getConstituent());
		//
		// x.cacheFeatureVector(Models.Identifier, feats);
		// } catch (Exception e) {
		// log.error("Unable to extract features for {}", x, e);
		//
		// throw new RuntimeException(e);
		// }
		// }
		// };
		// try {
		//
		// int timeout = 10;
		// Parallel.forLoop(FEATURE_EXTRACTION_N_THREADS, xs, function,
		// timeout, TimeUnit.MINUTES);
		// } catch (Exception e) {
		// log.error("Waited for ten minutes for feature extraction."
		// + " Giving up!", e);
		// }
	}

	/**
	 * This method caches features for all argument candidates for given
	 * predicate. This is used only during testing to speed up prediction.
	 * 
	 * @param cacheIdentifier
	 *            Should the identifier also be cached?
	 */
	public void cacheAllFeatureVectors(boolean cacheIdentifier) {

		List<Pair<SRLMulticlassInstance, Models>> list = new ArrayList<Pair<SRLMulticlassInstance, Models>>();
		list.add(new Pair<SRLMulticlassInstance, Models>(senseInstance,
				Models.Sense));
		for (SRLMulticlassInstance x : this.getCandidateInstances()) {
			list.add(new Pair<SRLMulticlassInstance, Models>(x,
					Models.Classifier));

			if (cacheIdentifier)
				list.add(new Pair<SRLMulticlassInstance, Models>(x,
						Models.Identifier));

		}

		Parallel.Method<Pair<SRLMulticlassInstance, Models>> function = new Parallel.Method<Pair<SRLMulticlassInstance, Models>>() {

			@Override
			public void run(Pair<SRLMulticlassInstance, Models> argument) {

				SRLMulticlassInstance x = argument.getFirst();
				Models m = argument.getSecond();

				ModelInfo modelInfo = manager.getModelInfo(m);

				try {
					Set<Feature> feats = modelInfo.fex.getFeatures(x
							.getConstituent());

					x.cacheFeatureVector(m, feats);
				} catch (Exception e) {
					log.error("Unable to extract features for {}", x, e);

					throw new RuntimeException(e);
				}
			}
		};
		try {

			int timeout = 10;
			Parallel.forLoop(FEATURE_EXTRACTION_N_THREADS, list, function,
					timeout, TimeUnit.MINUTES);
		} catch (Exception e) {
			log.error("Waited for ten minutes for feature extraction."
					+ " Giving up!", e);
		}

	}
}
