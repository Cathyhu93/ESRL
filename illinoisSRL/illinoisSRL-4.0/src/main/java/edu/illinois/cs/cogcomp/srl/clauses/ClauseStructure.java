package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.indsup.inference.IStructure;
import edu.illinois.cs.cogcomp.indsup.learning.FeatureVector;
import edu.illinois.cs.cogcomp.indsup.learning.WeightVector;

public class ClauseStructure implements IStructure {

	private FeatureVector features;
	private ClauseInstance x;
	public final Set<IntPair> constituents;
	private ClauseManager manager;
	private Map<IntPair, Double> scores;
	double score;
	private double dotProduct;

	public ClauseStructure(ClauseManager manager, ClauseInstance x,
			Set<IntPair> constituents, FeatureVector f) {
		this.manager = manager;
		this.x = x;
		features = f;

		this.constituents = Collections.unmodifiableSet(constituents);
	}

	@Override
	public FeatureVector getFeatureVector() {
		if (this.features == null) {
			synchronized (this) {
				if (this.features == null) {
					try {
						setFeatures();
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		return this.features;
	}

	private void setFeatures() throws Exception {
		// The feature vector is the sum of features of all the clauses;

		ClauseFeatureExtractor fex = manager.getStructuredFeatureExtractor();
		FeatureVector f = new FeatureVector(new int[] {}, new float[] {});

		for (IntPair span : constituents) {

			f = FeatureVector.plus(fex.getFeatures(x, span, constituents), f);
		}

		features = f;

	}

	@Override
	public String toString() {
		if (scores == null)
			return "Clauses: " + constituents;
		else
			return "Clauses: " + scores + "\nDotproduct + loss = " + score
					+ ", dotProduct = " + dotProduct;
	}

	public void assignScores(WeightVector weight) throws Exception {
		scores = new HashMap<IntPair, Double>();
		ClauseFeatureExtractor fex = manager.getStructuredFeatureExtractor();

		for (IntPair span : constituents) {
			double score = weight.dotProduct(fex.getFeatures(x, span,
					constituents));

			scores.put(span, score);
		}

		dotProduct = weight.dotProduct(this.getFeatureVector());
	}

	public void setScore(double score) {
		this.score = score;

	}
}
