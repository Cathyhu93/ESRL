package edu.illinois.cs.cogcomp.srl;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Lexicon;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.experiments.ClassificationTester;
import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;
import edu.illinois.cs.cogcomp.core.experiments.ShufflingBasedStatisticalSignificance;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.edison.annotators.HeadFinderDependencyViewGenerator;
import edu.illinois.cs.cogcomp.edison.data.ColumnFormatWriter;
import edu.illinois.cs.cogcomp.edison.data.IResetableIterator;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorClient;
import edu.illinois.cs.cogcomp.edison.data.srl.NomLexEntry.NomLexClasses;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.indsup.inference.AbstractLossSensitiveStructureFinder;
import edu.illinois.cs.cogcomp.indsup.learning.DenseVector;
import edu.illinois.cs.cogcomp.indsup.learning.StructuredProblem;
import edu.illinois.cs.cogcomp.indsup.learning.WeightVector;
import edu.illinois.cs.cogcomp.infer.ilp.ILPSolverFactory;
import edu.illinois.cs.cogcomp.infer.ilp.ILPSolverFactory.SolverType;
import edu.illinois.cs.cogcomp.srl.caches.FeatureVectorCacheFile;
import edu.illinois.cs.cogcomp.srl.caches.SentenceDBHandler;
import edu.illinois.cs.cogcomp.srl.core.Models;
import edu.illinois.cs.cogcomp.srl.data.Dataset;
import edu.illinois.cs.cogcomp.srl.experiment.PreExtractor;
import edu.illinois.cs.cogcomp.srl.experiment.PruningPreExtractor;
import edu.illinois.cs.cogcomp.srl.experiment.TextPreProcessor;
import edu.illinois.cs.cogcomp.srl.inference.ISRLInference;
import edu.illinois.cs.cogcomp.srl.inference.NoOverlapInference;
import edu.illinois.cs.cogcomp.srl.inference.SRLILPInference;
import edu.illinois.cs.cogcomp.srl.inference.SRLLagrangeInference;
import edu.illinois.cs.cogcomp.srl.inference.SRLMulticlassInference;
import edu.illinois.cs.cogcomp.srl.utilities.ArgumentCandidateUtilities;
import edu.illinois.cs.cogcomp.srl.utilities.PredicateArgumentEvaluator;
import edu.illinois.cs.cogcomp.srl.utilities.PredicateDetectionTester;
import edu.illinois.cs.cogcomp.srl.core.AbstractPredicateDetector;
import edu.illinois.cs.cogcomp.srl.core.ArgumentCandidateGenerator;
import edu.illinois.cs.cogcomp.srl.core.ArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.core.ModelInfo;
import edu.illinois.cs.cogcomp.srl.core.SRLManager;
import edu.illinois.cs.cogcomp.srl.core.VerbNom;
import edu.illinois.cs.cogcomp.srl.jlis.SRLMulticlassInstance;
import edu.illinois.cs.cogcomp.srl.jlis.SRLMulticlassLabel;
import edu.illinois.cs.cogcomp.srl.jlis.SRLPredicateInstance;
import edu.illinois.cs.cogcomp.srl.jlis.SRLPredicateStructure;
import edu.illinois.cs.cogcomp.srl.jlis.SRLSentenceInstance;
import edu.illinois.cs.cogcomp.srl.jlis.SRLSentenceStructure;
import edu.illinois.cs.cogcomp.srl.learn.IdentifierThresholdTuner;
import edu.illinois.cs.cogcomp.srl.learn.JLISLearner;
import edu.illinois.cs.cogcomp.srl.learn.LearnerParameters;

public class SRLExperiments {

	private final static Logger log = LoggerFactory
			.getLogger(SRLExperiments.class);

	private static final Random RANDOM = new Random();

	final boolean loadFromClassPath = false;
	final boolean trainingMode = true;

	final boolean softmax;

	private SRLManager manager;

	private VerbNom verbNom;

	private String defaultParser;

	private SRLProperties properties;

	public SRLExperiments(String verbNom_, String defaultParser)
			throws Exception {
		this(verbNom_, defaultParser, false);
	}

	public SRLExperiments(String verbNom_, String defaultParser, boolean softmax)
			throws Exception {

		this.defaultParser = defaultParser;
		verbNom = VerbNom.valueOf(verbNom_);
		this.softmax = softmax;

		manager = Main.getManager(verbNom, trainingMode, loadFromClassPath,
				softmax, defaultParser);

		properties = SRLProperties.getInstance();

	}

	public void preExtract(String model) throws Exception {

		int numConsumers = 20;

		Models modelToExtract = Models.valueOf(model);

		Dataset dataset = Dataset.PTBTrainDev;

		if (modelToExtract == Models.Identifier)
			dataset = Dataset.PTBTrain;

		log.info("Pre-extracting {} for PTB Train and dev", modelToExtract);
		ModelInfo modelInfo = manager.getModelInfo(modelToExtract);

		String featureSet = ""
				+ modelInfo.featureManifest.getIncludedFeatures().hashCode();

		String allDataCacheFile = properties.getFeatureCacheFile(verbNom,
				modelToExtract, featureSet, defaultParser, dataset);
		FeatureVectorCacheFile featureCache = preExtract(numConsumers, manager,
				modelToExtract, dataset, allDataCacheFile, false);

		pruneFeatures(numConsumers, manager, modelToExtract, featureCache,
				properties.getPrunedFeatureCacheFile(verbNom, modelToExtract,
						featureSet, defaultParser));

		Lexicon lexicon = modelInfo.getLexicon().getPrunedLexicon(
				manager.getPruneSize(modelToExtract));

		log.info("Saving lexicon  with {} features to {}", lexicon.size(),
				manager.getLexiconFileName(modelToExtract));
		log.info(lexicon.size() + " features in the lexicon");

		lexicon.save(manager.getLexiconFileName(modelToExtract));

		if (modelToExtract == Models.Identifier) {
			String devCacheFile = properties.getFeatureCacheFile(verbNom,
					modelToExtract, featureSet, defaultParser, Dataset.PTBDev);
			preExtract(numConsumers, manager, modelToExtract, Dataset.PTBDev,
					devCacheFile, false);
		}
	}

	private void pruneFeatures(int numConsumers, SRLManager manager,
			Models modelToExtract, FeatureVectorCacheFile featureCache,
			String cacheFile2) throws IOException, InterruptedException,
			Exception {
		if (IOUtils.exists(cacheFile2)) {
			log.warn("Old pruned cache file found. Deleting...");
			IOUtils.rm(cacheFile2);
			log.info("Done");
		}

		log.info("Pruning features. Saving pruned features to {}", cacheFile2);

		FeatureVectorCacheFile prunedfeatureCache = new FeatureVectorCacheFile(
				cacheFile2, modelToExtract, manager);

		PruningPreExtractor p1 = new PruningPreExtractor(manager,
				modelToExtract, featureCache, prunedfeatureCache, numConsumers);
		p1.run();
		p1.finalize();
	}

	private FeatureVectorCacheFile preExtract(int numConsumers,
			SRLManager manager, Models modelToExtract, Dataset dataset,
			String cacheFile, boolean lockLexicon) throws Exception {
		if (IOUtils.exists(cacheFile)) {
			log.warn("Old cache file found. Deleting...");
			IOUtils.rm(cacheFile);
			log.info("Done");
		}

		FeatureVectorCacheFile featureCache = new FeatureVectorCacheFile(
				cacheFile, modelToExtract, manager);

		Iterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(dataset);

		PreExtractor p = new PreExtractor(manager, data, numConsumers,
				modelToExtract, featureCache);

		if (lockLexicon)
			p.lockLexicon();

		p.run();

		p.finalize();
		return featureCache;
	}

	public void train(String model_) throws Exception {

		int numThreads = 20;

		Models model = Models.valueOf(model_);
		ModelInfo modelInfo = manager.getModelInfo(model);

		String featureSet = ""
				+ modelInfo.featureManifest.getIncludedFeatures().hashCode();

		String cacheFile = properties.getPrunedFeatureCacheFile(verbNom, model,
				featureSet, defaultParser);

		AbstractLossSensitiveStructureFinder[] inference = new AbstractLossSensitiveStructureFinder[numThreads];

		for (int i = 0; i < inference.length; i++)
			inference[i] = new SRLMulticlassInference(manager, model);

		double c;
		FeatureVectorCacheFile cache;

		if (model == Models.Classifier) {
			c = 0.00390625;
			log.info("Skipping cross-validation for Classifier. c = {}", c);
		} else {
			cache = new FeatureVectorCacheFile(cacheFile, model, manager);

			StructuredProblem cvProblem = cache.getStructuredProblem(20000);

			cache.close();

			LearnerParameters params = JLISLearner.cvStructSVMSRL(cvProblem,
					inference, 5);
			c = params.getcStruct();

			log.info("c = {} for {} after cv", c, verbNom + " " + model);
		}

		cache = new FeatureVectorCacheFile(cacheFile, model, manager);

		DenseVector.BLOCK_SIZE = 10000000;

		StructuredProblem problem = cache.getStructuredProblem();
		cache.close();

		WeightVector w = JLISLearner.trainStructSVM(inference, problem, c);

		JLISLearner.saveWeightVector(w, manager.getModelFileName(model));

	}

	public void tuneIdentifier() throws Exception {

		int nF = 2;
		if (verbNom == VerbNom.Nom)
			nF = 3;

		ModelInfo modelInfo = manager.getModelInfo(Models.Identifier);

		modelInfo.loadWeightVector();

		String featureSet = ""
				+ modelInfo.featureManifest.getIncludedFeatures().hashCode();

		String cacheFile = properties.getFeatureCacheFile(verbNom,
				Models.Identifier, featureSet, defaultParser, Dataset.PTBDev);

		FeatureVectorCacheFile cache = new FeatureVectorCacheFile(cacheFile,
				Models.Identifier, manager);

		StructuredProblem problem = cache.getStructuredProblem();
		cache.close();

		IdentifierThresholdTuner tuner = new IdentifierThresholdTuner(manager,
				nF, problem);

		List<Double> A = new ArrayList<Double>();
		List<Double> B = new ArrayList<Double>();

		for (double x = 0.01; x < 10; x += 0.01) {
			A.add(x);
			B.add(x);
		}

		Pair<Double, Double> pair = tuner.tuneIdentifierScale(A, B);

		manager.writeIdentifierScale(pair.getFirst(), pair.getSecond());
	}

	public void evaluate(Dataset testSet, String inferneceType)
			throws Exception {

		boolean ilp = inferneceType.equals("ilp")
				|| inferneceType.equals("beam");
		boolean noOverlap = inferneceType.equals("no-overlap");
		boolean lag = inferneceType.equals("lag");

		ILPSolverFactory solver = null;
		if (inferneceType.equals("ilp"))
			solver = new ILPSolverFactory(SolverType.CuttingPlaneGurobi);
		else if (inferneceType.equals("beam"))
			solver = new ILPSolverFactory(1000);

		String outputFilePrefix = "out/" + verbNom + "."
				+ manager.defaultParser + "." + RANDOM.nextInt();

		String goldOutFile = outputFilePrefix + ".gold";
		PrintWriter goldWriter = new PrintWriter(new File(goldOutFile));
		String predOutFile = outputFilePrefix + ".predicted";
		PrintWriter predWriter = new PrintWriter(new File(predOutFile));
		ColumnFormatWriter writer = new ColumnFormatWriter();

		ClassificationTester tester = new ClassificationTester();
		tester.ignoreLabelFromSummary("V");

		ClassificationTester senseTester = new ClassificationTester();

		Map<String, ShufflingBasedStatisticalSignificance> significance = new HashMap<String, ShufflingBasedStatisticalSignificance>();

		long start = System.currentTimeMillis();
		int count = 0;

		manager.getModelInfo(Models.Classifier).loadWeightVector();
		manager.getModelInfo(Models.Identifier).loadWeightVector();
		manager.getModelInfo(Models.Sense).loadWeightVector();

		IResetableIterator<TextAnnotation> dataset = SentenceDBHandler.instance
				.getDataset(testSet);

		while (dataset.hasNext()) {
			TextAnnotation ta = dataset.next();

			if (!ta.hasView(manager.getGoldViewName())) {
				continue;
			}

			ta.addView(new HeadFinderDependencyViewGenerator(
					manager.defaultParser));
			PredicateArgumentView gold = (PredicateArgumentView) ta
					.getView(manager.getGoldViewName());

			ISRLInference inference = null;

			if (ilp)
				inference = manager.getInference(solver, gold.getPredicates());
			else if (noOverlap)
				inference = new NoOverlapInference(manager, ta,
						gold.getPredicates(), true);
			else if (lag)
				inference = new SRLLagrangeInference(manager, ta,
						gold.getPredicates(), true, 100);

			PredicateArgumentView prediction = inference.getOutputView();

			PredicateArgumentEvaluator.evaluate(gold, prediction, tester);
			PredicateArgumentEvaluator.evaluateSense(gold, prediction,
					senseTester);

			writer.printPredicateArgumentView(gold, goldWriter);
			writer.printPredicateArgumentView(prediction, predWriter);

			count++;
			if (count % 100 == 0) {
				long end = System.currentTimeMillis();
				System.out.println(count + " sentences done. Took "
						+ (end - start) + "ms, F1 so far = "
						+ tester.getAverageF1());
			}
		}

		for (String label : significance.keySet())
			significance.get(label).runSignificanceTest();

		long end = System.currentTimeMillis();
		System.out.println(count + " sentences done. Took " + (end - start)
				+ "ms");

		System.out.println("* Arguments");
		System.out.println(tester.getPerformanceTable(false).toOrgTable());

		System.out.println("\n\n* Sense");
		System.out.println(senseTester.getPerformanceTable(false).toOrgTable());

		goldWriter.close();
		predWriter.close();

		System.out
				.println("To use standard CoNLL evaluation, compare the following files:\n"
						+ goldOutFile + " " + predOutFile);
	}

	public void compareInference(Dataset testSet) throws Exception {
		ILPSolverFactory solver = new ILPSolverFactory(
				SolverType.CuttingPlaneGurobi);

		manager.getModelInfo(Models.Classifier).loadWeightVector();
		manager.getModelInfo(Models.Identifier).loadWeightVector();
		manager.getModelInfo(Models.Sense).loadWeightVector();

		IResetableIterator<TextAnnotation> dataset = SentenceDBHandler.instance
				.getDataset(testSet);

		int count = 0;
		while (dataset.hasNext()) {
			TextAnnotation ta = dataset.next();

			if (!ta.hasView(manager.getGoldViewName())) {
				continue;
			}

			ta.addView(new HeadFinderDependencyViewGenerator(
					manager.defaultParser));
			PredicateArgumentView gold = (PredicateArgumentView) ta
					.getView(manager.getGoldViewName());

			SRLILPInference inference = manager.getInference(solver,
					gold.getPredicates());

			SRLSentenceStructure structure = inference.runInference();
			PredicateArgumentView predictionILP = structure
					.getView(manager, ta);

			NoOverlapInference noOverlapInference = new NoOverlapInference(
					manager, ta, gold.getPredicates(), true);

			PredicateArgumentView predictionNoOverlap = noOverlapInference
					.runInference();

			SRLLagrangeInference lagrangianInference = new SRLLagrangeInference(
					manager, ta, gold.getPredicates(), true, 100);

			PredicateArgumentView predictionLag = lagrangianInference
					.runInference();

			if (!predictionILP.toString().equals(predictionLag.toString())) {

				System.out.println("Example id " + count);
				System.out.println(ta);

				System.out.println();
				System.out.println("ILP: ");
				System.out.println(predictionILP);
				System.out.println();

				System.out.println("\nNo Overlap:");
				System.out.println(predictionNoOverlap);
				System.out.println();

				System.out.println("\nLagrangian:");
				System.out.println(predictionLag);

				System.out.println("Press any key");
				System.console().readLine();
			}

			count++;
			if (count % 100 == 0)
				System.out.println(count + " examples done");

		}

	}

	public void testComponent(String model_, Dataset dataset, String debug_)
			throws Exception {

		Models model = Models.valueOf(model_);

		ModelInfo modelInfo = manager.getModelInfo(model);
		modelInfo.loadWeightVector();

		switch (model) {
		case Predicate: {
			AbstractPredicateDetector detector = manager
					.getLearnedPredicateDetector();
			testPredicate(dataset, debug_, detector);
			break;
		}
		case Classifier:
			testClassifier(dataset, debug_.equals("true"));
			break;
		case Identifier: {
			testIdentifier(dataset, debug_.equals("true"));
			break;
		}
		case Sense:
			testSense(dataset, debug_.equals("true"));
			break;
		default:
			break;
		}
	}

	private void testClassifier(Dataset testSet, final boolean debug)
			throws Exception {

		// Only keep the unique label constraint.
		manager.setUnconstrainedInference(true);
		evaluate(testSet, "ilp");
	}

	private void testSense(Dataset dataset, final boolean debug)
			throws Exception, IOException {

		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(dataset);

		ClassificationTester tester = new ClassificationTester();
		SRLMulticlassInference inference = new SRLMulticlassInference(manager,
				Models.Sense);
		ModelInfo modelInfo = manager.getModelInfo(Models.Sense);

		modelInfo.loadWeightVector();

		WeightVector w = modelInfo.getWeights();

		int count = 0;
		while (data.hasNext()) {
			TextAnnotation ta = data.next();

			if (!ta.hasView(manager.getGoldViewName()))
				continue;

			Pair<SRLSentenceInstance, SRLSentenceStructure> examples = manager.exampleGenerator
					.getExamples(ta);

			for (int predicateId = 0; predicateId < examples.getFirst()
					.numPredicates(); predicateId++) {

				SRLPredicateInstance xx = examples.getFirst().predicates
						.get(predicateId);
				SRLPredicateStructure yy = examples.getSecond().ys
						.get(predicateId);

				String gold = manager.getSense(yy.getSense());

				if (gold.equals("XX"))
					continue;

				SRLMulticlassInstance x = xx.getSenseInstance();

				x.cacheFeatureVector(Models.Sense,
						modelInfo.fex.getFeatures(x.getConstituent()));

				SRLMulticlassLabel y = (SRLMulticlassLabel) inference
						.getBestStructure(w, x);

				String pred = manager.getSense(y.getLabel());

				tester.record(gold, pred);

				if (debug) {

					SRLMulticlassInference inf = new SRLMulticlassInference(
							manager, Models.Sense);

					inf.stepThrough();

					System.out.println(x);

					System.out.println(gold);

					inf.getBestStructure(w, x);
					System.out.println(pred);
					System.console().readLine();
				}

				count++;
				if (count % 100 == 0) {
					System.out.println(count + " predicates complete");
				}
			}
		}

		System.out.println(tester.getPerformanceTable(false).toOrgTable());
	}

	private void testPredicate(Dataset testSet, String debug_,
			AbstractPredicateDetector detector) throws Exception {
		PredicateDetectionTester tester = new PredicateDetectionTester(manager,
				verbNom, detector);

		if (debug_.contains("missed"))
			tester.debugMissed();

		if (debug_.contains("extra"))
			tester.debugExtra();

		if (debug_.contains("+")) {
			NomLexClasses cls = NomLexClasses.valueOf(debug_.split("\\+")[1]);
			tester.setNomClassFiter(cls);
		}

		tester.evaluate(testSet);

		// if (debug_.contains("extra"))
		tester.printExtra();

		// if (debug_.contains("missed"))
		tester.printMissing();

		tester.printPerf();

		if (verbNom == VerbNom.Nom) {
			tester.printByType();
		}
	}

	private void testIdentifier(Dataset testSet, boolean debug_)
			throws Exception {

		final ArgumentIdentifier identifier = manager.getArgumentIdentifier();

		final ClassificationTester tester = new ClassificationTester();
		ArgumentCandidateUtilities.foreach(manager, testSet,
				new ArgumentCandidateUtilities.CandidateFunction() {

					@Override
					public void run(SRLMulticlassInstance x, int goldY)
							throws Exception {

						boolean label = !manager.isNullLabel(goldY);

						// SRLMulticlassLabel l = (SRLMulticlassLabel) inference
						// .getBestStructure(weightVector, x);

						// String pred = ""
						// + (identifier.getIdentifierRawScore(x) >= 0);
						// + identifier.getIdentifierScaledDecision(x);

						Set<Feature> features = manager
								.getModelInfo(Models.Identifier).fex
								.getFeatures(x.getConstituent());

						x.cacheFeatureVector(Models.Identifier, features);

						boolean prediction = identifier
								.getIdentifierScaledDecision(x);

						tester.record(label + "", prediction + "");

					}
				});

		System.out.println(tester.getPerformanceTable(false).toOrgTable());
	}

	public void testArgumentCandidateGeneratorHeuristic(Dataset dataset)
			throws Exception {
		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(dataset);

		ArgumentCandidateGenerator candidateGenerator = manager
				.getArgumentCandidateGenerator();

		EvaluationRecord rec = new EvaluationRecord();

		while (data.hasNext()) {
			TextAnnotation ta = data.next();

			if (!ta.hasView(manager.getGoldViewName()))
				continue;

			PredicateArgumentView pav = (PredicateArgumentView) ta
					.getView(manager.getGoldViewName());

			for (Constituent pred : pav.getPredicates()) {

				List<Relation> arguments = pav.getArguments(pred);
				Set<IntPair> args = new HashSet<IntPair>();
				for (Relation r : arguments)
					args.add(r.getTarget().getSpan());

				rec.incrementGold(arguments.size());

				List<Constituent> cands = candidateGenerator
						.generateCandidates(pred);
				for (Constituent c : cands) {

					rec.incrementPredicted();

					if (args.contains(c.getSpan()))
						rec.incrementCorrect();
				}
			}

		}

		System.out.println(rec.toString());
	}

	public void testInference() throws Exception {
		String testSentence = "John went to school on Monday.";

		CuratorClient curator = new CuratorClient("trollope.cs.illinois.edu",
				9010);

		TextAnnotation ta = curator.getTextAnnotation("", "", testSentence,
				false);

		TextPreProcessor.preProcessText(ta, curator);

		ta.addView(new HeadFinderDependencyViewGenerator(manager.defaultParser));

		manager.getModelInfo(Models.Classifier).loadWeightVector();
		manager.getModelInfo(Models.Identifier).loadWeightVector();
		manager.getModelInfo(Models.Sense).loadWeightVector();

		List<Constituent> predicates = manager.getHeuristicPredicateDetector()
				.getPredicates(ta);

		NoOverlapInference inference = new NoOverlapInference(manager,
				ta, predicates, true);

		inference.setDebug();

		PredicateArgumentView pav = inference.runInference();
		System.out.println(pav);

	}

}
