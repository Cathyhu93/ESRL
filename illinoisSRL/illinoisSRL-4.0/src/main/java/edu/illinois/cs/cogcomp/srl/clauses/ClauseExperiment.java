package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.experiments.ClassificationTester;
import edu.illinois.cs.cogcomp.indsup.inference.AbstractLossSensitiveStructureFinder;
import edu.illinois.cs.cogcomp.indsup.learning.DenseVector;
import edu.illinois.cs.cogcomp.indsup.learning.StructuredProblem;
import edu.illinois.cs.cogcomp.indsup.learning.WeightVector;
import edu.illinois.cs.cogcomp.srl.data.Dataset;
import edu.illinois.cs.cogcomp.srl.learn.CrossValidationHelper.Tester;
import edu.illinois.cs.cogcomp.srl.learn.JLISCVHelper;
import edu.illinois.cs.cogcomp.srl.learn.JLISLearner;
import edu.illinois.cs.cogcomp.srl.learn.LearnerParameters;
import edu.illinois.cs.cogcomp.srl.learn.PerformanceMeasure;

public class ClauseExperiment {

	private final static Logger log = LoggerFactory
			.getLogger(ClauseExperiment.class);

	public static void train(Dataset trainingSet, double c, int numThreads)
			throws Exception {
		DenseVector.BLOCK_SIZE = 100000;
		log.info("Training with c = {} on {}", c, trainingSet);

		ClauseManager manager = new ClauseManager(false);

		AbstractLossSensitiveStructureFinder[] inference = new AbstractLossSensitiveStructureFinder[numThreads];

		for (int i = 0; i < inference.length; i++) {
			inference[i] = new ClausePredictor(manager);
		}

		StructuredProblem problem = manager.getStructuredProblem(trainingSet,
				500);

		WeightVector w = JLISLearner.trainStructSVM(inference, problem, c);

		w.save("models/clauses.model.lc");
		log.info("Saved model to models/clauses.model.lc");

		manager.getLexicon().save("models/clauses.lex");
		log.info("Saved lexicon to models/clauses.lex");
	}

	public static double cv(Dataset trainingSet) throws Exception {
		DenseVector.BLOCK_SIZE = 100000;
		log.info("CV on {}", trainingSet);

		ClauseManager manager = new ClauseManager(false);

		AbstractLossSensitiveStructureFinder[] inference = new AbstractLossSensitiveStructureFinder[20];

		for (int i = 0; i < inference.length; i++) {
			inference[i] = new ClausePredictor(manager);
		}

		StructuredProblem problem = manager.getStructuredProblem(trainingSet);
		Tester<StructuredProblem> evaluator = new Tester<StructuredProblem>() {

			@Override
			public PerformanceMeasure evaluate(StructuredProblem testSet,
					WeightVector weight,
					AbstractLossSensitiveStructureFinder inference)
					throws Exception {
				ClassificationTester results = ClauseExperiment.evaluate(
						testSet, weight, (ClausePredictor) inference);

				return new JLISCVHelper.RealMeasure(results
						.getEvaluationRecord("true").getF1());
			}
		};
		LearnerParameters params = JLISLearner.cvStructSVM(problem, inference,
				5, evaluator);

		return params.getcStruct();
	}

	public static void test(Dataset testSet) throws Exception {
		ClauseManager manager = new ClauseManager(false);

		ClausePredictor inference = new ClausePredictor(manager);

		WeightVector weights = manager.getWeightVector();

		ClassificationTester results = evaluate(
				manager.getStructuredProblem(testSet), weights, inference);

		System.out.println(results.getPerformanceTable().toOrgTable());
	}

	protected static ClassificationTester evaluate(StructuredProblem problem,
			WeightVector w, ClausePredictor inference) throws Exception {

		ClassificationTester tester = new ClassificationTester();
		for (int i = 0; i < problem.size(); i++) {
			ClauseInstance x = (ClauseInstance) problem.input_list.get(i);
			ClauseStructure goldY = (ClauseStructure) problem.output_list
					.get(i);

			ClauseStructure prediction = (ClauseStructure) inference
					.getBestStructure(w, x);

			Set<IntPair> splits = new HashSet<IntPair>(prediction.constituents);

			for (IntPair goldSpan : goldY.constituents) {

				String goldLabel = "true";

				String predLabel = "false";
				if (splits.contains(goldSpan)) {
					predLabel = "true";
					splits.remove(goldSpan);
				}

				tester.record(goldLabel, predLabel);
			}

			for (IntPair predSpan : splits) {
				tester.record("false", "true");
			}
		}

		return tester;
	}
}
