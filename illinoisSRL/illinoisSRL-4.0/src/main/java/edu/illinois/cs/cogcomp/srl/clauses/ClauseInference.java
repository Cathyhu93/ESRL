package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.HashSet;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.indsup.learning.FeatureVector;
import edu.illinois.cs.cogcomp.indsup.learning.WeightVector;

public class ClauseInference {

	ClauseSplit[][] splits;
	double[][] scores;
	private int size;
	private ClauseInstance x;

	private WeightVector w;
	private boolean debug;
	private ClauseFeatureExtractor fex;
	private ClauseManager manager;

	public ClauseInference(ClauseManager manager, ClauseInstance x,
			WeightVector w) {
		this(manager, x, w, false);
	}

	public ClauseInference(ClauseManager manager, ClauseInstance x,
			WeightVector w, boolean debug) {

		this(manager, x.ta.size(), w, debug);
		this.x = x;
	}

	private ClauseInference(ClauseManager manager, int size, WeightVector w,
			boolean debug) {
		this.manager = manager;
		this.x = null;
		this.w = w;
		this.debug = debug;
		this.size = size;
		scores = new double[size + 1][size + 1];
		splits = new ClauseSplit[size + 1][size + 1];

		fex = manager.getStructuredFeatureExtractor();
	}

	public ClauseStructure getClauseSplits() throws Exception {
		assignClauseSplits(0, size);

		ClauseStructure y = new ClauseStructure(manager, x,
				splits[0][size].splits, splits[0][size].fv);

		double sc = 0;
		for (IntPair span : splits[0][size].splits) {
			sc += scores[span.getFirst()][span.getSecond()];
		}

		y.setScore(sc);

		return y;
	}

	private void assignClauseSplits(int s, int t) throws Exception {

		if (debug) {
			System.out.println("Assigning splits for " + s + "-" + t);
		}

		if (splits[s][t] != null)
			return;

		if (s < t - 1) {
			assignClauseSplits(s + 1, t);
			assignClauseSplits(s, t - 1);
		}

		Set<IntPair> bestSplits = new HashSet<IntPair>();

		double maxScore = Double.NEGATIVE_INFINITY;

		ClauseSplit left = null;
		ClauseSplit right = null;
		for (int r = s + 1; r < t; r++) {

			ClauseSplit ls = splits[s][r];
			ClauseSplit rs = splits[r][t];

			double splitScore = ls.score + rs.score;
			if (debug)
				System.out.println("\tChecking " + s + "-" + r + " and " + r
						+ "-" + t + ". Score = " + ls.score + " + " + rs.score
						+ " = " + splitScore);

			if (splitScore > maxScore) {
				maxScore = splitScore;
				bestSplits.clear();

				left = ls;
				right = rs;

				bestSplits.addAll(left.splits);
				bestSplits.addAll(right.splits);
				if (debug)
					System.out.println("\t\t new maxer, best splits = "
							+ bestSplits);
			}
		}

		double score = maxScore;

		if (Double.isInfinite(score))
			score = 0;
		FeatureVector features;

		IntPair span = new IntPair(s, t);

		if (validSpan(s, t)) {
			Pair<FeatureVector, Double> pair = getScore(s, t, bestSplits);
			scores[s][t] = pair.getSecond();

			if (debug) {
				System.out.println("Span score = " + scores[s][t]);
			}
			if (scores[s][t] >= 0) {
				bestSplits.add(span);
				score += scores[s][t];
				features = pair.getFirst();

			} else {
				features = new FeatureVector(new int[0], new float[0]);
			}

			if (debug) {
				System.out.println("(" + s + ", " + t + ") " + features);
			}

			assert features != null;

		} else {
			if (debug) {
				System.out.println("Span score = 0. ");
			}
			features = new FeatureVector(new int[0], new float[0]);
		}
		if (debug) {
			System.out.println("best splits[" + s + "," + t + "] = "
					+ bestSplits + ", score = " + score);
		}

		if (left != null) {
			features = FeatureVector.plus(features, left.fv);
		}

		if (right != null) {
			assert right.fv != null;
			features = FeatureVector.plus(features, right.fv);
		}

		if (debug) {

			String l = left == null ? "<NULL>" : left.span + "";
			String r = right == null ? "<NULL>" : right.span + "";

			System.out.println("[" + s + "-" + t + "] -> " + l + " " + r);
			// System.out.println("Features[" + s + "-" + t + "] " + features);
		}

		splits[s][t] = new ClauseSplit(span, bestSplits, score, features);
	}

	protected boolean validSpan(int s, int t) {
		return x.isValidSpan(s, t);
	}

	protected Pair<FeatureVector, Double> getScore(int s, int t,
			Set<IntPair> bestSplits) throws Exception {

		assert validSpan(s, t);

		FeatureVector f = fex.getFeatures(x, new IntPair(s, t), bestSplits);

		return new Pair<FeatureVector, Double>(f, w.dotProduct(f));
	}

	class ClauseSplit {
		final Set<IntPair> splits;

		final FeatureVector fv;
		final double score;

		final IntPair span;

		public ClauseSplit(IntPair span, Set<IntPair> splits, double score,
				FeatureVector fv) {
			this.span = span;
			this.splits = splits;
			this.score = score;
			this.fv = fv;

			for (IntPair split : splits) {
				if (split.getFirst() < span.getFirst()) {
					System.out.println(split + " not contained in " + span
							+ "!");
					throw new RuntimeException();
				}

				if (split.getSecond() > span.getSecond()) {
					System.out.println(split + " not contained in " + span
							+ "!");
					throw new RuntimeException();
				}
			}

		}
	}

	public static void main(String[] args) throws Exception {

		final Set<IntPair> gold = new HashSet<IntPair>();
		gold.add(new IntPair(0, 1));
		gold.add(new IntPair(1, 2));
		gold.add(new IntPair(1, 3));
		gold.add(new IntPair(0, 4));

		double[] s0 = { 0, 0.1, -1.3, -0.7, 5 };
		double[] s1 = { 0, 0, 0.9, 1.4, 2 };
		double[] s2 = { 0, 0, 0, 2.1, -3 };
		double[] s3 = { 0, 0, 0, 0, -0.3 };
		final double[][] scoreMatrix = { s0, s1, s2, s3 };

		ClauseInference inference = new ClauseInference(null, 4, null, true) {
			protected boolean validSpan(int s, int t) {
				return true;
			};

			@Override
			protected Pair<FeatureVector, Double> getScore(int s, int t,
					Set<IntPair> bestSplits) throws Exception {

				if (gold.contains(new IntPair(s, t)))
					return new Pair<FeatureVector, Double>(null,
							scoreMatrix[s][t] - 1);
				else
					return new Pair<FeatureVector, Double>(null,
							scoreMatrix[s][t] + 1);
			}
		};

		Set<IntPair> pred = inference.getClauseSplits().constituents;
		System.out.println(pred);

		System.out.println(ClausePredictor.symDifference(gold, pred));
	}

	public void setDebug() {
		debug = true;
		scores = new double[size + 1][size + 1];
		splits = new ClauseSplit[size + 1][size + 1];

	}
}
