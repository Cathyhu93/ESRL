package edu.illinois.cs.cogcomp.indsup.learning;

import gnu.trove.map.hash.TIntObjectHashMap;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 
 * @author Vivek Srikumar
 * 
 */
public class DenseVector implements Serializable {
	private static final long serialVersionUID = 4496565917496408855L;

	public static int BLOCK_SIZE = 1024;

	protected TIntObjectHashMap<float[]> weights;
	protected int maxBlockStart;
	protected int blockSize = BLOCK_SIZE;

	protected boolean extendable = true;

	public DenseVector() {
		weights = new TIntObjectHashMap<float[]>();
		maxBlockStart = 0;
	}

	public DenseVector(int n) {
		// for now, we ignore the suggestion that there might be n elements in
		// this weight vector. The reason is that even with that knowledge,
		// right now, all we know is that the n elements are all zeros, which we
		// don't store.
		this();
	}

	/**
	 * @return If this vector is allowed to grow
	 */
	public boolean isExtendable() {
		return extendable;
	}

	/**
	 * Set the flag to indicate if this vector can grow or not
	 * 
	 * @param flag
	 */
	public void setExtendable(boolean flag) {
		extendable = flag;
	}

	/**
	 * return The size of this dense vector
	 */
	public int getVectorLength() {
		if (weights.size() == 0) {
			return 0;
		} else {
			return maxBlockStart + blockSize;
		}
	}

	protected float[] getBlock(int id) {
		return weights.get(id);
	}

	/**
	 * return the dot product of a sparse feature vector and the dense vector
	 * itself. Note that if the sparse vector contains some elements (feature
	 * indexes) that do not exist in the dense vector, the dot product function
	 * will ignore them (instead of throwing an exception)
	 * 
	 * @param fv
	 *            Sparse feature vector
	 * @return
	 */
	public double dotProduct(FeatureVector fv) {

		if (weights.size() == 0)
			return 0.0;

		double res = 0.0;
		float[] block = null;

		int prevBlockStart = -1;

		int[] idx = fv.getIdx();
		for (int i = 0; i < idx.length; i++) {

			int featureId = idx[i];
			if (featureId > maxBlockStart + blockSize)
				continue;

			int featureIndex = featureId % blockSize;
			int blockStart = featureId - featureIndex;

			if (prevBlockStart != blockStart) {
				if (!weights.contains(blockStart))
					continue;

				block = weights.get(blockStart);
				prevBlockStart = blockStart;
			}

			if (block[featureIndex] != 0)
				res += fv.value[i] * block[featureIndex];

		}

		return res;
	}

	/**
	 * Return the dot product of a dense feature vector and the dense vector
	 * itself
	 * 
	 * @param df
	 * @return
	 */
	public double dotProduct(DenseVector df) {
		double res = 0.0;

		int[] keys = this.weights.keys();

		for (int id = 0; id < keys.length; id++) {
			int blockId = keys[id];

			if (df.containsKey(blockId)) {
				res += dotProduct(getBlock(blockId), df.getBlock(blockId));
			}
		}

		return res;
	}

	private double dotProduct(float[] v1, float[] v2) {
		assert v1.length == v2.length;

		double res = 0;
		for (int i = 0; i < v1.length; i++) {
			res += v1[i] * v2[i];
		}

		return res;

	}

	/**
	 * Add a dense vector back into the dense vector itself
	 * <p>
	 * 
	 * w = w + alpha * dv
	 * 
	 * @param dv
	 *            the dense vector
	 * @param alpha
	 *            the scalar
	 */
	public synchronized void addDenseVector(DenseVector dv, double alpha) {

		this.maxBlockStart = Math.max(this.maxBlockStart, dv.maxBlockStart);

		int[] keys = dv.weights.keys();

		for (int kId = 0; kId < keys.length; kId++) {
			int blockId = keys[kId];

			if (containsKey(blockId)) {
				float[] block = this.getBlock(blockId);
				float[] other = dv.getBlock(blockId);

				for (int i = 0; i < block.length; i++) {
					block[i] += other[i] * alpha;
				}
			} else {
				float[] clone = dv.getBlock(blockId).clone();
				for (int i = 0; i < clone.length; i++)
					clone[i] *= alpha;
				this.weights.put(blockId, clone);
			}
		}
	}

	/**
	 * Add a sparse vector back into the dense vector itself
	 * <p>
	 * 
	 * w = w + alpha * fv
	 * 
	 * @param fv
	 *            A sparse feature vector
	 * @param alpha
	 *            The scalar
	 */
	public synchronized void addSparseFeatureVector(FeatureVector fv,
			double alpha) {

		if (alpha == 0)
			return;

		int[] idx = fv.getIdx();
		float[] value = fv.getValue();

		float a = (float) alpha;

		float[] block = null;
		int previousBlockStart = -1;

		for (int i = 0; i < idx.length; i++) {

			float update = value[i] * a;
			if (update == 0)
				continue;

			int featureId = idx[i];

			int featureIndex = featureId % blockSize;
			int blockStart = featureId - featureIndex;

			if (previousBlockStart == blockStart)
				block[featureIndex] += update;
			else if (!this.weights.contains(blockStart)) {
				block = new float[blockSize];
				block[featureIndex] = update;
				this.weights.put(blockStart, block);
				previousBlockStart = blockStart;

				this.maxBlockStart = Math.max(blockStart, maxBlockStart);
			} else {
				block = this.weights.get(blockStart);
				block[featureIndex] += update;
				previousBlockStart = blockStart;
			}

		}

	}

	/**
	 * Initialize an element of the weight vector, if the weight vector does not
	 * contain this item, allocate space internally
	 * 
	 * @param featureId
	 *            The index of the initialized item
	 * @param value
	 *            The value of the initialized item
	 */
	public synchronized void setElement(int featureId, float value) {
		int featureIdentifier = featureId % blockSize;
		int blockStart = featureId - featureIdentifier;

		float[] w;
		if (!containsKey(blockStart)) {
			// don't bother storing zeros in non-existant blocks
			if (value == 0)
				return;

			w = new float[blockSize];
			weights.put(blockStart, w);

			if (blockStart > this.maxBlockStart)
				maxBlockStart = blockStart;
		} else
			w = getBlock(blockStart);

		w[featureIdentifier] = (float) value;

	}

	public float getElement(int featureId) {

		if (this.weights.size() == 0)
			return 0f;

		int featIdentifier = featureId % blockSize;
		int blockStart = featureId - featIdentifier;

		if (!containsKey(blockStart))
			return 0f;
		else
			return getBlock(blockStart)[featIdentifier];

	}

	private boolean containsKey(int blockStart) {
		return (this.weights.get(blockStart) != null);
	}

	/**
	 * return the square of the 2-norm of this dense vector
	 * 
	 * @return
	 */
	public double getTwoNormSquare() {
		double res = 0;
		int[] keys = this.weights.keys();
		for (int i = 0; i < keys.length; i++) {

			float[] fs = getBlock(keys[i]);

			for (float f : fs)
				res += f * f;
		}
		return res;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("length = " + this.getVectorLength() + "\n");

		int[] keys = this.weights.keys().clone();

		Arrays.sort(keys);

		for (int i = 0; i < keys.length; i++) {

			int blockId = keys[i];
			float[] fs = getBlock(blockId);
			sb.append(blockId + "\t" + Arrays.toString(fs) + "\n");
		}

		return sb.toString();
	}

	public double[] getInternalArray() {
		throw new RuntimeException("Not implemented!");
	}

}
