package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.HashSet;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.indsup.inference.AbstractLossSensitiveStructureFinder;
import edu.illinois.cs.cogcomp.indsup.inference.IInstance;
import edu.illinois.cs.cogcomp.indsup.inference.IStructure;
import edu.illinois.cs.cogcomp.indsup.learning.FeatureVector;
import edu.illinois.cs.cogcomp.indsup.learning.WeightVector;

@SuppressWarnings("serial")
public class ClausePredictor extends AbstractLossSensitiveStructureFinder {

	private ClauseManager manager;

	public ClausePredictor(ClauseManager manager) {
		this.manager = manager;
	}

	@Override
	public Pair<IStructure, Double> getLossSensitiveBestStructure(
			WeightVector weight, IInstance ins, IStructure goldStructure)
			throws Exception {
		ClauseInstance x = (ClauseInstance) ins;
		final ClauseStructure gold = (ClauseStructure) goldStructure;

		ClauseInference inference = new ClauseInference(manager, x, weight) {
			protected Pair<FeatureVector, Double> getScore(int s, int t,
					Set<IntPair> bestSplits) throws Exception {
				Pair<FeatureVector, Double> pair = super.getScore(s, t,
						bestSplits);
				double score = pair.getSecond();

				if (gold.constituents.contains(new IntPair(s, t)))
					score--;
				else
					score++;
				return new Pair<FeatureVector, Double>(pair.getFirst(), score);
			};
		};

		ClauseStructure y = inference.getClauseSplits();
		Set<IntPair> splits = new HashSet<IntPair>(y.constituents);
		Set<IntPair> goldSplits = new HashSet<IntPair>(gold.constituents);

		double loss = symDifference(splits, goldSplits).size();

		// TODO Remove this line after bugfix and modify
		// ClauseStructure.toString
		y.assignScores(weight);

		// verify that the dot product + loss = the score

		// double dotProduct = weight.dotProduct(y.getFeatureVector());
		// if (Math.abs(y.score - dotProduct - loss) > 1e-1) {
		//
		// System.out.println("Gold: " + gold);
		// System.out.println(y);
		//
		// System.out.println("Score = " + y.score);
		// System.out.println("Dot product = " + dotProduct);
		// System.out.println(loss);
		//
		// inference.setDebug();
		//
		// inference.getClauseSplits();
		//
		// throw new RuntimeException("Error with inference");
		//
		// }

		return new Pair<IStructure, Double>(y, loss);

	}

	public static <T> Set<T> union(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new HashSet<T>(setA);
		tmp.addAll(setB);
		return tmp;
	}

	public static <T> Set<T> intersection(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new HashSet<T>(setA);
		tmp.retainAll(setB);
		return tmp;
	}

	public static <T> Set<T> symDifference(Set<T> setA, Set<T> setB) {
		Set<T> tmpA = union(setA, setB);
		Set<T> tmpB = intersection(setA, setB);

		tmpA.removeAll(tmpB);
		return tmpA;
	}

	@Override
	public IStructure getBestStructure(WeightVector weight, IInstance ins)
			throws Exception {
		ClauseInstance x = (ClauseInstance) ins;
		ClauseInference inference = new ClauseInference(manager, x, weight);
		return inference.getClauseSplits();
	}

}
