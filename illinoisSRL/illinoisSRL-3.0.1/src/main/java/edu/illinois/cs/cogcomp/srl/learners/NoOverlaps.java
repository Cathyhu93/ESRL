// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000057251DE43E0301CF5958704E8A7A80E59254245D1AB84800D59B387573ED6BE2EEAB2B7370201FFECA394394927F2193ED999DD91F69E92270D6981E6DFDDF3C0E4F63AA70C75EB222FCA9DA728884C8452660FE0736323C630A1B5A66C803189DFEB9F3C6D5CC798C7FD514574A13425F2BA014E6A3041648A7A368FB816E751695D64A85819F8D585C006BF3E4080F5EF38352AEF2B2BE014D73D0E260386E1DEB1AAC2958962FC69B2143883E9B470538BBC68C21F4FD82B47DA370DDE0435D46CA963F87AA0FE5BBD068F58550149D2545803849FADA364199E1F7DE347ABA2D03F3606D5ADC2C700592FB57317DA5C4DE29D43CD38F764A8A2BCD12D29753681D86D9C3AD504D27903336D1876B5D4A2389C40AEA79E5A70558368F01B24FC86A6349EA56912E3AF06AD0EBFAAADDC350727327A240378914E6BDA722419FA47CB59D7A4F3F53DF277C938728227A6939C9478BFA67E24CE0C9C90CE10E72996BC79DB9DD9BAC5B13DBE569CFF743D3680A669548F7698FA973E52C9330FB8DB1F5BC66FE313BDBD3D9A3D1363B7AA3383A37836AAC9B3E6A7E1E65D864575E316518356739300000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NoOverlaps extends ParameterizedConstraint
{
  private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier();

  public NoOverlaps() { super("edu.illinois.cs.cogcomp.srl.learners.NoOverlaps"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NoOverlaps(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      for (int j = 0; j < sentence.getTokens().length; ++j)
      {
        if (verb.getStartSpan() == j)
        {
          continue;
        }
        LinkedList containsWord = new LinkedList();
        for (Iterator I = argumentCandidates.iterator(); I.hasNext(); )
        {
          Constituent candidate = (Constituent) I.next();
          if (candidate.getStartSpan() <= j && candidate.getEndSpan() > j)
          {
            containsWord.add(candidate);
          }
        }
        if (containsWord.size() > 1)
        {
          {
            boolean LBJ2$constraint$result$0;
            {
              int LBJ$m$0 = 0;
              int LBJ$bound$0 = 1;
              for (java.util.Iterator __I0 = (containsWord).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
              {
                Constituent a = (Constituent) __I0.next();
                boolean LBJ2$constraint$result$1;
                LBJ2$constraint$result$1 = !("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("null"));
                if (LBJ2$constraint$result$1) ++LBJ$m$0;
              }
              LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
            }
            if (!LBJ2$constraint$result$0) return "false";
          }
        }
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NoOverlaps(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NoOverlaps".hashCode(); }
  public boolean equals(Object o) { return o instanceof NoOverlaps; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NoOverlaps(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      for (int j = 0; j < sentence.getTokens().length; ++j)
      {
        if (verb.getStartSpan() == j)
        {
          continue;
        }
        LinkedList containsWord = new LinkedList();
        for (Iterator I = argumentCandidates.iterator(); I.hasNext(); )
        {
          Constituent candidate = (Constituent) I.next();
          if (candidate.getStartSpan() <= j && candidate.getEndSpan() > j)
          {
            containsWord.add(candidate);
          }
        }
        if (containsWord.size() > 1)
        {
          {
            Object[] LBJ$constraint$context = new Object[1];
            LBJ$constraint$context[0] = containsWord;
            FirstOrderConstraint LBJ2$constraint$result$0 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$1 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(false, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("null"), LBJ$EAR);
              }
              LBJ2$constraint$result$0 = new AtMostQuantifier("a", containsWord, LBJ2$constraint$result$1, 1);
            }
            __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
          }
        }
      }
      currentPredicateId++;
    }

    return __result;
  }
}

