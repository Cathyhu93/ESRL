// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000D6EC13B02C0301500EFB273834D9CF10E261D548A1B4570707981EC2194FE24EE22288FFDD6540D12BEBFED387E8944395F4A06878347DC69EA8909C1E9262419D5B339C79B1A355EAE88F0DA9885DAA7620142D9A937872CA6C5B1EDFD1350B15AAF1D2ECF1B60B7B1AE4D7E164928B1ED4E81CB3BA850D5339A7AC35650DE0FB79FFC690FA73E393560BBD000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomSRLInferenceXpressMP$subjectto extends ParameterizedConstraint
{
  private static final NomNoOverlaps __NomNoOverlaps = new NomNoOverlaps();
  private static final NomNoDuplicates __NomNoDuplicates = new NomNoDuplicates();
  private static final NomReferences __NomReferences = new NomReferences();
  private static final NomLegalArguments __NomLegalArguments = new NomLegalArguments();
  private static final NomContinuences __NomContinuences = new NomContinuences();

  public NomSRLInferenceXpressMP$subjectto() { super("edu.illinois.cs.cogcomp.srl.learners.NomSRLInferenceXpressMP$subjectto"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomSRLInferenceXpressMP$subjectto(TextAnnotation)' defined on line 272 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    {
      boolean LBJ2$constraint$result$0;
      {
        boolean LBJ2$constraint$result$1;
        {
          boolean LBJ2$constraint$result$2;
          {
            boolean LBJ2$constraint$result$3;
            {
              boolean LBJ2$constraint$result$4;
              LBJ2$constraint$result$4 = __NomNoOverlaps.discreteValue(sentence).equals("true");
              if (LBJ2$constraint$result$4)
                LBJ2$constraint$result$3 = __NomLegalArguments.discreteValue(sentence).equals("true");
              else LBJ2$constraint$result$3 = false;
            }
            if (LBJ2$constraint$result$3)
              LBJ2$constraint$result$2 = __NomNoDuplicates.discreteValue(sentence).equals("true");
            else LBJ2$constraint$result$2 = false;
          }
          if (LBJ2$constraint$result$2)
            LBJ2$constraint$result$1 = __NomContinuences.discreteValue(sentence).equals("true");
          else LBJ2$constraint$result$1 = false;
        }
        if (LBJ2$constraint$result$1)
          LBJ2$constraint$result$0 = __NomReferences.discreteValue(sentence).equals("true");
        else LBJ2$constraint$result$0 = false;
      }
      if (!LBJ2$constraint$result$0) return "false";
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomSRLInferenceXpressMP$subjectto(TextAnnotation)' defined on line 272 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomSRLInferenceXpressMP$subjectto".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomSRLInferenceXpressMP$subjectto; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomSRLInferenceXpressMP$subjectto(TextAnnotation)' defined on line 272 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    {
      FirstOrderConstraint LBJ2$constraint$result$0 = null;
      {
        FirstOrderConstraint LBJ2$constraint$result$1 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$2 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$3 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$4 = null;
              LBJ2$constraint$result$4 = __NomNoOverlaps.makeConstraint(sentence);
              FirstOrderConstraint LBJ2$constraint$result$5 = null;
              LBJ2$constraint$result$5 = __NomLegalArguments.makeConstraint(sentence);
              LBJ2$constraint$result$3 = new FirstOrderConjunction(LBJ2$constraint$result$4, LBJ2$constraint$result$5);
            }
            FirstOrderConstraint LBJ2$constraint$result$6 = null;
            LBJ2$constraint$result$6 = __NomNoDuplicates.makeConstraint(sentence);
            LBJ2$constraint$result$2 = new FirstOrderConjunction(LBJ2$constraint$result$3, LBJ2$constraint$result$6);
          }
          FirstOrderConstraint LBJ2$constraint$result$7 = null;
          LBJ2$constraint$result$7 = __NomContinuences.makeConstraint(sentence);
          LBJ2$constraint$result$1 = new FirstOrderConjunction(LBJ2$constraint$result$2, LBJ2$constraint$result$7);
        }
        FirstOrderConstraint LBJ2$constraint$result$8 = null;
        LBJ2$constraint$result$8 = __NomReferences.makeConstraint(sentence);
        LBJ2$constraint$result$0 = new FirstOrderConjunction(LBJ2$constraint$result$1, LBJ2$constraint$result$8);
      }
      __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
    }

    return __result;
  }
}

