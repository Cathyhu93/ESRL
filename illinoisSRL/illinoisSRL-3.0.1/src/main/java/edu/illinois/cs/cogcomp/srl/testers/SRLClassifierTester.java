/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.testers;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.data.ColumnFormatWriter;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.srl.data.CoNLLColumnFormatReaderSRL;
import edu.illinois.cs.cogcomp.srl.features.NomFeatureHelper;
import edu.illinois.cs.cogcomp.srl.main.NomSRLSystem;
import edu.illinois.cs.cogcomp.srl.main.SRLSystem;
import edu.illinois.cs.cogcomp.srl.main.VerbSRLSystem;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

/**
 * @author Vivek Srikumar
 * 
 */
public class SRLClassifierTester {
	public static void main(String[] args) throws FileNotFoundException {

		if (args.length < 3) {
			System.err
					.println("Usage: SRLClassifierTesterNoConstraints [nom | verb] input-column-file output-column-file [beam|ilp (default ilp)]");
			System.exit(-1);
		}

		String verbNom = args[0];
		String inputFile = args[1];
		String outputFile = args[2];

		boolean beam = false;
		if (args.length == 4) {
			if (args[3].equals("beam"))
				beam = true;
		}

		SRLSystem srlSystem = null;
		if (verbNom.equals("verb"))
			srlSystem = VerbSRLSystem.getInstance();
		else if (verbNom.equals("nom"))
			srlSystem = NomSRLSystem.getInstance();
		else {
			System.err
					.println("Usage: SRLClassifierTesterNoConstraints [nom | verb] input-column-file output-column-file [beam|ilp (default ilp)]");
			System.exit(-1);
		}

		String viewName = srlSystem.getViewName();

		CoNLLColumnFormatReaderSRL reader = new CoNLLColumnFormatReaderSRL("",
				"", inputFile, viewName);

		PrintWriter goldWriter = new PrintWriter(outputFile + ".gold");
		PrintWriter predictedWriter = new PrintWriter(outputFile + ".predicted");

		ColumnFormatWriter writer = new ColumnFormatWriter();

		long start = System.currentTimeMillis();

		int count = 0;
		for (TextAnnotation ta : reader) {

			count++;
			if (!ta.hasView(viewName)) {

				for (int i = 0; i < ta.size(); i++) {
					goldWriter.println("-");
					predictedWriter.println("-");
				}
				goldWriter.println();
				predictedWriter.println();

			} else {

				PredicateArgumentView gold = (PredicateArgumentView) ta
						.getView(viewName);

				if (verbNom.equals("nom")) {
					gold = restrictToVerbal(gold);
					if (gold.getPredicates().size() == 0) {
						for (int i = 0; i < ta.size(); i++) {
							goldWriter.println("-");
							predictedWriter.println("-");
						}
						goldWriter.println();
						predictedWriter.println();
						continue;
					}

				}

				PredicateArgumentView predictedView = srlSystem
						.getSRL(ta, beam);

				writer.printPredicateArgumentView(gold, goldWriter);
				goldWriter.flush();

				writer.printPredicateArgumentView(predictedView,
						predictedWriter);
				predictedWriter.flush();

			}

			if (count % 100 == 0) {
				long end = System.currentTimeMillis();
				System.out.println(count + " examples completed. Took "
						+ (end - start) + "ms");
			}

		}

		goldWriter.close();
		predictedWriter.close();

		long end = System.currentTimeMillis();
		System.out.println("Done. Took " + (end - start) + "ms");
	}

	private static PredicateArgumentView restrictToVerbal(
			PredicateArgumentView gold) {
		PredicateArgumentView pav = new PredicateArgumentView(
				gold.getViewName(), gold.getViewGenerator(),
				gold.getTextAnnotation(), gold.getScore());

		for (Constituent p : gold.getPredicates()) {
			String predicateWord = p.getSurfaceString().toLowerCase().trim();
			String predicateLemma = p
					.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier);
			Set<NomLexClasses> nomLexClass = new HashSet<NomLexEntry.NomLexClasses>(
					NomFeatureHelper.getNomLexClass(predicateWord,
							predicateLemma));

			nomLexClass.retainAll(NomLexEntry.VERBAL);

			if (nomLexClass.size() == 0)
				continue;

			List<Relation> arguments = gold.getArguments(p);

			List<Constituent> args = new ArrayList<Constituent>();
			String[] relations = new String[arguments.size()];
			double[] scores = new double[arguments.size()];

			int rId = 0;
			for (Relation r : arguments) {
				args.add(r.getTarget().cloneForNewView(gold.getViewName()));
				relations[rId] = r.getRelationName();
				scores[rId] = r.getScore();

				rId++;
			}

			pav.addPredicateArguments(p.cloneForNewView(gold.getViewName()),
					args, relations, scores);
		}

		return pav;
	}
}
