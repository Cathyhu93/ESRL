// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B880000000000000005B7DD4F42C0401081EFB231F4D641380A7101396A08290F1D4143C57D674CD4AE26677BA643CF77765C0064DBDCC5843DDDEBBF472DB05CAD83F65963E127C704B86A0471D20FDC726CCADB2FA7D60C1A1F161268F081B67E1E9D269AB05E1D14F06E9F8F6DBEACD9EA0D76BB58867F0571EE0DED726755F31DDA1594F3AF143A50DBFBCE181C7DF76F544580EB2A6B4CBDF113A29E1B6671E5F1575801DF1BC79700D357AFD13A83CB24A1ED95BFA9673CB0D194998E0EE5C78F41DB3DF15621D7DC14D7363556A4D5E7FC3695366AAA724B7385B5AD5AB0AA814B4BC34D597A9179BFDF3C5BDD573A08939381B0D138F64507F38CA86A101DFE3A3EF13FB4B25ECD67E7A268E470E82F642DC328ED517639A8CD6C33673E91BB1B5CE6C61BB1BDCE6C63BB1FC9DD87ECE6CB0673E50BB13167362CE6C94329EFD1BB37B55E7B6A9CDA0853459FDADF15E206D05590BEC54CA3710BE069B010B68A2FB57C3B450CA1AACF6D9C452EB7D05510BECAF2165AA2FB57A38B610B68A2FB53B9A4C7FA1AA206DC52EB18055E7B6E30989B68A2FB5713110A254524A99805332BEFEF7797C7CD58D0C6E3110B6033BB9F00000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class References extends ParameterizedConstraint
{
  private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier();

  public References() { super("edu.illinois.cs.cogcomp.srl.learners.References"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'References(TextAnnotation)' defined on line 73 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getVerbPredicates(sentence);
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A0"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A0"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A1"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A1"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A2"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A2"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A3"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A3"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A4"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A4"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A5"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A5"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AA"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AA"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-ADV"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-ADV"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-CAU"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-CAU"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-DIR"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIR"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-DIS"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIS"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-EXT"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-EXT"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-LOC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-LOC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-MNR"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-MNR"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-MOD"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-MOD"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-NEG"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-NEG"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-PNC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-PNC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-PRD"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-PRD"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-REC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-REC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-TM"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-TM"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-TMP"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-TMP"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'References(TextAnnotation)' defined on line 73 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "References".hashCode(); }
  public boolean equals(Object o) { return o instanceof References; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'References(TextAnnotation)' defined on line 73 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getVerbPredicates(sentence);
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A0"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A0"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A1"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A1"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A2"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A2"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A3"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A3"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A4"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A4"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A5"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A5"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AA"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AA"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-ADV"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-ADV"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-CAU"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-CAU"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-DIR"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-DIR"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-DIS"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-DIS"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-EXT"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-EXT"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-LOC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-LOC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-MNR"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-MNR"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-MOD"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-MOD"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-NEG"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-NEG"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-PNC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-PNC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-PRD"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-PRD"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-REC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-REC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-TM"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-TM"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-TMP"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-TMP"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      currentPredicateId++;
    }

    return __result;
  }
}

