// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000058E8D4A02C030158FA23BC404CB08B257558222DA7181BD96B3066209E49655A7773908A8B279FD3EDF1B474184A128ABA2B873C180D5D481A910602C61E24B8EE54CB2A2B7181944376E9074F23A2BE41510C8C1847A0208BDE94F7356CEE06501F1C1ED8F14DE5FE024334DEB3578B81B0033BE095B2AC3F7EA8942C7FF898733E667CD7A5822E836239D449D51BAD8DD0CF3B5E2B3D4EA22D0C43A1B6D2CAF21A78ADAAD0100000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;
import java.util.*;


public class SRLInferenceBeamSearch extends ILPInference
{
  public static TextAnnotation findHead(Constituent a)
  {
    return a.getTextAnnotation();
  }


  public SRLInferenceBeamSearch() { }
  public SRLInferenceBeamSearch(TextAnnotation head)
  {
    super(head, new BeamSearch(SRLConfig.getInstance().getBeamSize()));
    constraint = new SRLInferenceBeamSearch$subjectto().makeConstraint(head);
  }

  public String getHeadType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }
  public String[] getHeadFinderTypes()
  {
    return new String[]{ "edu.illinois.cs.cogcomp.edison.sentences.Constituent" };
  }

  public Normalizer getNormalizer(Learner c)
  {
    return new Softmax();
  }
}

