package edu.illinois.cs.cogcomp.srl.main;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import LBJ2.classify.Classifier;
import LBJ2.learn.Learner;
import edu.illinois.cs.cogcomp.core.datastructures.trees.TreeParserFactory;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.SpanLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.TokenLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TreeView;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.learners.NomArgumentClassifier;
import edu.illinois.cs.cogcomp.srl.learners.NomArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.learners.NomArgumentTypeBeamSearch;
import edu.illinois.cs.cogcomp.srl.learners.NomArgumentTypeXpressMP;
import edu.illinois.cs.cogcomp.srl.utilities.NomArgumentCandidateHeuristic;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry;
import edu.illinois.cs.cogcomp.srl.utilities.NombankUtilities;
import edu.illinois.cs.cogcomp.srl.utilities.SRLUtils;

public class NomSRLSystem extends SRLSystem {

	private static NomArgumentIdentifier identifier;
	private static NomArgumentClassifier classifier;
	private static NomArgumentTypeXpressMP argumentTypeXpressMP;
	private static NomArgumentTypeBeamSearch argumentTypeBeamSearch;

	final static Set<String> coreArgs = new HashSet<String>(
			Arrays.asList(NombankUtilities.coreArguments));

	private static NomSRLSystem INSTANCE;

	public static NomSRLSystem getInstance() {

		synchronized (NomSRLSystem.class) {
			if (INSTANCE == null)
				INSTANCE = new NomSRLSystem();
		}

		return INSTANCE;
	}

	@Override
	public Learner getIdentifier() {
		return identifier;
	}

	@Override
	public Learner getArgumentClassifier() {
		return classifier;
	}

	public Classifier getSRLXpressMP() {
		return argumentTypeXpressMP;
	}

	public Classifier getSRLBeamSearch() {
		return argumentTypeBeamSearch;
	}

	@Override
	List<Constituent> getFilteredCandidatesForPredicate(
			Constituent goldPredicate) {
		return NomArgumentCandidateHeuristic
				.generateFilteredCandidatesForPredicate(goldPredicate,
						getIdentifier());
	}

	@Override
	protected List<Constituent> getPredicates(TextAnnotation ta) {
		return SRLUtils.getNomPredicates(ta, NomLexEntry.VERBAL);
	}

	@Override
	String getPredicateType() {
		return "nom";
	}

	@Override
	public String getViewName() {
		return ViewNames.NOM;
	}

	@Override
	protected void initializeClassifiers() {
		identifier = new NomArgumentIdentifier();
		classifier = new NomArgumentClassifier();
		argumentTypeBeamSearch = new NomArgumentTypeBeamSearch();
		argumentTypeXpressMP = new NomArgumentTypeXpressMP();
	}

	@Override
	protected TextAnnotation initializeDummySentence() {
		TextAnnotation ta = new TextAnnotation("", "",
				Arrays.asList("The construction of the library is complete ."));

		TokenLabelView tlv = new TokenLabelView(ViewNames.POS, "Test", ta, 1.0);
		tlv.addTokenLabel(0, "DT", 1d);
		tlv.addTokenLabel(1, "NN", 1d);
		tlv.addTokenLabel(2, "IN", 1d);
		tlv.addTokenLabel(3, "DT", 1d);
		tlv.addTokenLabel(4, "NN", 1d);
		tlv.addTokenLabel(5, "VB", 1d);
		tlv.addTokenLabel(6, "JJ", 1d);
		tlv.addTokenLabel(7, ". ", 1d);

		ta.addView(ViewNames.POS, tlv);

		ta.addView(ViewNames.NER, new SpanLabelView(ViewNames.NER, "test", ta,
				1d));

		SpanLabelView chunks = new SpanLabelView(ViewNames.SHALLOW_PARSE,
				"test", ta, 1d);

		chunks.addSpanLabel(0, 2, "NP", 1d);
		chunks.addSpanLabel(2, 3, "PP", 1d);
		chunks.addSpanLabel(3, 5, "NP", 1d);
		chunks.addSpanLabel(5, 6, "VP", 1d);
		chunks.addSpanLabel(6, 7, "ADJP", 1d);

		ta.addView(ViewNames.SHALLOW_PARSE, chunks);

		TreeView parse = new TreeView(SRLConfig.getInstance()
				.getDefaultParser(), "Charniak", ta, 1.0);

		String treeString = "(S1 (S (NP (NP (DT The) (NN construction)) (PP (IN of) (NP (DT the) (NN library)))) (VP (AUX is) (ADJP (JJ complete))) (. .)))";
		parse.setParseTree(0,
				TreeParserFactory.getStringTreeParser().parse(treeString));
		ta.addView(SRLConfig.getInstance().getDefaultParser(), parse);

		return ta;

	}

	@Override
	public boolean isCoreArgument(String label) {
		return coreArgs.contains(label);
	}

	@Override
	boolean ignorePredicatesWithoutArguments() {
		return false;
	}

	@Override
	public String getSRLSystemName() {
		return Constants.nomSRLSystemName;
	}

	@Override
	public String getSRLSystemIdentifier() {
		return Constants.nomSRLSystemIdentifier;
	}

	@Override
	public String getSRLSystemVersion() {
		return Constants.nomSRLSystemVersion;
	}

}