/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.main;

/**
 * @author Vivek Srikumar
 * 
 */
public class Constants {
	// public static final double srlIdThreshold = 0.03;
	// public static final double srlIdBeta = 0.17;

	// public static final double nomIdThreshold = 0.06;
	// public static final double nomIdBeta = 0.36;

	public static boolean VERBOSE = false;

	public final static String verbSRLSystemVersion = "3.1";

	public final static String verbSRLSystemName = "Illinois Verb Semantic Role Labeler";

	public final static String verbSRLSystemIdentifier = "verb-IllinoisSRL-v"
			+ verbSRLSystemVersion;

	public final static String nomSRLSystemVersion = "3.1";

	public final static String nomSRLSystemName = "Illinois Nominal Semantic Role Labeler";

	public final static String nomSRLSystemIdentifier = "nom-IllinoisSRL-v"
			+ verbSRLSystemVersion;
}
