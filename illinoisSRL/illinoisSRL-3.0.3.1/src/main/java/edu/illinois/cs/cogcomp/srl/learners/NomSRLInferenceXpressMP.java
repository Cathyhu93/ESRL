// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000055E81BA02C040150F75E5977D8F3065263602A403616B7A9D8934F673CDD684452FFE6C0440BC918181FC53542EB21E021AC36E9D2C7E6325A4BF20D09BA072A1473CC2AE4DB03211BEC1DB1B51E4AE5BB9C0CD4C194BB8C07BAB19EF756CEA132852607F0FF2AAE2F4035F825A6D0E603618EDB633BBC2F2E7326EB269B9D98CDD85B81F3A9CA22288B000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;
import java.util.*;


public class NomSRLInferenceXpressMP extends ILPInference
{
  public static TextAnnotation findHead(Constituent a)
  {
    return a.getTextAnnotation();
  }


  public NomSRLInferenceXpressMP() { }
  public NomSRLInferenceXpressMP(TextAnnotation head)
  {
    super(head, new XpressMPHook());
    constraint = new NomSRLInferenceXpressMP$subjectto().makeConstraint(head);
  }

  public String getHeadType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }
  public String[] getHeadFinderTypes()
  {
    return new String[]{ "edu.illinois.cs.cogcomp.edison.sentences.Constituent" };
  }

  public Normalizer getNormalizer(Learner c)
  {
    return new Softmax();
  }
}

