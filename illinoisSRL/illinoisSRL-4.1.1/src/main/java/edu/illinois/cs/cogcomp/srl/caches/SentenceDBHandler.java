package edu.illinois.cs.cogcomp.srl.caches;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;

import edu.illinois.cs.cogcomp.core.algorithms.Sorters;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.stats.OneVariableStats;
import edu.illinois.cs.cogcomp.edison.data.IResetableIterator;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.EdisonSerializationHelper;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.srl.SRLProperties;
import edu.illinois.cs.cogcomp.srl.data.Dataset;


public class SentenceDBHandler {

	private Logger log = org.slf4j.LoggerFactory
			.getLogger(SentenceDBHandler.class);

	private final String dbFile;

	public static final SentenceDBHandler instance = new SentenceDBHandler(
			SRLProperties.getInstance().getSentenceDBFile());

	private SentenceDBHandler(String dbFile) {

		this.dbFile = dbFile;

		boolean create = DBHelper.dbFileExists(dbFile);

		log.info("Sentence cache {}found", create ? "not " : "");

		DBHelper.initializeConnection(dbFile);
		log.info("Initialized connection to {}", dbFile);

		if (create) {
			createDatabase(dbFile);
		}
	}

	private void createDatabase(String dbFile) {
		try {
			log.info("Creating sentence cache database at " + dbFile);

			DBHelper.createTable(dbFile, "datasets",
					"id int not null auto_increment, name varchar(20)");

			DBHelper.createTable(dbFile, "sentences",
					"id int not null, ta blob, primary key(id)");

			DBHelper.createTable(
					dbFile,
					"sentencesToDataset",
					"sentenceId int not null, "
							+ "datasetId int not null, "
							+ "foreign key (sentenceId) references sentences(id), "
							+ "foreign key (datasetId) references datasets(id), "
							+ "primary key(sentenceId, datasetId)");

			initializeDatasets(dbFile);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void initializeDatasets(String dbFile) {
		Connection connection = DBHelper.getConnection(dbFile);
		for (Dataset d : Dataset.values()) {
			PreparedStatement stmt;
			try {

				stmt = connection
						.prepareStatement("select * from datasets where name = ?");
				stmt.setString(1, d.name());
				ResultSet rs = stmt.executeQuery();

				if (!rs.next()) {

					stmt = connection
							.prepareStatement("insert into datasets(name) values (?)");
					stmt.setString(1, d.name());
					stmt.executeUpdate();
				}
			} catch (SQLException e) {
				log.error("Error with databse access", e);
				throw new RuntimeException(e);
			}

		}
	}

	public void addTextAnnotation(Dataset dataset, TextAnnotation ta) {
		try {

			Connection connection = DBHelper.getConnection(dbFile);

			PreparedStatement stmt = connection
					.prepareStatement("select id from datasets where name = ?");
			stmt.setString(1, dataset.name());
			ResultSet rs = stmt.executeQuery();
			rs.next();
			int datasetId = rs.getInt("id");

			stmt.close();

			int id = ta.getTokenizedText().hashCode();

			stmt = connection
					.prepareStatement("select * from sentences where id = ?");
			stmt.setInt(1, id);

			rs = stmt.executeQuery();
			if (!rs.next()) {

				stmt = connection
						.prepareStatement("insert into sentences (id, ta) values (?,?)");

				byte[] bytes = serialize(ta);

				stmt.setInt(1, id);
				stmt.setBytes(2, bytes);
				stmt.executeUpdate();
			}

			stmt = connection
					.prepareStatement("select * from sentencesToDataset where sentenceId = ? and datasetId = ?");
			stmt.setInt(1, id);
			stmt.setInt(2, datasetId);

			rs = stmt.executeQuery();
			if (!rs.next()) {

				stmt = connection
						.prepareStatement("insert into sentencesToDataset (sentenceId, datasetId) values(?,?)");
				stmt.setInt(1, id);
				stmt.setInt(2, datasetId);
				stmt.executeUpdate();
			} else {
				System.out.println("Repeated in " + dataset + ": " + ta);
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void removeTextAnnotationDatasetMembership(Dataset dataset,
			TextAnnotation ta) {
		try {
			Connection connection = DBHelper.getConnection(dbFile);

			PreparedStatement stmt = connection
					.prepareStatement("select id from datasets where name = ?");
			stmt.setString(1, dataset.name());
			ResultSet rs = stmt.executeQuery();
			rs.next();
			int datasetId = rs.getInt("id");

			stmt.close();

			int id = ta.getTokenizedText().hashCode();

			stmt = connection
					.prepareStatement("select * from sentences where id = ?");
			stmt.setInt(1, id);

			rs = stmt.executeQuery();
			if (!rs.next()) {
				System.out.println("Sentence not found! ");
			}

			stmt = connection
					.prepareStatement("select * from sentencesToDataset where sentenceId = ? and datasetId = ?");
			stmt.setInt(1, id);
			stmt.setInt(2, datasetId);

			rs = stmt.executeQuery();
			if (rs.next()) {

				stmt = connection
						.prepareStatement("delete from sentencesToDataset where sentenceId =? and datasetId = ?");
				stmt.setInt(1, id);
				stmt.setInt(2, datasetId);
				stmt.executeUpdate();
			} else {
				System.out.println("Sentence never belonged to "
						+ dataset.name());
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	TextAnnotation getTextAnnotation(String tokenizedText) {
		try {
			int id = tokenizedText.hashCode();

			Connection connection = DBHelper.getConnection(dbFile);

			PreparedStatement stmt = connection
					.prepareStatement("select ta from sentences where id = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				byte[] bytes = rs.getBytes("ta");

				TextAnnotation ta = deserialize(bytes);
				return ta;
			} else
				return null;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void updateTextAnnotation(String tokenizedText, String viewName,
			View newView, boolean forceMerge) {

		TextAnnotation ta = getTextAnnotation(tokenizedText);

		if (ta == null) {

			System.out.println(newView.getTextAnnotation().getId() + ":"
					+ tokenizedText + " not found!");

			// System.exit(-1);
		} else if (addViewIfNew(viewName, newView, ta, forceMerge)) {
			updateTextAnnotation(ta);
		}

	}

	TextAnnotation getTextAnnotation(int taId) {
		try {
			Connection connection = DBHelper.getConnection(dbFile);

			PreparedStatement stmt = connection
					.prepareStatement("select ta from sentences where id = ?");
			stmt.setInt(1, taId);

			ResultSet rs = stmt.executeQuery();

			rs.next();
			byte[] bytes = rs.getBytes(1);

			return deserialize(bytes);

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void updateTextAnnotation(TextAnnotation ta) {

		try {
			Connection connection = DBHelper.getConnection(dbFile);

			int id = ta.getTokenizedText().hashCode();
			byte[] bytes = serialize(ta);
			PreparedStatement stmt = connection
					.prepareStatement("update sentences set ta = ? where id = ?");
			stmt.setBytes(1, bytes);
			stmt.setInt(2, id);

			stmt.execute();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	boolean updateView(String viewName, View newView, TextAnnotation ta) {
		View clView = cloneView(newView, ta);
		ta.addView(viewName, clView);
		return true;
	}

	boolean addViewIfNew(String viewName, View newView, TextAnnotation ta,
			boolean forceMerge) {

		boolean add = false;
		if (!ta.hasView(viewName)) {

			View clView = cloneView(newView, ta);

			ta.addView(viewName, clView);
			add = true;
		} else {

			if (!ta.getView(viewName).toString().equals(newView.toString())) {
				if (forceMerge) {
					ta.addView(viewName, newView);
					add = true;

				} else {
					System.out
							.println("Duplicate view found for "
									+ viewName
									+ " and the two are not equal. Not adding new view.");
					System.out.println(ta);
					System.out.println("Original view:\n"
							+ ta.getView(viewName));
					System.out.println("Duplicate view:\n" + newView);

					System.out.println(ta.getAvailableViews());

					// System.exit(-1);
				}
			}
		}
		return add;
	}

	private View cloneView(View newView, TextAnnotation ta) {
		View view = null;

		Object[] args = { newView.getViewName(), newView.getViewGenerator(),
				ta, newView.getScore() };
		@SuppressWarnings("rawtypes")
		Class[] argsClass = { String.class, String.class, TextAnnotation.class,
				double.class };

		try {

			Constructor<? extends View> constructor = newView.getClass()
					.getConstructor(argsClass);

			view = constructor.newInstance(args);
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}

		Map<Constituent, Constituent> cMap = new HashMap<Constituent, Constituent>();

		for (Constituent c : newView.getConstituents()) {
			Constituent c1 = new Constituent(c.getLabel(), c.getViewName(), ta,
					c.getStartSpan(), c.getEndSpan());

			for (String key : c.getAttributeKeys()) {
				c1.addAttribute(key, c.getAttribute(key));
			}
			view.addConstituent(c1);
			cMap.put(c, c1);
		}

		for (Relation r : newView.getRelations()) {
			Relation r1 = new Relation(r.getRelationName(), cMap.get(r
					.getSource()), cMap.get(r.getTarget()), r.getScore());

			view.addRelation(r1);
		}

		return view;
	}

	private byte[] serialize(TextAnnotation ta) {
		try {
			return EdisonSerializationHelper.serializeToBytes(ta);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private TextAnnotation deserialize(byte[] bytes) {
		try {
			return EdisonSerializationHelper.deserializeFromBytes(bytes);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public int getSize(Dataset dataset) {
		return getSizeInternal(dataset);
	}

	private int getSizeInternal(Dataset dataset) {
		try {
			Connection connection = DBHelper.getConnection(dbFile);
			PreparedStatement stmt = connection.prepareStatement(
					"select count(*)  from sentences, datasets, sentencesToDataset "
							+ "where datasets.name = ? and "
							+ "sentencesToDataset.datasetId = datasets.id and "
							+ "sentences.id = sentencesToDataset.sentenceId",
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			stmt.setString(1, dataset.name());
			final ResultSet rs = stmt.executeQuery();

			rs.next();

			return rs.getInt(1);

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public IResetableIterator<TextAnnotation> getDataset(final Dataset dataset) {

		try {

			Connection connection = DBHelper.getConnection(dbFile);
			PreparedStatement stmt;

			String base = "select sentences.ta, datasets.name from sentences, datasets, sentencesToDataset "
					+ "where "
					+ "sentencesToDataset.datasetId = datasets.id and "
					+ "sentences.id = sentencesToDataset.sentenceId ";

			stmt = connection.prepareStatement(base + " and datasets.name = ?",
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			stmt.setString(1, dataset.name());

			final ResultSet rs = stmt.executeQuery();

			return new IResetableIterator<TextAnnotation>() {

				@Override
				public void remove() {
				}

				@Override
				public TextAnnotation next() {
					try {

						byte[] bytes = rs.getBytes(1);
						return deserialize(bytes);
					} catch (SQLException e) {
						throw new RuntimeException(e);
					}
				}

				@Override
				public boolean hasNext() {
					try {

						return rs.next();
					} catch (SQLException e) {
						throw new RuntimeException(e);
					}
				}

				@Override
				public void reset() {
					try {
						rs.beforeFirst();
					} catch (SQLException e) {
						throw new RuntimeException(e);
					}
				}
			};

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public boolean contains(TextAnnotation ta) {
		int id = ta.getTokenizedText().hashCode();
		Connection connection = DBHelper.getConnection(dbFile);

		PreparedStatement stmt;
		try {
			stmt = connection
					.prepareStatement("select * from sentences where id = ?");

			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public void printDatasetStatistics(Dataset d) throws Exception {
		System.out.println("Dataset: " + d);

		IResetableIterator<TextAnnotation> dataset = getDataset(d);

		Counter<String> counter = new Counter<String>();

		long start = System.currentTimeMillis();

		int count = 0;
		int numTokens = 0;

		OneVariableStats stats = new OneVariableStats();

		Set<String> views = new HashSet<String>();

		while (dataset.hasNext()) {
			TextAnnotation ta = dataset.next();
			numTokens += ta.size();
			count++;
			stats.add(ta.size());

			if (count % 10000 == 0)
				System.out.println("\t" + count + " sentences seen");
			views.addAll(ta.getAvailableViews());
		}
		long end = System.currentTimeMillis();

		System.out.println("Number of sentences: " + count);
		System.out.println("Number of tokens: " + numTokens);

		System.out.println("Each sentence has " + stats.mean() + "+/-"
				+ stats.std() + " tokens ");

		System.out.println("Available views: " + views);

		System.out.println("Time to iterate through data: " + (end - start)
				+ "ms");

		System.out.println("\nIterating again to verify. ");
		dataset.reset();
		start = System.currentTimeMillis();

		int newNumTokens = 0;
		int newCount = 0;
		while (dataset.hasNext()) {
			TextAnnotation ta = dataset.next();
			newNumTokens += ta.size();
			newCount++;
			stats.add(ta.size());

			if (newCount % 10000 == 0)
				System.out.println("\t" + newCount + " sentences seen");
			Set<String> vv = ta.getAvailableViews();

			// if (!vv.equals(views)) {
			// System.out.println("\nMissing views!");
			// System.out.println(ta);
			// HashSet<String> hashSet = new HashSet<String>(views);
			// hashSet.removeAll(vv);
			// System.out.println(hashSet);
			// }

			for (String viewName : vv) {
				counter.incrementCount(viewName);
			}
		}
		end = System.currentTimeMillis();

		if (count != newCount) {
			System.out
					.println("ERROR: Number of sentences not consistent between two iterations!. "
							+ count
							+ " sentences seen first, "
							+ newCount
							+ " seen the second time.");

			throw new Exception();

		}

		if (newNumTokens != numTokens) {
			System.out
					.println("ERROR: Number of tokens not consistent between two iterations!. "
							+ numTokens
							+ " tokens seen first, "
							+ newNumTokens
							+ " seen the second time.");

			throw new Exception();

		}

		System.out.println("Time to iterate through data second time: "
				+ (end - start) + "ms");

		System.out.println("\n");

		System.out.println("View statistics: ");
		for (String viewName : Sorters.sortSet(counter.keySet())) {
			System.out.println(viewName + "\t"
					+ (int) (counter.getCount(viewName)));
		}
	}

	public void printDatabaseStatistics() throws Exception {
		try {

			Connection connection = DBHelper.getConnection(dbFile);

			System.out.println("-------- ");
			PreparedStatement stmt = connection
					.prepareStatement("select count(*) from datasets");
			ResultSet rs = stmt.executeQuery();
			rs.next();
			System.out.println("Number of datasets: " + rs.getInt(1));
			rs.close();
			stmt.close();

			stmt = connection
					.prepareStatement("select count(*) from sentences");
			rs = stmt.executeQuery();
			rs.next();
			System.out.println("Number of sentences: " + rs.getInt(1));
			rs.close();
			stmt.close();
			System.out.println("-------- ");

			for (Dataset d : Dataset.values()) {
				printDatasetStatistics(d);
				System.out.println("\n-------- \n\n");

			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}
}
