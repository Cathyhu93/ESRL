package edu.illinois.cs.cogcomp.srl.inference;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.illinois.cs.cogcomp.infer.ilp.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.algorithms.Sorters;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.infer.ilp.ILPSolverFactory.SolverType;
import edu.illinois.cs.cogcomp.srl.core.Models;
import edu.illinois.cs.cogcomp.srl.core.SRLManager;
import edu.illinois.cs.cogcomp.srl.jlis.SRLMulticlassInstance;
import edu.illinois.cs.cogcomp.srl.jlis.SRLPredicateInstance;
import edu.illinois.cs.cogcomp.srl.jlis.SRLSentenceInstance;
import edu.illinois.cs.cogcomp.srl.jlis.SRLSentenceStructure;

final public class SRLILPInference extends
		AbstractILPInference<SRLSentenceStructure> implements ISRLInference {

	final static boolean DEBUG = false;

	private final static Logger log = LoggerFactory
			.getLogger(SRLILPInference.class);

	public final SRLSentenceInstance instance;

	protected TextAnnotation ta;

	private boolean hasCands;

	protected final SRLManager manager;

	private String viewName;

	private final int numPredicates;

	private final boolean standardIdClPipeline;

	public boolean debugMode = false;

	private ILPOutput outputGenerator;

	public SRLILPInference(ILPSolverFactory solverFactory, SRLManager manager,
			List<Constituent> predicates, boolean standardIdClPipeline)
			throws Exception {
		super(solverFactory, DEBUG);
		this.manager = manager;

		this.outputGenerator = new ILPOutput(manager);

		this.standardIdClPipeline = standardIdClPipeline;

		hasCands = false;
		List<SRLPredicateInstance> instances = new ArrayList<SRLPredicateInstance>();
		for (Constituent predicate : predicates) {

			Constituent predicateClone = predicate.cloneForNewView(predicate
					.getViewName());
			SRLPredicateInstance x;

			assert predicateClone
					.hasAttribute(CoNLLColumnFormatReader.LemmaIdentifier);

			if (this.standardIdClPipeline)
				x = new SRLPredicateInstance(predicateClone, manager,
						manager.getArgumentIdentifier());
			else
				x = new SRLPredicateInstance(predicateClone, manager);

			x.cacheAllFeatureVectors(false);

			instances.add(x);

			if (x.getCandidateInstances().size() > 0)
				hasCands = true;

		}

		ta = predicates.get(0).getTextAnnotation();

		viewName = manager.getPredictedViewName();

		instance = new SRLSentenceInstance(instances);
		numPredicates = instance.numPredicates();
	}

	@Override
	protected void initializeSolver(ILPSolver xmp,
			InferenceVariableLexManager variableManager) {
		if (this.solverFactory.type == SolverType.JLISCuttingPlaneGurobi) {
			JLISCuttingPlaneILPSolverGurobi s = (JLISCuttingPlaneILPSolverGurobi) xmp;
			s.setInput(instance);
			s.setVariableManager(variableManager);
			s.setOutputGenerator(outputGenerator);
		}
	}

	public boolean hasCandidates() {
		return hasCands;
	}

	@Override
	protected SRLSentenceStructure getOutput(ILPSolver xmp,
			InferenceVariableLexManager variableManager) throws Exception {
		return outputGenerator.getOutput(xmp, variableManager, this.instance);
	}

	protected void addConstraints(ILPSolver xmp,
			List<ILPConstraint> constraints, String debugMessage) {
		log.debug(debugMessage);

		for (ILPConstraint c : constraints) {
			addConstraint(xmp, c);
		}
	}

	@Override
	protected void addConstraints(ILPSolver xmp,
			InferenceVariableLexManager variableManager) {

		if (manager.isUnconstrained())
			return;

		Set<SRLConstraints> constraints = manager.getConstraints();
		if (constraints.size() == 0)
			log.error("No constraints found. This can't be right.");

		for (SRLConstraints cc : constraints) {

			if (debugMode) {
				System.out.println("Adding constriant: " + cc);
			}

			SRLILPConstraintGenerator c = cc.getGenerator(manager);

			if (c.isDelayedConstraint()
					&& xmp instanceof JLISCuttingPlaneILPSolverGurobi) {
				((JLISCuttingPlaneILPSolverGurobi) xmp)
						.addCuttingPlaneConstraintGenerator(c);
			} else {
				List<ILPConstraint> cs = c.getILPConstraints(instance,
						variableManager);

				if (c.isDelayedConstraint()
						&& xmp instanceof CuttingPlaneILPHook) {
					((CuttingPlaneILPHook) xmp)
							.addCuttingPlaneConstraintCollection(cs);
				} else {
					addConstraints(xmp, cs, c.name);
				}
			}
		}
	}

	public void printConstraints(SRLConstraints constraint) {
		InferenceVariableLexManager variableManager = new InferenceVariableLexManager();

		xmp = solverFactory.getSolver();
		reset();

		this.addVariables(xmp, variableManager);

		List<ILPConstraint> cs = constraint.getGenerator(manager)
				.getILPConstraints(instance, variableManager);

		Set<Integer> var = new HashSet<Integer>();
		for (ILPConstraint c : cs) {
			System.out.println(c.toString());

			for (int v : c.vars)
				var.add(v);
		}

		System.out.println();
		for (int v : Sorters.sortSet(var)) {
			System.out.println(v + "\t" + variableManager.getVariableName(v));
		}
	}

	@Override
	protected void addVariables(ILPSolver xmp,
			InferenceVariableLexManager variableManager) {

		assert xmp != null;

		for (int predicateId = 0; predicateId < numPredicates; predicateId++) {

			SRLPredicateInstance x = instance.predicates.get(predicateId);

			SRLMulticlassInstance senseX = x.getSenseInstance();
			List<SRLMulticlassInstance> candidates = x.getCandidateInstances();

			// Constituent predicate = predicates.get(predicateId);

			String lemma = senseX.getPredicateLemma();
			assert lemma != null;

			log.debug("Adding variables for " + lemma);

			Set<String> legalArgs = manager.getLegalArguments(lemma);

			Set<Integer> set;
			for (int candidateId = 0; candidateId < candidates.size(); candidateId++) {

				SRLMulticlassInstance cX = candidates.get(candidateId);
				double[] scores = manager
						.getScores(cX, Models.Classifier, true);

				log.debug("\tCandidate = " + cX.toString());

				assert scores.length == manager.getAllArguments().size();

				double idScore = 0;
				if (!this.standardIdClPipeline) {
					double[] sc = manager
							.getScores(cX, Models.Identifier, true);

					idScore = sc[1] - sc[0];
				}

				set = new HashSet<Integer>();
				for (int labelId = 0; labelId < scores.length; labelId++) {
					String label = manager.getArgument(labelId);

					assert label != null : labelId + " is a null object!";

					if (!legalArgs.contains(label))
						continue;

					double score = scores[labelId];
					if (label.equals(SRLManager.NULL_LABEL))
						score -= idScore;

					String variableIdentifier = getArgumentVariableIdentifier(
							viewName, predicateId, candidateId, label);

					int var = xmp.addBooleanVariable(score);
					variableManager.addVariable(variableIdentifier, var);

					set.add(var);

					log.debug("Arg variable: " + score + " "
							+ variableIdentifier + " " + var + " " + label);
				}

				log.debug("Adding unique arg label constraint for {}",
						candidateId);
				addUniqueLabelConstraint(xmp, set);

			}

			double[] senseScores = manager
					.getScores(senseX, Models.Sense, true);

			set = new HashSet<Integer>();

			for (int senseId = 0; senseId < senseScores.length; senseId++) {

				if (!manager.isValidSense(lemma, senseId))
					continue;

				String label = manager.getSense(senseId);

				double score = senseScores[senseId];

				String variableIdentifier = getSenseVariableIdentifier(
						viewName, predicateId, label);

				int var = xmp.addBooleanVariable(score);
				variableManager.addVariable(variableIdentifier, var);

				set.add(var);
				log.debug("Sense variable: " + score + " " + variableIdentifier
						+ " " + var + " " + label);
			}

			log.debug("Adding unique sense label constraint");
			addUniqueLabelConstraint(xmp, set);

		}

		assert variableManager.size() > 0 : "No varaibles added for " + this.ta;
	}

	private void addUniqueLabelConstraint(ILPSolver xmp, Set<Integer> set) {
		int[] vars = new int[set.size()];
		double[] coeffs = new double[set.size()];
		int i = 0;
		for (int v : set) {
			vars[i] = v;
			coeffs[i++] = 1.0;
		}

		addEqualityConstraint(xmp, vars, coeffs, 1.0);
	}

	public static String getArgumentVariableIdentifier(String type,
			int predicateId, int candidateId, String label) {
		return type + ":" + predicateId + ":" + candidateId + ":" + label;
	}

	public static String getSenseVariableIdentifier(String type,
			int predicateId, String label) {
		return type + ":sense:" + predicateId + ":" + label;
	}

	public static boolean isSenseVariable(String variableName) {
		return variableName.contains(":sense:");
	}

	@Override
	public PredicateArgumentView getOutputView() throws Exception {
		return runInference().getView(manager, ta);
	}
}
