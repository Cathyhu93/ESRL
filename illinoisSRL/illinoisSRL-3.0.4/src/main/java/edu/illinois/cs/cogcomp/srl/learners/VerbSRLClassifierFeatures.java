// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000B49CC2E4E2A4D294555580B4D2A4A0E02F17EC94C2E2ECC4BCC4D227B4D4C292D2A4D26D07ECFCB2E29CC292D4DCB215846D450B1D589A589A034D1898876A005176596E52794620531C58D5B2A4A82116AA57410051E5264457000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.main.Constants;
import java.util.*;


public class VerbSRLClassifierFeatures extends Classifier
{
  private static final VerbSRLFeatures1 __VerbSRLFeatures1 = new VerbSRLFeatures1();
  private static final VerbSRLIdConjunctions __VerbSRLIdConjunctions = new VerbSRLIdConjunctions();
  private static final VerbSRLExtraFeatures __VerbSRLExtraFeatures = new VerbSRLExtraFeatures();

  public VerbSRLClassifierFeatures()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "VerbSRLClassifierFeatures";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete%"; }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'VerbSRLClassifierFeatures(Constituent)' defined on line 24 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    FeatureVector __result;
    __result = new FeatureVector();
    __result.addFeatures(__VerbSRLFeatures1.classify(__example));
    __result.addFeatures(__VerbSRLIdConjunctions.classify(__example));
    __result.addFeatures(__VerbSRLExtraFeatures.classify(__example));
    return __result;
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'VerbSRLClassifierFeatures(Constituent)' defined on line 24 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "VerbSRLClassifierFeatures".hashCode(); }
  public boolean equals(Object o) { return o instanceof VerbSRLClassifierFeatures; }

  public java.util.LinkedList getCompositeChildren()
  {
    java.util.LinkedList result = new java.util.LinkedList();
    result.add(__VerbSRLFeatures1);
    result.add(__VerbSRLIdConjunctions);
    result.add(__VerbSRLExtraFeatures);
    return result;
  }
}

