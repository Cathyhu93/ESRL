/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.testers;

import java.io.FileNotFoundException;

import edu.illinois.cs.cogcomp.core.experiments.ClassificationTester;
import edu.illinois.cs.cogcomp.core.utilities.Table;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.srl.data.SRLClassifierParser;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentClassifier;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentClassifierLabel;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.main.VerbSRLSystem;

/**
 * @author Vivek Srikumar
 * 
 */
public class SRLClassifierTesterNoConstraints {
	public static void main(String[] args) throws FileNotFoundException {

		if (args.length != 1) {
			System.err
					.println("Usage: SRLClassifierTesterNoConstraints input-column-file");
			System.exit(-1);
		}

		String inputFile = args[0];

		SRLClassifierParser parser = new SRLClassifierParser(inputFile,
				VerbSRLSystem.srlIdThreshold, VerbSRLSystem.srlIdBeta,
				new VerbArgumentIdentifier());

		ClassificationTester tester = new ClassificationTester();

		VerbArgumentClassifier classifier = new VerbArgumentClassifier();
		VerbArgumentClassifierLabel goldLabeler = new VerbArgumentClassifierLabel();

		for (Constituent c : parser) {
			if (c == null)
				break;

			String gold = goldLabeler.discreteValue(c);
			String predicted = classifier.discreteValue(c);

			tester.record(gold, predicted);
		}

		Table table = tester.getPerformanceTable();

		System.out.println(table.toOrgTable());
	}
}
