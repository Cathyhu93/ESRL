// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000055E8D4A02C030158FA2F699C6CB08B257558E24CA7181BD96310B31847A4BA2DBBB94045C5E7F87F7E5A7E8C2D23A9B4D5D70E8C4363C41B5707C4D1EACBAE1442829AF0289854B46E58350994DBEC941052E8CA37410DE606DFF491BB7C6090174ABBF72777B704871431A7D196536185CBAB2A555F9FB75C461E7F7C4AB9963B7F34E58A2D835639DC45C59AAD85B61BDB10A1850986AD000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;
import java.util.*;


public class SRLInferenceBeamSearch extends ILPInference
{
  public static TextAnnotation findHead(Constituent a)
  {
    return a.getTextAnnotation();
  }


  public SRLInferenceBeamSearch() { }
  public SRLInferenceBeamSearch(TextAnnotation head)
  {
    super(head, new BeamSearch(SRLConfig.getInstance().getBeamSize()));
    constraint = new SRLInferenceBeamSearch$subjectto().makeConstraint(head);
  }

  public String getHeadType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }
  public String[] getHeadFinderTypes()
  {
    return new String[]{ "edu.illinois.cs.cogcomp.edison.sentences.Constituent" };
  }

  public Normalizer getNormalizer(Learner c)
  {
    return new Softmax();
  }
}

