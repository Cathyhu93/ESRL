/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.data;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;

import LBJ2.parse.Parser;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;

/**
 * This class provides the abstraction for a parser for the identifier part of
 * the SRL. For verb SRL, the Xue/Palmer filter can be an implementation of this
 * class.
 * <p>
 * An object of this class can be used as a {@code Parser} for training a
 * corresponding LBJ classifier. Furthermore, it can also be within a foreach
 * loop since it implements an iterator over {@code Constituent}s. For instance,
 * we could have the following code:
 * <p>
 * 
 * <pre>
 * 
 * for (Constiuent identifierCandidate : identifierFilterParser) {
 * 	// do something with the candidate here.
 * }
 * 
 * </pre>
 * <p>
 * Note, however that this class does not implement the actual logic of the
 * filtering. In order to use it, say for SRL, one needs to subclass it to and
 * implement the function
 * {@code
 * IdentifierFilterParser#generateCandidatesForPredicate(TextAnnotation,
 * Constituent)} with the actual logic.
 * 
 * @author Vivek Srikumar
 * 
 */
public abstract class IdentifierFilterParser implements Parser,
		Iterable<Constituent>, Iterator<Constituent> {

	private final String predicateArgumentViewName;

	public CoNLLColumnFormatReaderSRL reader;
	private List<Constituent> argumentCandidates;
	private int currentPredicateId;
	protected List<Constituent> predicates;
	private TextAnnotation ta;
	private PredicateArgumentView predicateArgView;
	private int candidateIndex;
	private int exampleCount;
	private int sentenceCount;

	private int predicateCount;

	private long startTime;

	/**
	 * @param section
	 *            The section of the WSJ corpus that is being read. This is used
	 *            to assign ids to the text annotation and doesn't affect
	 *            anything else.
	 * @param conllFile
	 *            The file containing the data in column format.
	 * @param predicateArgumentViewName
	 *            The name of the view which denotes the predicate/argument
	 *            structure. For verb SRL, use {@code ViewNames#SRL} and for
	 *            nominalization, use {@code ViewNames#NOM}.
	 * @throws FileNotFoundException
	 * 
	 */
	protected IdentifierFilterParser(String section, String conllFile,
			String predicateArgumentViewName) {
		this.predicateArgumentViewName = predicateArgumentViewName;
		try {
			reader = new CoNLLColumnFormatReaderSRL("", section, conllFile,
					predicateArgumentViewName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		reset();

	}

	public Constituent next() {
		if (argumentCandidates == null
				|| candidateIndex >= argumentCandidates.size()) {
			boolean done = false;

			if (predicates == null)
				currentPredicateId = 0;
			else
				while ((++currentPredicateId < predicates.size())
						&& (done == false)) {
					argumentCandidates = generateCandidatesForPredicate(ta,
							predicates.get(currentPredicateId));
					if (argumentCandidates.size() > 0) {
						done = true;
						break;
					}
				}

			if (predicates == null || currentPredicateId >= predicates.size()) {
				done = false;

				while (!done) {
					if (!reader.hasNext()) {
						System.out.println(exampleCount
								+ " candidates generated.");
						return null;
					}
					ta = reader.next();
					sentenceCount++;

					if (ta.hasView(predicateArgumentViewName)) {

						predicateArgView = (PredicateArgumentView) ta
								.getView(predicateArgumentViewName);

						if (predicateArgView.getPredicates().size() > 0) {
							predicates = predicateArgView.getPredicates();

							predicates = filterPredicates(predicates);

							currentPredicateId = 0;

							while ((currentPredicateId < predicates.size())
									&& (!done)) {
								argumentCandidates = generateCandidatesForPredicate(
										ta, predicates.get(currentPredicateId));
								if (argumentCandidates.size() > 0)
									done = true;
								else
									currentPredicateId++;
							}

						}

					}
				}
				predicateCount++;
			}

			candidateIndex = 0;
		}
		exampleCount++;

		if (exampleCount % 10000 == 0) {
			long endTime = System.currentTimeMillis();

			long elapsed = (endTime - startTime) / 1000;

			System.out.println(exampleCount + " candidates generated. Took "
					+ elapsed + "s. ");
			// return null;
		}

		// if(exampleCount > 200000)
		// return null;

		Constituent candidate = argumentCandidates.get(candidateIndex);

		candidateIndex++;

		return candidate;

	}

	protected List<Constituent> filterPredicates(List<Constituent> predicates) {
		return predicates;
	}

	public void reset() {
		exampleCount = 0;
		predicateCount = 0;
		sentenceCount = 0;

		reader.reset();

		predicates = null;

		argumentCandidates = null;

		startTime = System.currentTimeMillis();

		// log.info("Resetting IdentifierFilterParser");
	}

	public Iterator<Constituent> iterator() {
		return this;
	}

	public boolean hasNext() {
		if (argumentCandidates == null
				|| candidateIndex >= argumentCandidates.size()) {

			if (predicates == null || currentPredicateId >= predicates.size()) {
				if (!reader.hasNext()) {
					return false;
				}
			}
		}
		return true;
	}

	public void remove() {
	}

	public abstract List<Constituent> generateCandidatesForPredicate(
			TextAnnotation ta, Constituent predicate);

}