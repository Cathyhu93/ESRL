package edu.illinois.cs.cogcomp.srl.main;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import LBJ2.classify.Classifier;
import LBJ2.learn.Learner;

public abstract class SRLSystem {

	protected static final Logger log = LoggerFactory.getLogger(SRLSystem.class);

	public static void writeThresholds(String file, double threshold,
			double beta) throws FileNotFoundException, IOException {

		BufferedOutputStream stream = new BufferedOutputStream(
				new GZIPOutputStream(new FileOutputStream(file)));

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				stream));

		writer.write("IdentifierThresholds");
		writer.newLine();

		writer.write("" + threshold);
		writer.newLine();

		writer.write("" + beta);

		writer.close();
	}

	public static Pair<Double, Double> readThresholds(InputStream in)
			throws IOException {
		GZIPInputStream zipin = new GZIPInputStream(in);
		BufferedReader reader = new BufferedReader(new InputStreamReader(zipin));

		String line;

		line = reader.readLine().trim();
		if (!line.equals("IdentifierThresholds")) {
			throw new IOException("Invalid threshold file");
		}

		double threshold = Double.parseDouble(reader.readLine().trim());
		double beta = Double.parseDouble(reader.readLine().trim());

		zipin.close();
		
		return new Pair<Double, Double>(threshold, beta);
	}

	private ExecutorService executor;

	protected SRLSystem() {

		log.info("Initializing SRL classifiers");
		initializeClassifiers();

		log.info("Finished initializing all classifiers");

		int numThreads = SRLConfig.getInstance().getNumThreads();
		executor = Executors.newFixedThreadPool(numThreads);

		runDummySentence();
	}

	double getClassifierNullScore(Constituent candidate) {
		return this.getArgumentClassifier().scores(candidate).get("null");
	}

	abstract List<Constituent> getFilteredCandidatesForPredicate(
			Constituent goldPredicate);

	protected abstract List<Constituent> getPredicates(TextAnnotation ta);

	abstract String getPredicateType();

	public PredicateArgumentView getSRL(TextAnnotation ta, boolean beamSearch) {
		return getSRL(ta, getPredicates(ta), beamSearch);
	}

	public PredicateArgumentView getSRL(TextAnnotation ta,
			List<Constituent> predicates, boolean beamSearch) {
		SemanticRoleLabeler labeler = new SemanticRoleLabeler(this, ta,
				predicates, beamSearch);

		try {
			return labeler.call();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	abstract Classifier getSRLBeamSearch();

	abstract Classifier getSRLXpressMP();

	public abstract Learner getIdentifier();

	public abstract Learner getArgumentClassifier();

	public abstract String getViewName();

	protected abstract void initializeClassifiers();

	protected abstract TextAnnotation initializeDummySentence();

	public abstract boolean isCoreArgument(String label);

	protected void runDummySentence() {

		TextAnnotation ta = initializeDummySentence();

		log.info("Loading models into memory by running SRL on a dummy sentence...");
		log.info("Dummy sentence: {} ", ta.getText());

		PredicateArgumentView srl = this.getSRL(ta, true);
		log.info("Obtained SRL for sentence: {}", srl);

		assert srl != null;

		log.info("Finished loading models. Phew!");

	}

	abstract boolean ignorePredicatesWithoutArguments();

	public abstract String getSRLSystemName();

    public abstract String getSRLCuratorName();

    public abstract String getSRLSystemVersion();

	public abstract String getSRLSystemIdentifier();
}
