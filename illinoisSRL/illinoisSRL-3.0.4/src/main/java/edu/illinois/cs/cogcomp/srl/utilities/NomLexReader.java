/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.utilities;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.core.datastructures.trees.TreeParserFactory;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

/**
 * @author Vivek Srikumar
 */
public class NomLexReader {

	// private String nomLexFile;
	private HashMap<String, List<NomLexEntry>> nomLex;
	private Map<String, String> pluralToSingularMap;

	public NomLexReader(String nomLexFile) throws IOException {
		this(new FileInputStream(nomLexFile));
	}

	public NomLexReader(InputStream stream) throws IOException {

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(stream));

		nomLex = new HashMap<String, List<NomLexEntry>>();
		pluralToSingularMap = new HashMap<String, String>();

		readNomLexLisp(reader);

	}

	private void readNomLexLisp(BufferedReader in) throws IOException {

		String line;
		int numParens = 0;
		StringBuffer current = new StringBuffer();
		while ((line = in.readLine()) != null) {

			boolean inQuotes = false;
			String prevChar = "";
			for (int i = 0; i < line.length(); i++) {
				char c = line.charAt(i);
				String c1 = c + "";

				if (c == '"') {
					if (i > 0 && line.charAt(i - 1) != '\\')
						inQuotes = !inQuotes;
				} else if (c == '(') {
					if (!inQuotes) {
						numParens++;
					} else
						c1 = "[";
				} else if (c == ')') {
					if (!inQuotes) {
						numParens--;
					} else
						c1 = "]";
				}

				if (prevChar.equals(c1) && c1.equals("("))
					current.append("DummyNode");

				current.append(c1);

				prevChar = c1;

			}

			if (numParens == 0) {

				NomLexEntry entry = createNewRecord(current.toString());

				// System.out.println(entry.nomClass + "\t" + entry.orth + "\t"
				// + entry.plural + "\t" + entry.verb + "\t" + entry.adj);

				if (!this.nomLex.containsKey(entry.orth))
					this.nomLex.put(entry.orth, new ArrayList<NomLexEntry>());

				this.nomLex.get(entry.orth).add(entry);

				current = new StringBuffer();
				numParens = 0;

			}

		}
		in.close();
	}

	private NomLexEntry createNewRecord(String string) {
		Tree<String> tree = TreeParserFactory.getStringTreeParser().parse(
				string);

		NomLexEntry entry = new NomLexEntry();

		entry.nomClass = NomLexClasses.valueOf(tree.getLabel().replaceAll("-",
				"_"));

		// for (Tree<String> key : tree.getChildren()) {

		for (int childId = 0; childId < tree.getNumberOfChildren(); childId++) {
			Tree<String> key = tree.getChild(childId);
			String label = key.getLabel();

			if (!label.startsWith(":"))
				continue;

			Tree<String> value;
			if (key.isLeaf()) {
				if (childId == tree.getNumberOfChildren() - 1)
					break;
				value = tree.getChild(childId + 1);
			} else
				value = key.getChild(0);

			String valueLabel = value.getLabel().replaceAll("\"", "");

			if (label.equals(":ORTH")) {
				entry.orth = valueLabel;
			} else if (label.equals(":VERB")) {
				entry.verb = valueLabel;
			} else if (label.equals(":ADJ")) {
				entry.adj = valueLabel;
			} else if (label.equals(":PLURAL")) {
				if (!valueLabel.equals("*NONE*"))
					entry.plural = valueLabel;
				else
					entry.plural = null;
			}
		}

		if (entry.plural == null)
			entry.plural = entry.orth;
		else
			pluralToSingularMap.put(entry.plural, entry.orth);

		assert NomLexEntry.VERBAL.contains(entry.nomClass) == (entry.verb != null) : "Nomclass = "
				+ entry.nomClass.name()
				+ ", verb = "
				+ entry.verb
				+ " for ORTH= " + entry.orth;

		if (NomLexEntry.ADJECTIVAL.contains(entry.nomClass)
				&& entry.adj == null) {
			entry.adj = entry.orth;
		}

		//
		// assert NomLexEntry.ADJECTIVAL.contains(entry.nomClass) == (entry.adj
		// != null) : "NomClass = "
		// + entry.nomClass
		// + ", adj = "
		// + entry.adj
		// + " for orth = "
		// + entry.orth;

		return entry;
	}

	/**
	 * @param line
	 * @param fieldLabel
	 * @return
	 */
	private String getField(String line, String fieldLabel) {
		String data;
		data = line.substring(line.indexOf(fieldLabel) + fieldLabel.length()
				+ 1);
		data = data.replaceAll("\\(", "");
		data = data.replaceAll("\\)", "");
		data = data.trim();
		return data;
	}

	public boolean containsEntry(String token) {
		return this.nomLex.containsKey(token) || isPlural(token);
	}

	public List<NomLexEntry> getNomLexEntry(String token) {
		if (this.nomLex.containsKey(token))
			return this.nomLex.get(token);
		else if (isPlural(token)) {
			String orth = getSingular(token);
			return this.nomLex.get(orth);
		} else
			return null;
	}

	public String getSingular(String token) {
		String orth = this.pluralToSingularMap.get(token);
		return orth;
	}

	public boolean isPlural(String token) {
		return this.pluralToSingularMap.containsKey(token);
	}

}