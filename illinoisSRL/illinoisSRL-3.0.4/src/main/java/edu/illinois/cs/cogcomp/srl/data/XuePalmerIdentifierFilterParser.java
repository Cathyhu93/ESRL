package edu.illinois.cs.cogcomp.srl.data;

import java.util.List;

import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.utilities.XuePalmerHeuristic;

public class XuePalmerIdentifierFilterParser extends IdentifierFilterParser {

	public XuePalmerIdentifierFilterParser(String section, String conllFile) {
		super(section, conllFile, ViewNames.SRL);
	}

	@Override
	public List<Constituent> generateCandidatesForPredicate(TextAnnotation ta,
			Constituent predicate) {
		return XuePalmerHeuristic.generateCandidatesForPredicate(predicate);
	}

	public void close() {

	}

}
