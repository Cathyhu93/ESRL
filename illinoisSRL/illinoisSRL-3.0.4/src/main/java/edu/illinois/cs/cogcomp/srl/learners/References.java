// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B880000000000000005B7D15F42C0301700FFA271E96B0A4454F544C46910A4208BC043EB6DDE0D623A896B312A1FBBB75120815FDEEE569567DEFFE7DB4F565C2D83F65963E127C93A54350AB8668B6F9813B4FACBE5A10786C7824C0F1032DEC3CBA5C2571AC3A38E2C43F1DDB7D59B6D33AF704BF49DEA81DE6367024521BFC5F28E3D0B4A78E9B64B0A7FFAD5038BAF76D544901C854D6988E77D26852D6B39E0CAE5475801DF15EBA30C6BC9E77C82E07C843C9B5BFA9653CB15B4A898E0EB5C78BB8EC5F742398EB761A6B8D45992579BD99C36D899AA6186F61B6B4BA47149260D25970AB2F437E27FBE70B4BBBCD8206E8E06C24D607D490EE70951D4302AFDDA3EF13FB4B25ECD66E7A268BCB4864E7C9C9430AB7DC999498CD6C35673E92BB1FC8DD876CE6C63BB1BDCE6C37673E93BB1F28DD871CE6C48DD890BB17C7C94FE18DD9B945E7B6A9CDB08534A2FB5B73CC50CA12552CA3511BE450CADF7C9908534A2FB574779A08534A2FB57C3198BFA12550CA77D390B25A2FB572DFB110B6845E7B663198BFA12550CA9B4CD1809ACF6DCBF2137D09ACF6D9D8508A41A212DC448A9195F7FFD563B9D18F48FCF2863A6170F9F00000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class References extends ParameterizedConstraint
{
  private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier();

  public References() { super("edu.illinois.cs.cogcomp.srl.learners.References"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'References(TextAnnotation)' defined on line 73 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getVerbPredicates(sentence);
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A0"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A0"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A1"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A1"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A2"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A2"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A3"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A3"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A4"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A4"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-A5"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A5"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AA"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AA"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-ADV"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-ADV"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-CAU"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-CAU"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-DIR"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIR"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-DIS"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIS"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-EXT"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-EXT"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-LOC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-LOC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-MNR"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-MNR"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-MOD"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-MOD"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-NEG"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-NEG"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-PNC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-PNC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-PRD"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-PRD"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-REC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-REC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-TM"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-TM"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-TMP"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-TMP"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'References(TextAnnotation)' defined on line 73 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "References".hashCode(); }
  public boolean equals(Object o) { return o instanceof References; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'References(TextAnnotation)' defined on line 73 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getVerbPredicates(sentence);
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A0"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A0"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A1"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A1"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A2"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A2"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A3"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A3"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A4"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A4"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-A5"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A5"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AA"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AA"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-ADV"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-ADV"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-CAU"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-CAU"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-DIR"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-DIR"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-DIS"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-DIS"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-EXT"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-EXT"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-LOC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-LOC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-MNR"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-MNR"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-MOD"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-MOD"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-NEG"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-NEG"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-PNC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-PNC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-PRD"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-PRD"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-REC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-REC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-TM"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-TM"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("R-AM-TMP"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-TMP"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      currentPredicateId++;
    }

    return __result;
  }
}

