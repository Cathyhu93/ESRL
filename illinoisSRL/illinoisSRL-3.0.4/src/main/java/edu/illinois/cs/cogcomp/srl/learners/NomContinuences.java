// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000DA79D5F6AD034168FFA85C5932A54DFCB925625A12DD09205490DA67BE626A53107A2BD8A4B3DEFBFEC12412B145B96E324240726F37E97F8D710975ADA3324967C645D22AA4B35A79257E2D2F98C59B05BEAC907AA2DCCA4DE0F140CE70E2D0DC3D27105A1410F6A6AA4A16A67F1B7C4BC7D3CBC870D56982BE8DB81958AC58396169769627FE4DCD67E94A38D63EDE33EF1D7F801798C55CAD997BEC34C9ED48900A0DC3F5A130BA6BB9605001F4ABCE5F95DC523E70E1F573AB77CAA779C30CC511E43493810836ABA5005873A414373187E100F74631B98D190D58A26391B13E8D6DFB9C5A18D0A2700A6960A27BA6EE420177B5F6B23BD61C14BE8A13968683DAC03E836895D9A7609E6FBFF6256795BBD3BF33A6EF8B794F000031FC6D105792B857CC7490DC5E6E4B7757ED4BA2B6BA2FF6655EE95DFBA45987D06A07EC8FD26D546F2243F08D57F0281692372C8BAB88D3A69EE882A0E93CECF90FA6E9CCC585B5F54DF476010BBAB26DA8E83C3961BED716275099DEF6EE20EE523E577A0E33C7D810847FFB50E49240E4D340EC8240ECC340EC9240ECD340E28240E2C340E29240E2D340687C16FF18242A609F88441ED3D88028C744AF3849644014E7229159846E5221F7F90D88028C74429BB88644014E3223C111DD11409798CD5F9844004E3223A8FB2D88028C7446C322AB32802F219498E86014E32296C44311409F88C46843E10C1F3D813597C8B58FB247FFFC44BBDBEF57AFB0AE81622D3AD00000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomContinuences extends ParameterizedConstraint
{
  private static final NomArgumentClassifier __NomArgumentClassifier = new NomArgumentClassifier();

  public NomContinuences() { super("edu.illinois.cs.cogcomp.srl.learners.NomContinuences"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomContinuences(TextAnnotation)' defined on line 156 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      for (int j = 0; j < argumentCandidates.size(); ++j)
      {
        Constituent cb = (Constituent) argumentCandidates.get(j);
        LinkedList before = new LinkedList();
        for (int k = 0; k < argumentCandidates.size(); ++k)
        {
          Constituent c = (Constituent) argumentCandidates.get(k);
          if (c.getEndSpan() <= cb.getStartSpan())
          {
            before.add(c);
          }
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A0"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A0"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A1"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A1"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A2"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A2"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A3"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A3"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A4"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A4"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A5"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A5"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-ADV"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-ADV"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-CAU"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-CAU"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-DIR"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIR"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-DIS"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIS"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-EXT"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-EXT"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-LOC"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-LOC"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-MNR"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-MNR"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-MOD"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-MOD"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-NEG"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-NEG"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-PNC"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-PNC"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-PRD"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-PRD"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-REC"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-REC"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-TM"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-TM"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-TMP"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-TMP"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomContinuences(TextAnnotation)' defined on line 156 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomContinuences".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomContinuences; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomContinuences(TextAnnotation)' defined on line 156 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      for (int j = 0; j < argumentCandidates.size(); ++j)
      {
        Constituent cb = (Constituent) argumentCandidates.get(j);
        LinkedList before = new LinkedList();
        for (int k = 0; k < argumentCandidates.size(); ++k)
        {
          Constituent c = (Constituent) argumentCandidates.get(k);
          if (c.getEndSpan() <= cb.getStartSpan())
          {
            before.add(c);
          }
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A0"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A0"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A1"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A1"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A2"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A2"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A3"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A3"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A4"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A4"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A5"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A5"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-ADV"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-ADV"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-CAU"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-CAU"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-DIR"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-DIR"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-DIS"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-DIS"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-EXT"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-EXT"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-LOC"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-LOC"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-MNR"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-MNR"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-MOD"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-MOD"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-NEG"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-NEG"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-PNC"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-PNC"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-PRD"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-PRD"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-REC"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-REC"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-TM"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-TM"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-TMP"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-TMP"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
      }
      currentPredicateId++;
    }

    return __result;
  }
}

