// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000052C813A008030140FB27A59105F30AD885902EF126C5520132E5EEA21FFE60C66A81689DC727C01C3BBD0905390B2E5A5EA370E34F444996BC0FBB70FC67548A6CB2621F2A9D29334D734F05E18274A2C5B704EFCC4B8B53F2BA23A6805A9E8EDF0016E45C6AB6000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomArgumentIdentifierLabel extends Classifier
{
  public NomArgumentIdentifierLabel()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "NomArgumentIdentifierLabel";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete"; }

  private static String[] __allowableValues = DiscreteFeature.BooleanValues;
  public static String[] getAllowableValues() { return __allowableValues; }
  public String[] allowableValues() { return __allowableValues; }


  public FeatureVector classify(Object __example)
  {
    return new FeatureVector(featureValue(__example));
  }

  public Feature featureValue(Object __example)
  {
    String result = discreteValue(__example);
    return new DiscretePrimitiveStringFeature(containingPackage, name, "", result, valueIndexOf(result), (short) allowableValues().length);
  }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'NomArgumentIdentifierLabel(Constituent)' defined on line 22 of NomSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    String __cachedValue = _discreteValue(__example);

    if (valueIndexOf(__cachedValue) == -1)
    {
      System.err.println("Classifier 'NomArgumentIdentifierLabel' defined on line 22 of NomSRLIdentifier.lbj produced '" + __cachedValue  + "' as a feature value, which is not allowable.");
      System.exit(1);
    }

    return __cachedValue;
  }

  private String _discreteValue(Object __example)
  {
    Constituent c = (Constituent) __example;

    return "" + (!c.getLabel().equals("null"));
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomArgumentIdentifierLabel(Constituent)' defined on line 22 of NomSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomArgumentIdentifierLabel".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomArgumentIdentifierLabel; }
}

