% Created 2011-10-21 Fri 14:46
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{soul}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{latexsym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\providecommand{\alert}[1]{\textbf{#1}}
\begin{document}



\title{The Illinois SRL Manual}
\author{Vivek Srikumar}
\date{}
\maketitle

\setcounter{tocdepth}{3}
\tableofcontents
\vspace*{1cm}

\section{Introduction}
\label{sec-1}

  The Illinois SRL implements the single-parse Semantic Role Labeler
  that is described in (Punyakanonk, et. al. 2008). Using a similar
  approach, it also implements a nominal SRL system for deverbal nouns
  in Nombank (See (Meyers 2007) for a detailed description of this
  class.)

  This re-implementation is entirely in Java and achieves an
  equivalent performance on the test set of the Penn Treebank as
  described in the paper. Using parse trees from the Charniak parser,
  the original work achieves an average F1 of 76.29\%. In comparison, ,
  this re-implementation gets an F1 of 76.47\% with beam search (which
  is comparable to the performance when ILP inference is used). The
  nominal SRL gets an F1 score of 66.97\% with beam search.

  

  \textbf{Citing this work} To come soon.
\section{Installation and usage}
\label{sec-2}
\subsection{Getting started}
\label{sec-2_1}

   After downloading the archive containing the SRL system, unpack it
   and run \texttt{srl.sh -v -i}. This will start the verb SRL system in the
   interactive mode, where you can enter sentences on the command line
   and get it verb semantic role labels. For nominal semantic role
   labeling, replace \texttt{-v} with \texttt{-n}.  For the first sentence alone,
   the system will take a long time to load the model to the
   memory. Subsequent sentences will be faster.  Note that this system
   requires nearly 10 GB of RAM for verb SRL and about 5 GB for
   nominals.

   

   If this works you are all set. You can now use the semantic role
   labeler in one of three modes: as a curator plugin, as a batch
   annotator and in the interactive mode.
\subsection{Configuration}
\label{sec-2_2}

   Most of the configuration to the SRL system can be provided via a
   config file. The configuration file can be specified via the
   command line option \texttt{-c <config-file>}. If this option is not
   specified, the system looks for the file \texttt{srl-config.properties} in
   the same directory.

   Here is a summary of the configuration options:


\begin{enumerate}
\item \emph{CuratorHost}: Specifies the host of the curator instance which
      provides the various inputs to the SRL system.
\item \emph{CuratorPort}: Specifies the port on which the curator is
      listening on \emph{CuratorHost}.
\item \emph{DefaultParser}: This can either be \texttt{Charniak} or
      \texttt{Stanford}. This selects the constituent parser that provides
      the features for the SRL system. It is assumed that the parser
      corresponding to the choice here is provided by the
      Curator. (Note: The SRL system has been trained using the
      Charniak parser.)
\item \emph{WordNetConfig}: Specifies the xml file that provides the
      configuration for Java WordNet Library(JWNL). An example
      configuration file is provided as \texttt{jwnl\_properties.xml}. The
      path to the WordNet dictionary should be set in this file. 
\begin{verbatim}
<param name="dictionary_path" value="/path/to/wordnet/dict/here"/>
\end{verbatim}
\item \emph{LoadWordNetConfigFromClassPath}: Specifies whether the WordNet
      config file specified in \emph{WordNetConfig} should be loaded from
      the classpath. This property can take either \texttt{true} or \texttt{false}
      values. If \texttt{true}, the system will look for the WordNet
      configuration file in the classpath. If \texttt{false} or if the
      property is not present, it loads the file from the filesystem.
\item \emph{Inference}: This can either be \texttt{BeamSearch} or \texttt{ILP} and decides
      the inference algorithm that is used to make the final
      prediction. If the choice is \texttt{BeamSearch}, in in-built beam
      search engine is used for inference. If the choice is \texttt{ILP},
      then the Gurobi ILP solver will be used. (Note: To use ILP
      inference, the Gurobi engine needs to be configured.)
\item \emph{BeamSize}: Specifies the beam size if beam search inference is
      chosen. Otherwise, this option is ignored.
\item \emph{TrimLeadingPrepositions}: Should the leading prepositions of
      arguments be trimmed. If this is set to true, then a sentence
      like ``John bought a car from Mary on Thursday for 2000 dollars.''
      would be analyzed as ``bought(A0:John, A1: the car, A2: Mary, A3:
      2000 dollars, AM-TMP: Thursday)''. If this is set to false (or if
      the argument is not present), then the leading prepositions are
      included. This gives ``bought(A0:John, A1: the car, A2: from
      Mary, A3: for 2000 dollars, AM-TMP: on Thursday)'' This option
      applies for both verbs and nouns.
\end{enumerate}
\subsection{Modes of use}
\label{sec-2_3}

   For all three modes, either \texttt{-v} or \texttt{-n} argument is required to
   indicate verb or nominal SRL respectively.
   
\subsubsection{As a Curator plugin}
\label{sec-2_3_1}

   To start the SRL system as a curator plugin, run the following command:
\begin{verbatim}
./srl.sh [-v |-n ] -s <port-number> [-t <number-of-threads>]
\end{verbatim}

   The number of threads need not be specified and defaults to using
   one thread. 

   After the server starts, the curator instance can be configured to
   use this to serve SRL outputs. The following XML snippet should be
   added on to the curator annotator descriptor file (with appropriate
   type, host and port entries):

\begin{verbatim}
<annotator>
  <type>parser</type>
  <field>srl</field>
  <host>srl-host:srlport</host>
  <requirement>sentences</requirement>
  <requirement>tokens</requirement>
  <requirement>pos</requirement>
  <requirement>ner</requirement>
  <requirement>chunk</requirement>
  <requirement>charniak</requirement>
</annotator>
\end{verbatim}

   
\subsubsection{As a batch annotator}
\label{sec-2_3_2}

   The SRL system can be used to annotate several sentences as a batch
   by running it on an input file with a set of sentences. Running the
   SRL in this form produces a CoNLL style column format with the SRL
   annotation. 

   The following command runs the SRL in batch mode:
   
\begin{verbatim}
./srl.sh [-v | -n ] -b <input-file> -o <output-file> [-w]
\end{verbatim}
   
   Each line in the input file is treated as a separate sentence. The
   option \texttt{-w} indicates that the sentences in the input file are
   whitespace tokenized. Otherwise, the curator is asked to provide
   the tokenization.
\subsubsection{Interactive mode}
\label{sec-2_3_3}

   The SRL system can be used in an interactive mode by running it
   with the \texttt{-i} option.
   
\section{Papers that used this software}
\label{sec-3}

  The following papers have used an earlier version of this software:
  
\begin{itemize}
\item G. Kundu and D. Roth, \emph{Adapting Text Instead of the Model: An Open     Domain Approach}. In Proc. of the Conference of Computational
    Natural Language Learning, 2011.
\item V. Srikumar and D. Roth, A Joint Model for Extended Semantic Role
    Labeling. Proceedings of the Conference on Empirical Methods in
    Natural Language Processing (EMNLP), 2011.
\end{itemize}

  If you use this package, please let me know and I will add the
  reference to this list here.
  
  
\section{References}
\label{sec-4}


\begin{enumerate}
\item V. Punyakanok, D. Roth and W. Yih, \emph{The importance of Syntactic      Parsing and Inference in Semantic Role Labeling}. Computational
     Linguistics, 2008.
\item A. Meyers. \emph{Those other nombank dictionaries}. Technical report,
     Technical report, New York University, 2007.
\end{enumerate}
  

\end{document}