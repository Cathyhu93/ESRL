package edu.illinois.cs.cogcomp.srl.experiment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.edison.annotators.ClauseViewGenerator;
import edu.illinois.cs.cogcomp.edison.annotators.HeadFinderDependencyViewGenerator;
import edu.illinois.cs.cogcomp.edison.annotators.PseudoParse;
import edu.illinois.cs.cogcomp.edison.annotators.ViewGenerator;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorClient;
import edu.illinois.cs.cogcomp.edison.features.factory.BrownClusterFeatureExtractor;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.srl.SRLProperties;
import edu.illinois.cs.cogcomp.srl.utilities.ResourceUtilities;

public class TextPreProcessor {

	private final static Logger log = LoggerFactory
			.getLogger(TextPreProcessor.class);

	public static void preProcessText(TextAnnotation ta, boolean tokenized)
			throws Exception {
		SRLProperties config = SRLProperties.getInstance();
		CuratorClient curator = new CuratorClient(config.curatorHost,
				config.curatorPort, tokenized);

		preProcessText(ta, curator);

		addHelperViews(ta);

	}

	public static void preProcessText(TextAnnotation ta, CuratorClient curator)
			throws EdisonException {

		SRLProperties config = SRLProperties.getInstance();
		for (String view : config.getCuratorViewsToFetch()) {

			if (ta.hasView(view))
				continue;

			try {
				boolean forceUpdate = false;
				if (view.equals(ViewNames.PARSE_CHARNIAK)) {
					curator.addCharniakParse(ta, forceUpdate);
					ta.addView(ClauseViewGenerator.CHARNIAK);
					ta.addView(PseudoParse.CHARNIAK);

					ta.addView(new HeadFinderDependencyViewGenerator(
							ViewNames.PARSE_CHARNIAK));

				} else if (view.equals(ViewNames.PARSE_BERKELEY)) {
					curator.addBerkeleyParse(ta, forceUpdate);
					ta.addView(ClauseViewGenerator.BERKELEY);
					ta.addView(PseudoParse.BERKELEY);

					ta.addView(new HeadFinderDependencyViewGenerator(
							ViewNames.PARSE_BERKELEY));

				} else if (view.equals(ViewNames.NER))
					curator.addNamedEntityView(ta, forceUpdate);
				else if (view.equals(ViewNames.SHALLOW_PARSE))
					curator.addChunkView(ta, forceUpdate);
				else if (view.equals(ViewNames.POS))
					curator.addPOSView(ta, forceUpdate);
				else if (view.equals(ViewNames.DEPENDENCY))
					curator.addEasyFirstDependencyView(ta, forceUpdate);
				else if (view.equals(ViewNames.PARSE_STANFORD)) {
					curator.addStanfordParse(ta, forceUpdate);
					ta.addView(ClauseViewGenerator.STANFORD);
					ta.addView(PseudoParse.STANFORD);
				}

			} catch (Exception e) {
				log.error("Unable to fetch view " + view + " for {}", ta);
			}
		}

	}

	public static void addViewsFromCurator(TextAnnotation ta,
			CuratorClient curator, String defaultParser) throws Exception {

		boolean forceUpdate = false;

		curator.addNamedEntityView(ta, forceUpdate);
		curator.addChunkView(ta, forceUpdate);

		curator.addPOSView(ta, forceUpdate);

		if (defaultParser.equals(ViewNames.PARSE_CHARNIAK)) {
			curator.addCharniakParse(ta, forceUpdate);

			ta.addView(ClauseViewGenerator.CHARNIAK);
			ta.addView(PseudoParse.CHARNIAK);

			ta.addView(new HeadFinderDependencyViewGenerator(
					ViewNames.PARSE_CHARNIAK));
		} else if (defaultParser.equals(ViewNames.PARSE_BERKELEY)) {
			curator.addBerkeleyParse(ta, forceUpdate);
			ta.addView(ClauseViewGenerator.BERKELEY);
			ta.addView(PseudoParse.BERKELEY);

			ta.addView(new HeadFinderDependencyViewGenerator(
					ViewNames.PARSE_BERKELEY));
		} else if (defaultParser.equals(ViewNames.PARSE_STANFORD)) {
			curator.addStanfordParse(ta, forceUpdate);
			curator.addStanfordDependencyView(ta, forceUpdate);
			ta.addView(ClauseViewGenerator.STANFORD);
			ta.addView(PseudoParse.STANFORD);
		}

	}

	public static void addHelperViews(TextAnnotation ta) throws EdisonException {
		if (!ta.hasView(ViewNames.LEMMA)) {
			ta.addView(ResourceUtilities.wordNetPlusLemmatizer);
		}

		for (ViewGenerator vg : new ViewGenerator[] {
				BrownClusterFeatureExtractor.instance100.getViewGenerator(),
				BrownClusterFeatureExtractor.instance1000.getViewGenerator(),
				BrownClusterFeatureExtractor.instance3200.getViewGenerator(),
				BrownClusterFeatureExtractor.instance320.getViewGenerator() }) {
			if (!ta.hasView(vg.getViewName()))
				ta.addView(vg);
		}
	}
}
