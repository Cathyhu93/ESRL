package edu.illinois.cs.cogcomp.srl.data;

import java.util.Iterator;

import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.srl.caches.SentenceDBHandler;

public enum Dataset {
	PTBAll {
		@Override
		public String[] getSections() {
			return new String[] { "00", "01", "02", "03", "04", "05", "06",
					"07", "08", "09", "10", "11", "12", "13", "14", "15", "16",
					"17", "18", "19", "20", "21", "22", "23", "24" };
		}
	},
	PTBTrain {
		@Override
		public String[] getSections() {
			return new String[] { "02", "03", "04", "05", "06", "07", "08",
					"09", "10", "11", "12", "13", "14", "15", "16", "17", "18",
					"19", "20", "21", "24" };
		}
	},
	PTBDev {
		@Override
		public String[] getSections() {
			return new String[] { "01", "22" };

		}
	},
	PTB23 {
		@Override
		public String[] getSections() {
			return new String[] { "23" };
		}
	},
	PTB0204 {
		@Override
		public String[] getSections() {
			return new String[] { "02", "03", "04" };
		}
	},
	PTBTrainDev {

		@Override
		public String[] getSections() {
			return new String[] { "01", "02", "03", "04", "05", "06", "07",
					"08", "09", "10", "11", "12", "13", "14", "15", "16", "17",
					"18", "19", "20", "21", "22", "24" };
		}
	};

	public abstract String[] getSections();

	public static Iterator<TextAnnotation> getPTBTrainDev() {
		return SentenceDBHandler.instance.getDataset(PTBTrainDev);
	}
}
