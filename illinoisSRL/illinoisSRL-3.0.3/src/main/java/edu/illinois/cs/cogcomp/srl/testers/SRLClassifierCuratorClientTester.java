/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.testers;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

import edu.illinois.cs.cogcomp.edison.data.ColumnFormatWriter;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorDataStructureInterface;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorViewNames;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.ParseUtils;
import edu.illinois.cs.cogcomp.srl.curator.IllinoisSRLHandler;
import edu.illinois.cs.cogcomp.srl.data.CoNLLColumnFormatReaderSRL;
import edu.illinois.cs.cogcomp.srl.main.SRLSystem;
import edu.illinois.cs.cogcomp.srl.main.VerbSRLSystem;
import edu.illinois.cs.cogcomp.srl.utilities.SRLUtils;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.Forest;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;
import edu.illinois.cs.cogcomp.thrift.curator.Curator;
import edu.illinois.cs.cogcomp.thrift.curator.Record;

/**
 * @author Vivek Srikumar
 * 
 */
public class SRLClassifierCuratorClientTester {
	public static void main(String[] args) throws FileNotFoundException,
			ServiceUnavailableException, AnnotationFailedException, TException {

		if (args.length < 2) {
			System.err
					.println("Usage: SRLClassifierTesterNoConstraints input-column-file output-column-file [beam|ilp (default ilp)]");
			System.exit(-1);
		}

		String inputFile = args[0];
		String outputFile = args[1];

		boolean beam = false;
		if (args.length == 3) {
			if (args[2].equals("beam"))
				beam = true;
		}

		CoNLLColumnFormatReaderSRL reader = new CoNLLColumnFormatReaderSRL("",
				"", inputFile, ViewNames.SRL);

		PrintWriter goldWriter = new PrintWriter(outputFile + ".gold");
		PrintWriter predictedWriter = new PrintWriter(outputFile + ".predicted");

		PrintWriter predictedCuratorWriter = new PrintWriter(outputFile
				+ ".curator.predicted");

		ColumnFormatWriter writer = new ColumnFormatWriter();

		long start = System.currentTimeMillis();

		SRLSystem srlSystem = VerbSRLSystem.getInstance();

		IllinoisSRLHandler handler = new IllinoisSRLHandler(srlSystem, beam);

		TTransport transport = new TSocket("grandma.cs.uiuc.edu", 9010);

		transport = new TFramedTransport(transport);
		TProtocol protocol = new TBinaryProtocol(transport);
		transport.open();

		Curator.Client client = new Curator.Client(protocol);

		int count = 0;
		for (TextAnnotation ta : reader) {

			count++;
			if (!ta.hasView(ViewNames.SRL)) {

				for (int i = 0; i < ta.size(); i++) {
					goldWriter.println("-");
					predictedWriter.println("-");
					predictedCuratorWriter.println("-");
				}
				goldWriter.println();
				predictedWriter.println();
				predictedCuratorWriter.println();

			} else {

				String tokenizedText = ta.getTokenizedText();

				tokenizedText = ParseUtils
						.convertBracketsFromPTBFormat(tokenizedText);

				List<String> sentences = Arrays.asList(tokenizedText);

				Record record = client.wsprovide(CuratorViewNames.charniak,
						sentences, false);

				record = client.wsprovide(CuratorViewNames.pos, sentences,
						false);

				record = client.wsprovide(CuratorViewNames.chunk, sentences,
						false);

				List<Constituent> predicates = SRLUtils.getVerbPredicates(ta);

				Forest forest = handler.parseRecord(record, predicates);

				PredicateArgumentView predictedCuratorView = CuratorDataStructureInterface
						.alignForestToPredicateArgumentView(ViewNames.SRL, ta,
								forest);

				PredicateArgumentView gold = (PredicateArgumentView) ta
						.getView(ViewNames.SRL);

				PredicateArgumentView predictedView = srlSystem.getSRL(ta,
						predicates, beam);

				if (!predictedView.toString().equals(
						predictedCuratorView.toString())) {
					System.out.println(ta);
					System.out.println(gold);

					System.out.println("\nPredicted");
					System.out.println(predictedView);
					System.out.println("\n Via curator:");
					System.out.println(predictedCuratorView);

					System.out.println(forest);

					System.exit(-1);
				}

				writer.printPredicateArgumentView(gold, goldWriter);
				writer.printPredicateArgumentView(predictedView,
						predictedWriter);
				writer.printPredicateArgumentView(predictedCuratorView,
						predictedCuratorWriter);

			}

			if (count % 100 == 0) {
				long end = System.currentTimeMillis();
				System.out.println(count + " examples completed. Took "
						+ (end - start) + "ms");
			}

		}

		goldWriter.flush();
		predictedWriter.flush();
		predictedCuratorWriter.flush();

		goldWriter.close();
		predictedWriter.close();
		predictedWriter.close();

		transport.close();

		long end = System.currentTimeMillis();
		System.out.println("Done. Took " + (end - start) + "ms");
	}
}
