Version 3.0.5
Minor changes to Curator interface to clarify name/version API, update coreUtilities dependency to v1.4, update logging dependency

Version 3.0.4 
Changes to Curator components: IllinoisSRLAssistant has the code to transfer 
btw SRL and Curator data structures; SRL Handler and Server for Verb and Nom 
now reside in Curator distribution, to remove some dependencies on Curator 
from SRL.
These changes were made as Curator was modified to work with Thrift 0.8.0.

Version 3.0.3
Minor bugfixes. Uses edison v0.2.9

Version 3.0.2
Added an option to trim leading prepositions from arguments.

Revamped the training mechanism to train using LBJ's BatchTrainer in
the code. This allows manual lexicon handling, which reduces the
memory requirements by nearly 40 percent.

Version 3.0.1
Minor bugfix

Version 3.0
A complete Java based re-implementation of the Illinois SRL from
Punyakanok 2008. This version uses LBJ to train classifiers and
for performing inference with a home-brewed beam search.
