/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.testers;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.srl.data.NomIdentifierFilterParser;
import edu.illinois.cs.cogcomp.srl.learners.NomArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.learners.NomArgumentIdentifierLabel;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

/**
 * @author Vivek Srikumar
 * 
 */
public class NomIdentifierThresholdTuner {

	private final NomArgumentIdentifier identifier;

	/**
	 * @param identifier
	 */
	public NomIdentifierThresholdTuner(NomArgumentIdentifier identifier) {
		this.identifier = identifier;
	}

	public Pair<Double, Double> tuneThreshold(String testFile, double thStart,
			double thStep, double thEnd, double beStart, double beStep,
			double beEnd) {

		Set<NomLexClasses> types = new HashSet<NomLexClasses>(Arrays.asList(
				NomLexClasses.NOM, NomLexClasses.NOMLIKE));

		NomIdentifierFilterParser parser = new NomIdentifierFilterParser("",
				testFile, types);

		NomArgumentIdentifierLabel goldLabeler = new NomArgumentIdentifierLabel();

		Map<Pair<Double, Double>, ArgumentIdentifier> identifiers = new HashMap<Pair<Double, Double>, ArgumentIdentifier>();
		for (double threshold = thStart; threshold < thEnd; threshold += thStep) {
			for (double beta = beStart; beta < beEnd; beta += beStep) {

				identifiers.put(new Pair<Double, Double>(threshold, beta),
						new ArgumentIdentifier(threshold, beta, null));
			}
		}

		System.out.println("Searching over " + identifiers.size()
				+ " parameters");

		int numExamples = 0;
		Counter<String> counter = new Counter<String>();

		for (Constituent c : parser) {

			if (c == null)
				break;

			boolean goldLabel = Boolean.parseBoolean(goldLabeler
					.discreteValue(c));

			counter.incrementCount("TOTAL");
			counter.incrementCount("TOTAL" + goldLabel);

			double rawScore = identifier.scores(c).get("true");

			// System.out.println(rawScore + "\t" + goldLabel);

			for (Pair<Double, Double> key : identifiers.keySet()) {
				Pair<Boolean, Double> prediction = identifiers.get(key)
						.thresholdScore(rawScore);

				counter.incrementCount(key + "TOTAL" + prediction.getFirst());
				if (prediction.getFirst() == goldLabel) {
					counter.incrementCount(key + "CORRECT");
					counter.incrementCount(key + "CORRECT" + goldLabel);
				}
			}

			numExamples++;
			if (numExamples % 1000 == 0) {
				System.out.println(numExamples + " examples complete");
			}
		}

		double totalGold = counter.getCount("TOTALtrue");

		double maxF3 = Double.NEGATIVE_INFINITY;
		Pair<Double, Double> maxer = null;

		System.out
				.println("(Threshold, beta)\ttotalGold\ttotalPredicted\tcorrect\tP\tR\tF1\tF2");

		Pair<Double, Double> pr = null;

		for (Pair<Double, Double> key : identifiers.keySet()) {
			double totalPredicted = counter.getCount(key + "TOTALtrue");
			double correct = counter.getCount(key + "CORRECTtrue");

			double precision = 0, recall = 0, f1 = 0, f3 = 0;

			if (totalPredicted > 0)
				precision = correct / totalPredicted;

			if (totalGold > 0)
				recall = correct / totalGold;

			if (precision + recall > 0)
				f1 = 2 * precision * recall / (precision + recall);

			if (4 * precision + recall > 0)
				f3 = 10 * precision * recall / (9 * precision + recall);

			String output = key.toString();
			output += "\t" + (int) (totalGold);
			output += "\t" + (int) (totalPredicted);
			output += "\t" + (int) (correct);

			output += "\t"
					+ StringUtils.getFormattedTwoDecimal(precision * 100);
			output += "\t" + StringUtils.getFormattedTwoDecimal(recall * 100);
			output += "\t" + StringUtils.getFormattedTwoDecimal(f1 * 100);
			output += "\t" + StringUtils.getFormattedTwoDecimal(f3 * 100);
			System.out.println(output);

			if (f3 > maxF3) {
				maxF3 = f3;
				maxer = key;
				pr = new Pair<Double, Double>(precision, recall);
			}
		}

		System.out.println();

		double precision = pr.getFirst();
		double recall = pr.getSecond();
		double f1 = 0;
		double f3 = 0;

		if (precision + recall > 0)
			f1 = 2 * precision * recall / (precision + recall);

		if (4 * precision + recall > 0)
			f3 = 10 * precision * recall / (9 * precision + recall);

		String output = maxer.toString();

		output += "\t" + StringUtils.getFormattedTwoDecimal(precision * 100);
		output += "\t" + StringUtils.getFormattedTwoDecimal(recall * 100);
		output += "\t" + StringUtils.getFormattedTwoDecimal(f1 * 100);
		output += "\t" + StringUtils.getFormattedTwoDecimal(f3 * 100);
		System.out
				.println("Based on F3 measure, recommended (threshold, beta) = "
						+ maxer);
		System.out.println(output);

		return maxer;
	}

	public static void main(String[] args) throws FileNotFoundException {
		try {
			if (args.length != 3)
				throw new Exception();
		} catch (Exception ex) {
			System.err
					.println("Usage: NomIdentifierThresholdTuner file thresholdStart:thresholdStep:thresholdEnd betaStart:betaStep:betaEnd");

			System.exit(-1);

		}

		String file = args[0];
		String th = args[1];
		String be = args[2];

		String[] parts = th.split(":");
		assert parts.length == 3;

		double thStart = Double.parseDouble(parts[0]);
		double thStep = Double.parseDouble(parts[1]);
		double thEnd = Double.parseDouble(parts[2]);

		parts = be.split(":");
		assert parts.length == 3;

		double beStart = Double.parseDouble(parts[0]);
		double beStep = Double.parseDouble(parts[1]);
		double beEnd = Double.parseDouble(parts[2]);

		NomArgumentIdentifier identifier = new NomArgumentIdentifier();

		NomIdentifierThresholdTuner tuner = new NomIdentifierThresholdTuner(
				identifier);

		tuner.tuneThreshold(file, thStart, thStep, thEnd, beStart, beStep,
				beEnd);

	}
}
