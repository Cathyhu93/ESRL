/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.data;

import java.util.Iterator;
import java.util.Set;

import LBJ2.learn.Learner;
import LBJ2.parse.Parser;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.srl.testers.ArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

/**
 * This class is an iterator that generates candidates for the argument
 * classification step of SRL. It implements {@code Parser}, and hence, can be
 * used with LBJ. Additionally, it also implements an {@link Iterator} over
 * {@link Constituent}s, which means that this class can be used with a foreach
 * loop, just like the {@link IdentifierFilterParser}.
 * <p>
 * Its constructor takes as argument an {@link IdentifierFilterParser} and a
 * trained argument identifier. It iterates over the argument identifier
 * candidates and only generates the ones that the identifier classifier
 * classifiers as positive.
 * 
 * @author Vivek Srikumar
 */
public class NomClassifierParser implements Parser, Iterable<Constituent>,
		Iterator<Constituent> {

	private ArgumentIdentifier identifer;
	private final IdentifierFilterParser identifierParser;
	private int count;

	private Constituent next = null;

	/**
	 * @param validNomClasses
	 * @param section
	 * @param conllFile
	 */
	public NomClassifierParser(String filename, double threshold, double beta,
			Learner identifier, Set<NomLexClasses> validNomClasses) {
		this.identifierParser = new NomIdentifierFilterParser("", filename,
				validNomClasses);

		this.identifer = new ArgumentIdentifier(threshold, beta, identifier);
	}

	public Constituent next() {
		if (hasNext())
			return next;
		else
			return null;
	}

	public void reset() {
		identifierParser.reset();
		next = null;
	}

	public void remove() {
	}

	@Override
	public Iterator<Constituent> iterator() {
		return this;
	}

	@Override
	public boolean hasNext() {
		for (next = identifierParser.next(); next != null
				&& !this.identifer.classify(next).getFirst(); next = identifierParser
				.next())
			;
		count++;

		if (count % 1000 == 0)
			System.out.println(count + " candidate ");

		if (next == null)
			return false;
		else
			return true;

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}
}
