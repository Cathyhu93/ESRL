package edu.illinois.cs.cogcomp.srl.curator;

import java.util.List;


import edu.illinois.cs.cogcomp.edison.data.curator.CuratorDataStructureInterface;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.srl.main.SRLSystem;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.Forest;
import edu.illinois.cs.cogcomp.thrift.curator.Record;

public class IllinoisSRLAssistant
{

    static protected void checkAvailableViews(Record record)
            throws AnnotationFailedException {
        if (!record.isSetLabelViews()
                || !record.getLabelViews().containsKey("pos")) {
            throw new AnnotationFailedException(
                    "Unable to find POS view in the input record");
        }

        if (!record.isSetLabelViews()
                || !record.getLabelViews().containsKey("chunk")) {
            throw new AnnotationFailedException(
                    "Unable to find chunk view in the input record");
        }

        if (!record.isSetParseViews())
            throw new AnnotationFailedException(
                    "Unable to find parse view in the input record");

        if (!record.getParseViews().containsKey("charniak")
                && !record.getParseViews().containsKey("stanfordParse")) {
            throw new AnnotationFailedException(
                    "Unable to find parse view in the input record"
                            + ". Expecting charniak or stanfordParse.");
        }

    }

    static public Forest performSRL(SRLSystem system_, 
                                           Record record_,
                                           List<Constituent> predicates_, 
                                           boolean beamSearch_ ) throws AnnotationFailedException 
    {
        checkAvailableViews( record_ );
        
        TextAnnotation ta = CuratorDataStructureInterface
                .getTextAnnotationViewsFromRecord("", "", record_);

        PredicateArgumentView srl = system_.getSRL(ta, predicates_, beamSearch_);

        Forest srlForest = CuratorDataStructureInterface
                .convertPredicateArgumentViewToForest(srl);

        return srlForest;

    }

    static public Forest performSRL(SRLSystem system_, Record record_, boolean beamSearch_ ) throws AnnotationFailedException 
    {

        checkAvailableViews( record_ );
        
        TextAnnotation ta = CuratorDataStructureInterface
                .getTextAnnotationViewsFromRecord("", "", record_ );

        PredicateArgumentView srl = system_.getSRL(ta, beamSearch_);

        Forest srlForest = CuratorDataStructureInterface
                .convertPredicateArgumentViewToForest(srl);

        return srlForest;

    }

}
