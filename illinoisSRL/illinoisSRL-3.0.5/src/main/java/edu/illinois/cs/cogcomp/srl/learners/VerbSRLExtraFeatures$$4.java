// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// discrete% VerbSRLExtraFeatures$$4(Constituent c) <- PredicateLemma && VerbSRLFeatures1

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.main.Constants;
import java.util.*;


public class VerbSRLExtraFeatures$$4 extends Classifier
{
  private static final PredicateLemma left = new PredicateLemma();
  private static final VerbSRLFeatures1 right = new VerbSRLFeatures1();

  public VerbSRLExtraFeatures$$4()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "VerbSRLExtraFeatures$$4";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete%"; }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'VerbSRLExtraFeatures$$4(Constituent)' defined on line 22 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    FeatureVector __result;
    __result = new FeatureVector();
    Feature lf = left.featureValue(__example);
    FeatureVector rightVector = right.classify(__example);
    int M = rightVector.featuresSize();
    for (int j = 0; j < M; ++j)
    {
      Feature rf = rightVector.getFeature(j);
      __result.addFeature(lf.conjunction(rf, this));
    }

    return __result;
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'VerbSRLExtraFeatures$$4(Constituent)' defined on line 22 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "VerbSRLExtraFeatures$$4".hashCode(); }
  public boolean equals(Object o) { return o instanceof VerbSRLExtraFeatures$$4; }
}

