package edu.illinois.cs.cogcomp.srl.utilities;

import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;

public class SRLEval {

	public static class EvalRecord {
		public int numTokens;
		public double precision;
		public double recall;
		public double f1;

		public int correctArgs;
		public int goldArgs;
		public int predictedArgs;
	}

	public static EvalRecord evaluate(PredicateArgumentView g,
			PredicateArgumentView p) {

		if (g.getPredicates().size() == 0)
			return null;

		EvalRecord record = new EvalRecord();
		record.numTokens = g.getTextAnnotation().size();

		for (Constituent gp : g.getPredicates()) {

			Constituent pp = null;
			for (Constituent c : p.getPredicates()) {
				if (c.getSpan().equals(gp.getSpan())) {
					pp = c;
					break;
				}
			}

			if (pp == null) {
				record.goldArgs += g.getArguments(gp).size();
				continue;
			}

			for (Relation gar : g.getArguments(gp)) {
				Constituent ga = gar.getTarget();

				record.goldArgs++;

				boolean foundCorrespondingArgument = false;
				for (Relation par : p.getArguments(pp)) {
					Constituent pa = par.getTarget();
					if (!pa.getSpan().equals(ga.getSpan()))
						continue;

					foundCorrespondingArgument = true;
					if (!pa.getLabel().equals("null")) {

						record.predictedArgs++;
						if (ga.getLabel().equals(pa.getLabel())) {

							record.correctArgs++;
						}

					}
					break;
				}

			}

			for (Relation par : p.getArguments(pp)) {
				Constituent pa = par.getTarget();

				boolean found = false;

				for (Relation gar : g.getArguments(gp)) {
					Constituent ga = gar.getTarget();

					if (ga.getSpan().equals(pa.getSpan())) {
						found = true;
						break;
					}
				}

				if (!found)
					record.predictedArgs++;
			}
		}

		if (record.predictedArgs > 0)
			record.precision = 100.0 * record.correctArgs
					/ record.predictedArgs;
		else
			record.precision = 0;

		if (record.goldArgs > 0)
			record.recall = 100.0 * record.correctArgs / record.goldArgs;
		else
			record.recall = 0;

		if (record.precision > 0 && record.recall > 0) {
			record.f1 = 2 * record.precision * record.recall
					/ (record.precision + record.recall);
		} else
			record.f1 = 0;

		return record;

	}
}
