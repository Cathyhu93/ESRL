#!/bin/sh
START=$PWD
DIRNAME=`dirname "$0"`
SCRIPTS_HOME=`cd "$DIRNAME" > /dev/null && pwd`
CURATOR_BASE=$SCRIPTS_HOME/..
CURATOR_BASE=`cd "$CURATOR_BASE" > /dev/null && pwd`
LIBDIR=$CURATOR_BASE/lib
COMPONENTDIR=$CURATOR_BASE/components

COMPONENT_CLASSPATH=$CURATOR_BASE:$COMPONENTDIR/illinois--srl-server.jar:$COMPONENTDIR/curator-interfaces.jar

MODEL_CLASSPATH=$LIBDIR/illinoisSRL-verb-models-3.0.jar

LIB_CLASSPATH=$LIBDIR/illinoisSRL-3.0.jar:$LIBDIR/commons-cli-1.2.jar:$LIBDIR/commons-collections-3.2.1.jar:$LIBDIR/commons-configuration-1.6.jar:$LIBDIR/commons-lang-2.5.jar:$LIBDIR/commons-logging-1.1.1.jar:$LIBDIR/coreUtilities-0.1.2.jar:$LIBDIR/curator-client-0.6.jar:$LIBDIR/edison-0.2.6.jar:$LIBDIR/jwnl-1.4_rc3.jar:$LIBDIR/LBJ-2.8.2.jar:$LIBDIR/LBJLibrary-2.8.2.jar:$LIBDIR/libthrift.jar:$LIBDIR/logback-classic-0.9.28.jar:$LIBDIR/logback-core-0.9.28.jar:$LIBDIR/slf4j-api-1.6.1.jar:$LIBDIR/snowball-1.0.jar

CLASSPATH=$COMPONENT_CLASSPATH:$LIB_CLASSPATH

cd $CURATOR_BASE
echo java -cp $CLASSPATH -Dhome=$CURATOR_BASE -Xmx1G edu.illinois.cs.cogcomp.annotation.server.IllinoisVerbRLServer $@
exec java -cp $CLASSPATH -Dhome=$CURATOR_BASE -Xmx1G edu.illinois.cs.cogcomp.annotation.server.IllinoisVerbSRLServer $@
cd $START