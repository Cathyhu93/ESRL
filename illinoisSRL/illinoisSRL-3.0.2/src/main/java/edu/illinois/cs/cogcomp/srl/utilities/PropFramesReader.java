/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.utilities;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.utilities.XMLUtils;

/**
 * @author vsrikum2
 * 
 *         Feb 11, 2010
 */
public class PropFramesReader {

	private static final Set<String> prepositions = new HashSet<String>(
			Arrays.asList(new String[] { "about", "above", "across", "after",
					"against", "along", "among", "around", "as", "at",
					"before", "behind", "beneath", "beside", "between", "by",
					"down", "during", "for", "from", "in", "inside", "into",
					"like", "of", "off", "on", "onto", "over", "round",
					"through", "to", "towards", "with" }));

	public static final String UNKNOWN_VERB_CLASS = "UNKNOWN";
	private String framesDir;

	public PropFramesReader(String dir) throws Exception {
		this.framesDir = dir;

		String[] files = IOUtils.ls(dir);

		readFrameData(files);
	}

	/**
	 * @param files2
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	private void readFrameData(String[] files)
			throws ParserConfigurationException, SAXException, IOException {
		frameData = new HashMap<String, PropFrameData>();

		for (String file : files) {

			if (!file.endsWith("xml"))
				continue;

			// System.out.println(file);

			Element doc = XMLUtils
					.getXMLDocumentElement(framesDir + "/" + file);
			String lemma = ((Element) (doc.getElementsByTagName("predicate")
					.item(0))).getAttribute("lemma");

			if (lemma.equals(""))
				lemma = IOUtils.stripFileExtension(file);

			if (file.startsWith("re") && !file.startsWith(lemma))
				lemma = IOUtils.stripFileExtension(file);

			lemma = IOUtils.stripFileExtension(file);

			PropFrameData propFrameData = new PropFrameData(lemma, file);
			frameData.put(lemma, propFrameData);

			NodeList roleSets = doc.getElementsByTagName("roleset");

			// System.out.println("\n" + lemma);

			for (int i = 0; i < roleSets.getLength(); i++) {
				Element roleSet = (Element) roleSets.item(i);

				String sense = roleSet.getAttribute("id");

				// WTF frame makers?
				if (sense.equals("lionise.01"))
					sense = "lionize.01";
				// if (sense.equals("co-author.01"))
				// sense = "coauthor.01";

				assert sense.startsWith(IOUtils.stripFileExtension(file))
						|| sense.startsWith(lemma) : lemma + "\t" + sense;

				String verbClass;
				if (roleSet.hasAttribute("vncls")) {
					verbClass = roleSet.getAttribute("vncls");
					if (verbClass.equals("-") || verbClass.length() == 0)
						verbClass = UNKNOWN_VERB_CLASS;
				} else
					verbClass = UNKNOWN_VERB_CLASS;

				propFrameData.addSense(sense, verbClass);

				NodeList roles = roleSet.getElementsByTagName("role");

				// System.out.println(sense);

				for (int j = 0; j < roles.getLength(); j++) {
					Element role = (Element) roles.item(j);

					String argLabel = "A" + role.getAttribute("n");
					propFrameData.addArgument(sense, argLabel);

					if (role.hasAttribute("descr")) {
						String descr = role.getAttribute("descr");

						propFrameData.addArgumentDescription(sense, argLabel,
								descr);

					}

					NodeList elementsByTagName = role
							.getElementsByTagName("vnrole");

					for (int roleId = 0; roleId < elementsByTagName.getLength(); roleId++) {

						String vntheta = ((Element) (elementsByTagName
								.item(roleId))).getAttribute("vntheta");

						propFrameData.addArgumentVNTheta(sense, argLabel,
								vntheta);
					}

					// System.out.println(lemma + "\t" + sense + "\t" +
					// argLabel);

				} // end of arguments

				addExamples(propFrameData, roleSet, sense);

			}
		}

	}

	private void addExamples(PropFrameData propFrameData, Element roleSet,
			String sense) {
		NodeList examples = roleSet.getElementsByTagName("example");

		for (int exampleId = 0; exampleId < examples.getLength(); exampleId++) {
			Element example = (Element) examples.item(exampleId);

			String name = "";
			if (example.hasAttribute("name"))
				name = example.getAttribute("name");

			String text = example.getElementsByTagName("text").item(0)
					.getTextContent();

			NodeList args = example.getElementsByTagName("arg");

			Map<String, String> argDescriptions = new HashMap<String, String>();
			Map<String, String> argExamples = new HashMap<String, String>();

			for (int argId = 0; argId < args.getLength(); argId++) {
				Element arg = (Element) args.item(argId);

				String n = "";
				if (arg.hasAttribute("n"))
					n = "A" + arg.getAttribute("n");

				if (n.length() <= 1 || n.toUpperCase().equals("AM"))
					continue;

				if (arg.hasAttribute("f")) {
					String f = arg.getAttribute("f").toLowerCase();
					if (prepositions.contains(f)) {
						argDescriptions.put(n, f);
						argExamples.put(n, arg.getTextContent());
					}
				} else {
					String t = arg.getTextContent();
					String[] parts = t.split(" ");
					if (parts.length > 0) {
						t = parts[0].toLowerCase();
						if (prepositions.contains(t)) {
							argDescriptions.put(n, t);
							argExamples.put(n, arg.getTextContent());

						}
					}
				}

			}

			propFrameData.addExample(sense, name, text, argDescriptions,
					argExamples);
		}
	}

	public HashMap<String, PropFrameData> frameData;

	public Set<String> getPredicates() {
		return frameData.keySet();
	}

	public PropFrameData getFrame(String lemma) {
		return frameData.get(lemma);
	}

	public static void main(String[] args) throws Exception {
		new PropFramesReader("data/phenomena/verbs/pb-frames");
	}

}
