package edu.illinois.cs.cogcomp.srl.data;

import java.io.FileNotFoundException;

import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.utilities.SRLUtils;

public class CoNLLColumnFormatReaderSRL extends CoNLLColumnFormatReader {
	public CoNLLColumnFormatReaderSRL(String corpora, String section,
			String columnFile, String predicateArgumentViewName)
			throws FileNotFoundException {
		super(corpora, section, columnFile, predicateArgumentViewName);
	}

	@Override
	protected TextAnnotation makeTextAnnotation() throws Exception {
		TextAnnotation ta = super.makeTextAnnotation();
		if (ta == null)
			return null;

		try {
			ta.addView(ViewNames.PSEUDO_PARSE,
					SRLUtils.createPsuedoParseView(ta));

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(ta.getTokenizedText());
			throw new RuntimeException(e);
		}

		return ta;
	}

}