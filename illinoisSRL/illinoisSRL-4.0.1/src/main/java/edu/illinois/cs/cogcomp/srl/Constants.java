package edu.illinois.cs.cogcomp.srl;

/**
 * @author Vivek Srikumar
 * 
 */
public class Constants {

	public static boolean VERBOSE = false;

	public final static String systemVersion = "4.0";

	public final static String verbSRLSystemName = "Illinois Verb Semantic Role Labeler";

	public final static String verbSRLSystemCuratorName = "verb-IllinoisSRL";

	public final static String verbSRLSystemIdentifier = verbSRLSystemCuratorName
			+ "-" + systemVersion;

	public final static String nomSRLSystemCuratorName = "nom-IllinoisSRL";

	public final static String nomSRLSystemName = "Illinois Nominal Semantic Role Labeler";

	public final static String nomSRLSystemIdentifier = nomSRLSystemCuratorName
			+ "-" + systemVersion;
}