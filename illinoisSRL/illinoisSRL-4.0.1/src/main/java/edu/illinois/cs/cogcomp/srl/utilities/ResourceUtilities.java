package edu.illinois.cs.cogcomp.srl.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.edison.annotators.WordNetPlusLemmaViewGenerator;
import edu.illinois.cs.cogcomp.edison.utilities.WordNetManager;
import edu.illinois.cs.cogcomp.srl.SRLProperties;

public class ResourceUtilities {
	private static Logger log = LoggerFactory
			.getLogger(ResourceUtilities.class);

	private static WordNetManager wordnet;

	public static WordNetManager getWordNetManager() {
		if (wordnet == null) {
			try {
				wordnet = WordNetManager.getInstance(SRLProperties
						.getInstance().getWordNetFile());
			} catch (Exception e) {
				log.error("Error initializing wordnet", e);
			}
		}
		return wordnet;
	}

	public final static WordNetPlusLemmaViewGenerator wordNetPlusLemmatizer = new WordNetPlusLemmaViewGenerator(
			getWordNetManager());

}
