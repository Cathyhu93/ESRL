package edu.illinois.cs.cogcomp.srl.core;

public enum Models {
	Classifier, Identifier, Sense, Predicate
}
